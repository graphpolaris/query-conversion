/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	"git.science.uu.nl/graphpolaris/query-conversion/entity"
	"git.science.uu.nl/graphpolaris/query-conversion/sparql"
)

/*
The main function that calls the appropriate functions
*/
func main() {
	queryservice := sparql.NewService()
	jsonFile, err := os.Open("../realtest.json")
	// if we os.Open returns an error then handle it
	if err != nil {
		log.Println(err)
	}
	log.Println("Successfully Opened users.json")
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()
	js, _ := ioutil.ReadAll(jsonFile)
	log.Println(string(js))
	var inc entity.IncomingQueryJSON
	json.Unmarshal(js, &inc)
	result, tripleMap, _ := queryservice.ConvertQuery(&inc)
	log.Println(string(*tripleMap))
	log.Println(*result)
}
