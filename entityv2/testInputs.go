/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entityv2


var TwoEntitiesWithFilter = []byte(`{
	"queryname": 1,
	"db": 1,
	"query": [
	  {
		"node": {
		  "label": "person",
		  "id": "p1",
		  "filter": [{ "attribute": "name", "operation": "EQ", "value": "\"John\"" }],
		  "relationship": {
			"label": "friend",
			"direction": "TO",
			"filter": [{ "attribute": "since", "operation": "GT", "value": "\"2015-01-01\"" }],
			"node": {
			  "label": "person",
			  "filter": [{ "attribute": "name", "operation": "NEQ", "value": "\"John\"" }]
			}
		  }
		}
	  }
	]
  }
  `)
