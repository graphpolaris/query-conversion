/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entityv2

/*
IncomingQueryJSON describes the query coming into the service in JSON format
*/
type IncomingQueryJSON struct {
	QueryID string `json:"queryID"`
	MachineLearning []QueryMLStruct `json:"ml"`
	Cached bool `json:"cached"`
}

/*
QueryMLStruct holds info for machinelearning
*/
type QueryMLStruct struct {
	Type       string `json:"type"`
	Parameters []string `json:"parameters"`
}

type QueryStruct struct {
	ID   string     `json:"id"`
	Node NodeStruct `json:"node"`
}

type NodeStruct struct {
	Label string `json:"label"`
	ID    string `json:"id"`
	// Logic      []LogicStruct      `json:"logic"`
	Filter   []FilterStruct     `json:"filter"`
	Relation RelationStruct     `json:"relation"`
	SubQuery *QueryStruct       `json:"subquery"`
	Export   []ExportNodeStruct `json:"export"`
}

type ExportNodeStruct struct {
	ID        string `json:"id"`
	Attribute string `json:"attribute"`
}

type FilterStruct struct {
	Attribute string `json:"attribute"`
	Operation string `json:"operation"`
	Value     string `json:"value"`
}

type RelationStruct struct {
	Label     string                 `json:"label"`
	ID        string                 `json:"id"`
	Depth     QuerySearchDepthStruct `json:"depth"`
	Direction string                 `json:"direction"`
	Node      *NodeStruct            `json:"node"`
}

/*
QuerySearchDepthStruct holds the range of traversals for the relation
*/
type QuerySearchDepthStruct struct {
	Min int `json:"min"`
	Max int `json:"max"`
}

//
// Logic Structs!
//

type LogicStruct struct {
	Attribute string `json:"attribute"`
	Operation string `json:"operation"`
	ID        string `json:"id"`
}
