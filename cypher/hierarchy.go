package cypher

import (
	"errors"
	"log"

	"git.science.uu.nl/graphpolaris/query-conversion/entity"
)

// createQueryHierarchy finds out what depends on what, then uses topological sort to create a hierarchy
func createQueryHierarchy(JSONQuery *entity.IncomingQueryJSON) (entity.Query, error) {

	var parts entity.Query
	IDctr := 0

	// Add relations all to query parts
	for _, rel := range JSONQuery.Relations {

		part := entity.QueryPart{
			QType:        "relation",
			QID:          rel.ID,
			PartID:       IDctr,
			Dependencies: make([]int, 0),
		}
		parts = append(parts, part)

		IDctr++

	}

	// Add the Groupby's
	for _, gb := range JSONQuery.GroupBys {
		part := entity.QueryPart{
			QType:        "groupBy",
			QID:          gb.ID,
			PartID:       IDctr,
			Dependencies: make([]int, 0),
		}
		parts = append(parts, part)

		IDctr++

	}

	// Add the entities, if they have an IN, otherwise they are not important
	for _, ent := range JSONQuery.Entities {

		skip := true
		for _, con := range ent.Constraints {
			if con.InID != -1 {
				skip = false
			}
		}

		if skip {
			continue
		}

		part := entity.QueryPart{
			QType:        "entity",
			QID:          ent.ID,
			PartID:       IDctr,
			Dependencies: make([]int, 0),
		}
		parts = append(parts, part)

		IDctr++
	}

	// Check dependencies in a nice O(n^2)
	for _, rel := range JSONQuery.Relations {
		if rel.FromID == -1 {
			continue
		}

		// Check the dependencies From - To
		for _, rela := range JSONQuery.Relations {
			if rela.ToID == -1 {
				continue
			}

			if rel.FromID == rela.ToID && rel.FromType == rela.ToType {
				part := parts.Find(rel.ID, "relation")
				part.Dependencies = append(part.Dependencies, parts.Find(rela.ID, "relation").PartID)
			}
		}

		if rel.ToID == -1 {
			continue
		}

		// Now for connections to group by's it doesnt matter if the GB is attached to the from or the to
		// The GB always has priority
		for _, gb := range JSONQuery.GroupBys {
			if (rel.FromID == gb.ID && rel.FromType == "groupBy") || (rel.ToID == gb.ID && rel.ToType == "groupBy") {
				part := parts.Find(rel.ID, "relation")
				gbID := parts.Find(gb.ID, "groupBy").PartID
				part.Dependencies = append(part.Dependencies, gbID)
			}
		}
	}

	// Same trick for group by's
	for _, gb := range JSONQuery.GroupBys {
		for _, rela := range JSONQuery.Relations {
			// Check if the gb is connected to the relation

			// Check for all by's
			for _, ID := range gb.ByID {
				if (ID == rela.ID && gb.ByType == "relation") || // Is the By connected to a relation
					(ID == rela.FromID && gb.ByType == rela.FromType) || // Is the by connected to an entity connected to the "From" of a relation
					(ID == rela.ToID && gb.ByType == rela.ToType) { // Is the by connected to an entity connected to the "To" of a relation
					part := parts.Find(gb.ID, "groupBy")
					part.Dependencies = append(part.Dependencies, parts.Find(rela.ID, "relation").PartID)
					continue
				}
			}

			// Check for all groups
			for _, ID := range gb.GroupID {

				if (ID == rela.ID && gb.GroupType == "relation") || // is the Group connected to a relation
					(ID == rela.FromID && gb.GroupType == rela.FromType) || // Is the group connected to an entity connected to the "From" of arelation
					(ID == rela.ToID && gb.GroupType == rela.ToType) { // Is the group connected to an entity connected to the "To" of a relation
					part := parts.Find(gb.ID, "groupBy")
					part.Dependencies = append(part.Dependencies, parts.Find(rela.ID, "relation").PartID)
				}
			}
		}

	}

	for _, ent := range JSONQuery.Entities {
		for _, con := range ent.Constraints {
			if con.InID != -1 {
				part := parts.Find(ent.ID, "entity") // Should always be groupBy
				part.Dependencies = append(part.Dependencies, parts.Find(con.InID, con.InType).PartID)
			}
		}

	}

	// Here comes a checker for (A)-->(B) and (B)-->(A). This is mitigated partly by ignoring it
	// Lets call it a small cycle. It wont catch bigger cycles (with 3 nodes for example)

	for _, p := range parts {
		// We only allow small cycles with relations
		if p.QType != "relation" {
			continue
		}

		for _, dep := range p.Dependencies {
			other := parts.SelectByID(dep)

			if other.QType != "relation" {
				continue
			}

			// Deleting from a slice while looping through it is an easy way to make mistakes, hence the workaround
			cycle := false
			toRemove := -1

			for i, otherDep := range other.Dependencies {
				if otherDep == p.PartID {
					// Small cycle detected

					cycle = true
					toRemove = i
				}
			}

			// Remove one of the two dependencies, does not really matter which, cypher knits it back together due to the query
			// using the same ID's, thus making it a cycle again later on.
			if cycle {
				log.Println("Cycle detected and removed")
				if len(other.Dependencies) == 0 {
					other.Dependencies = make([]int, 0)
				} else {
					other.Dependencies[toRemove] = other.Dependencies[len(other.Dependencies)-1]
					other.Dependencies = other.Dependencies[:len(other.Dependencies)-1]
				}

			}
		}
	}

	// Now we have a directed graph, meaning we can use some topological sort (Kahn's algorithm)
	var sortedQuery entity.Query
	incomingEdges := make(map[int]int)

	// Set all to 0
	for _, p := range parts {
		incomingEdges[p.PartID] = 0
	}

	// Count the incoming edges (dependencies)
	for _, p := range parts {
		for _, dp := range p.Dependencies {
			incomingEdges[dp]++
		}
	}

	for { // While there is a someone where incomingEdges[someone] == 0
		part := entity.QueryPart{PartID: -1}
		// Select a node with no incoming edges
		for ID, edges := range incomingEdges {
			if edges == 0 {
				part = *parts.SelectByID(ID)
			}
		}

		// Check to see if there are parts withouth incoming edges left
		if part.PartID == -1 {
			break
		}

		// Remove it from the set
		incomingEdges[part.PartID] = -1
		sortedQuery = append(sortedQuery, part)

		// Decrease incoming edges of other parts
		for _, ID := range part.Dependencies {
			incomingEdges[ID]--
		}
	}

	// Now check for cycles in the graph
	partRemaining := false
	for _, edges := range incomingEdges {
		if edges != -1 {
			partRemaining = true
		}
	}

	if partRemaining {
		// Somehow there was a cycle in the query,
		return nil, errors.New("Cyclic query detected")
	}

	// Reverse the list
	retQuery := make([]entity.QueryPart, len(sortedQuery))
	for i := 0; i < len(sortedQuery); i++ {
		retQuery[i] = sortedQuery[len(sortedQuery)-i-1]
	}

	return retQuery, nil
}
