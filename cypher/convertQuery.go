/*
 This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
 © Copyright Utrecht University(Department of Information and Computing Sciences)
*/

package cypher

import (
	"errors"
	"fmt"
	"strings"

	"git.science.uu.nl/graphpolaris/query-conversion/entity"
)

// ConvertQuery takes the json from the visual query builder and converts it into Cypher
func (s *Service) ConvertQuery(totalJSONQuery *entity.IncomingQueryJSON) (*string, *[]byte, error) {
	var finalCypher *string

	queryJSON := totalJSONQuery

	// If you want to query the other cluster as well, remove the underscores
	query, _, _ := checkForQueryCluster(queryJSON)

	if query == nil {
		return nil, nil, errors.New("Invalid query")
	}

	ok, err := checkNoDeadEnds(query)
	if !ok {
		return nil, nil, err
	}

	finalCypher, err = createCypher(query)
	if err != nil {
		return nil, nil, err
	}

	return finalCypher, nil, nil
}

// createCypher translates a cluster of nodes (query) to Cypher
func createCypher(JSONQuery *entity.IncomingQueryJSON) (*string, error) {

	// create the hierarchy from the cluster
	hierarchy, err := createQueryHierarchy(JSONQuery)
	if err != nil {
		return nil, err
	}

	// translate it to cypher in the right order, using the hierarchy
	cypher, err := formQuery(JSONQuery, hierarchy)
	if err != nil {
		return nil, err
	}

	// create the return statement
	returnStatement, err := createReturnStatement(JSONQuery, hierarchy)
	if err != nil {
		return nil, errors.New("Creation of return Cypher failed")
	}

	finalCypher := *cypher + *returnStatement

	return &finalCypher, nil
}

// createReturnStatement creates the final return statement
func createReturnStatement(JSONQuery *entity.IncomingQueryJSON, parts entity.Query) (*string, error) {

	// new plan: add them all to a list
	// MATCH (e0:Person)-[r0:DIRECTED]-(e1:Movie)
	// WHERE  e0.name <> "Raymond Campbell"
	// WITH *
	// MATCH (e0:Person)-[r1:ACTED_IN]-(e3:Movie)
	// UNWIND [e0, e1, e3] as e
	// UNWIND [r0, r1] as r
	// return count(distinct e), count(distinct r)

	var retStatement string
	var retType string // This is a marker attached to the end, for ease of parsing in the executor

	// First check to see if the return is a table (due to a groupby at the end) or if it is nodelink data
	numOfParts := len(parts)
	if numOfParts == 0 {
		return nil, errors.New("No parts found in return statement")
	}

	if parts[numOfParts-1].QType == "groupBy" {
		// Return is a table
		groupBy := JSONQuery.FindG(parts[numOfParts-1].QID)

		gName := fmt.Sprintf("%v_%v", groupBy.AppliedModifier, groupBy.GroupAttribute)
		byID := fmt.Sprint(groupBy.ByID[0])

		if len(groupBy.ByID) > 1 {
			byID = ""

			for _, x := range groupBy.ByID {
				byID += fmt.Sprint(x)
			}
			byID += "L"
		}
		by := fmt.Sprintf("%v%v.%v", string(groupBy.ByType[0]), byID, groupBy.ByAttribute)
		byName := strings.Replace(by, ".", "_", 1)

		retStatement = fmt.Sprintf("RETURN %v, %v", byName, gName)
		retType = ";table"
	} else {

		returnlist := make([]string, 0)

		// Return is nodelink
		// Loop through the parts of the query from back to front
		for i := numOfParts - 1; i >= 0; i-- {
			part := parts[i]
			if part.QType == "relation" {
				rel := JSONQuery.FindR(part.QID)
				returnlist = append(returnlist, fmt.Sprintf("r%v", rel.ID))

				if rel.FromID != -1 {
					if rel.FromType == "entity" {

						returnlist = append(returnlist, fmt.Sprintf("e%v", rel.FromID))
					} else {
						id := JSONQuery.FindG(rel.FromID).ByID
						idstr := fmt.Sprint(id[0])
						if len(id) > 1 {
							idstr = ""

							for _, x := range id {
								idstr += fmt.Sprint(x)
							}
							idstr += "L"
						}
						returnlist = append(returnlist, fmt.Sprintf("eg%v", idstr))
					}
				}

				if rel.ToID != -1 {
					if rel.ToType == "entity" {
						returnlist = append(returnlist, fmt.Sprintf("e%v", rel.ToID))
					} else {
						id := JSONQuery.FindG(rel.ToID).ByID
						idstr := fmt.Sprint(id[0])
						if len(id) > 1 {
							idstr = ""

							for _, x := range id {
								idstr += fmt.Sprint(x)
							}
							idstr += "L"
						}
						returnlist = append(returnlist, fmt.Sprintf("eg%v", idstr))
					}
				}
			} else if part.QType == "entity" {
				returnlist = append(returnlist, fmt.Sprintf("e%v", part.QID))
				break

				// Probably ends with a break, since a single entity is always connected via an IN to a groupby? (maybe not in case of ONLY having an entity as the entire query)
			} else {
				// Then it is a groupby which must not be returned, thus the returns are done.
				break
			}
		}

		// Format nodes
		lineStart := ""
		unwindStatement := "UNWIND ["
		for _, node := range returnlist {
			unwindStatement += fmt.Sprintf("%v%v", lineStart, node)
			lineStart = ","
		}
		unwindStatement += "] AS x \n"

		retStatement = unwindStatement + "RETURN DISTINCT x"

		retType = ";nodelink"
	}

	retStatement = retStatement + "\n" + fmt.Sprintf("LIMIT %v", JSONQuery.Limit) + retType

	return &retStatement, nil
}

// formQuery uses the hierarchy to create cypher for each part of the query in the right order
func formQuery(JSONQuery *entity.IncomingQueryJSON, hierarchy entity.Query) (*string, error) {

	// Traverse through the hierarchy and for every entry create a part like:
	// Match p0 = (l:Lorem)-[:Ipsum*1..1]-(d:Dolor)
	// Constraints on l and d
	// Unwind relationships(p0) as r0
	// Constraints on r0
	// With *

	totalQuery := ""

	for _, entry := range hierarchy {
		var cypher *string
		var err error

		switch entry.QType {
		case "relation":
			cypher, err = createRelationCypher(JSONQuery, entry)
			if err != nil {
				return nil, err
			}
			break
		case "groupBy":
			cypher, err = createGroupByCypher(JSONQuery, entry)
			if err != nil {
				return nil, err
			}

			break
		case "entity":
			// This would be in case of an IN or if there was only 1 entity in the query builder
			cypher, err = createInCypher(JSONQuery, entry)
			if err != nil {
				return nil, err
			}

			break
		default:
			// Should never be reached
			return nil, errors.New("Invalid query pill type detected")
		}

		totalQuery += *cypher
	}

	return &totalQuery, nil
}

// createInCypher creates the cypher for an entity with an IN-clause
func createInCypher(JSONQuery *entity.IncomingQueryJSON, part entity.QueryPart) (*string, error) {
	ent := JSONQuery.FindE(part.QID)
	eName := fmt.Sprintf("e%v", ent.ID)

	match := fmt.Sprintf("MATCH (%v:%v)\n", eName, ent.Name)
	eConstraints := ""
	newLineStatement := "\tWHERE"

	// Find the IN
	for _, con := range ent.Constraints {
		if con.InID != -1 {
			gby := JSONQuery.FindG(con.InID) // Because this could only be on a groupby
			byID := fmt.Sprint(gby.ByID[0])

			if len(gby.ByID) > 1 {
				byID = ""

				for _, x := range gby.ByID {
					byID += fmt.Sprint(x)
				}
				byID += "L"
			}

			byName := fmt.Sprintf("%v%v", string(gby.ByType[0]), byID)
			eConstraints += fmt.Sprintf("%v %v.%v IN %v_%v\n", newLineStatement, eName, con.Attribute, byName, gby.ByAttribute)
			newLineStatement = "\tAND"
		}
	}

	// Attach other constraints (if any)
	for _, v := range ent.Constraints {
		if v.InID != -1 {
			continue
		}
		eConstraints += fmt.Sprintf("%v %v \n", newLineStatement, *createConstraintBoolExpression(&v, eName, false))
	}

	with := "WITH *\n"
	retStatement := match + eConstraints + with
	return &retStatement, nil

}

// createRelationCypher takes the json and a query part, finds the necessary entities and converts it into cypher
func createRelationCypher(JSONQuery *entity.IncomingQueryJSON, part entity.QueryPart) (*string, error) {

	rel := JSONQuery.FindR(part.QID)

	if (rel.FromID == -1) && (rel.ToID == -1) {
		// Now there is only a relation, which we do not allow
		return nil, errors.New("Relation only queries are not supported")
	}

	var match, eConstraints, unwind, rConstraints string

	// There is some duplicate code here below that could be omitted with extra if-statements, but that is something to do
	// for a later time. Since this way it is easier to understand the flow of the code
	// Removing the duplicate code here, probably more than triples the if-statements and is a puzzle for a later time (TODO)
	if rel.ToID == -1 {
		// There is no To, only a From
		var eName string
		var entFrom *entity.QueryEntityStruct
		var entFromType string

		if rel.FromType == "entity" {

			entFrom = JSONQuery.FindE(rel.ToID)
			entFromType = entFrom.Name
			eName = fmt.Sprintf("e%v", entFrom.ID)

		} else if rel.FromType == "groupBy" {
			gb := JSONQuery.FindG(rel.FromID)
			if gb.ByType == "relation" {
				return nil, errors.New("Invalid query: cannot connect a relation to a group by that groups by another relation")
			}

			if len(gb.ByID) == 1 {
				// This is a sort of dummy variable, since it is not directly visible in the query, but it is definitely needed
				eName = fmt.Sprintf("eg%v", gb.ByID[0])
			} else {
				byID := ""

				for _, x := range gb.ByID {
					byID += fmt.Sprint(x)
				}
				byID += "L"
				eName = fmt.Sprintf("eg%v", byID)
			}
			entFromType = JSONQuery.FindE(gb.ByID[0]).Name

		} else {
			// Should never be reachable
			return nil, errors.New("Invalid connection type to relation")
		}

		match = fmt.Sprintf("MATCH p%v = (%v:%v)-[:%v*%v..%v]-()\n", part.PartID, eName, entFromType, rel.Name, rel.Depth.Min, rel.Depth.Max)

		eConstraints = ""
		newLineStatement := "\tWHERE"

		// The nil-check is there in case it is connected to a groupby
		if entFrom != nil {
			for _, v := range entFrom.Constraints {
				eConstraints += fmt.Sprintf("%v %v \n", newLineStatement, *createConstraintBoolExpression(&v, eName, false))
				newLineStatement = "\tAND"
			}
		}

		// Add an IN clause, connecting the relation to the output of the groupby
		if rel.FromType == "groupBy" {
			gb := JSONQuery.FindG(rel.FromID)
			inConstraint := fmt.Sprintf("%v %v.%v IN %v_%v \n", newLineStatement, eName, gb.ByAttribute, gb.AppliedModifier, gb.ByAttribute)
			eConstraints += inConstraint
		}

	} else if rel.FromID == -1 {
		var eName string
		var entToType string
		var entTo *entity.QueryEntityStruct

		if rel.ToType == "entity" {
			entTo = JSONQuery.FindE(rel.ToID)
			entToType = entTo.Name
			eName = fmt.Sprintf("e%v", entTo.ID)

		} else if rel.ToType == "groupBy" {
			gb := JSONQuery.FindG(rel.ToID)
			if gb.ByType == "relation" {
				return nil, errors.New("Invalid query: cannot connect a relation to a group by that groups by another relation")
			}

			if len(gb.ByID) == 1 {
				// This is a sort of dummy variable, since it is not directly visible in the query, but it is definitely needed
				eName = fmt.Sprintf("eg%v", gb.ByID[0])
			} else {
				byID := ""

				for _, x := range gb.ByID {
					byID += fmt.Sprint(x)
				}
				byID += "L"
				eName = fmt.Sprintf("eg%v", byID)
			}
			entToType = JSONQuery.FindE(gb.ByID[0]).Name

		} else {
			// Should never be reachable
			return nil, errors.New("Invalid connection type to relation")
		}

		match = fmt.Sprintf("MATCH p%v = ()-[:%v*%v..%v]-(%v:%v)\n", part.PartID, rel.Name, rel.Depth.Min, rel.Depth.Max, eName, entToType)

		eConstraints = ""
		newLineStatement := "\tWHERE"

		if entTo != nil {
			for _, v := range entTo.Constraints {
				eConstraints += fmt.Sprintf("%v %v \n", newLineStatement, *createConstraintBoolExpression(&v, eName, false))
				newLineStatement = "\tAND"
			}
		}

		// Add an IN clause, connecting the relation to the output of the groupby
		if rel.ToType == "groupBy" {
			gb := JSONQuery.FindG(rel.ToID)
			inConstraint := fmt.Sprintf("%v %v.%v IN %v_%v \n", newLineStatement, eName, gb.ByAttribute, gb.AppliedModifier, gb.ByAttribute)
			eConstraints += inConstraint
		}

	} else {
		var eTName string
		var entFromType string
		var eFName string
		var entToType string
		var entFrom, entTo *entity.QueryEntityStruct

		// Check of what type the To is
		if rel.ToType == "entity" {
			entTo = JSONQuery.FindE(rel.ToID)
			entToType = entTo.Name
			eTName = fmt.Sprintf("e%v", entTo.ID)

		} else if rel.ToType == "groupBy" {
			gb := JSONQuery.FindG(rel.ToID)
			if gb.ByType == "relation" {
				return nil, errors.New("Invalid query: cannot connect a relation to a group by that groups by another relation")
			}

			if len(gb.ByID) == 1 {
				// This is a sort of dummy variable, since it is not directly visible in the query, but it is definitely needed
				eTName = fmt.Sprintf("eg%v", gb.ByID[0])
			} else {
				byID := ""

				for _, x := range gb.ByID {
					byID += fmt.Sprint(x)
				}
				byID += "L"
				eTName = fmt.Sprintf("eg%v", byID)
			}
			entToType = JSONQuery.FindE(gb.ByID[0]).Name

		} else {
			// Should never be reachable
			return nil, errors.New("Invalid connection type to relation")
		}

		// Check of what type the From is
		if rel.FromType == "entity" {

			entFrom = JSONQuery.FindE(rel.FromID)
			entFromType = entFrom.Name
			eFName = fmt.Sprintf("e%v", entFrom.ID)

		} else if rel.FromType == "groupBy" {
			gb := JSONQuery.FindG(rel.FromID)
			if gb.ByType == "relation" {
				return nil, errors.New("Invalid query: cannot connect a relation to a group by that groups by another relation")
			}

			if len(gb.ByID) == 1 {
				// This is a sort of dummy variable, since it is not directly visible in the query, but it is definitely needed
				eFName = fmt.Sprintf("eg%v", gb.ByID[0])
			} else {
				byID := ""

				for _, x := range gb.ByID {
					byID += fmt.Sprint(x)
				}
				byID += "L"
				eFName = fmt.Sprintf("eg%v", byID)
			}

			entFromType = JSONQuery.FindE(gb.ByID[0]).Name

		} else {
			// Should never be reachable
			return nil, errors.New("Invalid connection type to relation")
		}

		match = fmt.Sprintf("MATCH p%v = (%v:%v)-[:%v*%v..%v]-(%v:%v)\n", part.PartID, eFName, entFromType, rel.Name, rel.Depth.Min, rel.Depth.Max, eTName, entToType)

		eConstraints = ""
		newLineStatement := "\tWHERE"
		if entFrom != nil {
			for _, v := range entFrom.Constraints {
				eConstraints += fmt.Sprintf("%v %v \n", newLineStatement, *createConstraintBoolExpression(&v, eFName, false))
				newLineStatement = "\tAND"
			}
		}
		if entTo != nil {
			for _, v := range entTo.Constraints {
				eConstraints += fmt.Sprintf("%v %v \n", newLineStatement, *createConstraintBoolExpression(&v, eTName, false))
				newLineStatement = "\tAND"
			}
		}

		// Add an IN clause, connecting the relation to the output of the groupby
		if rel.ToType == "groupBy" {
			gb := JSONQuery.FindG(rel.ToID)
			inConstraint := fmt.Sprintf("%v %v.%v IN %v_%v \n", newLineStatement, eTName, gb.ByAttribute, strings.Replace(eFName, "g", "", 1), gb.ByAttribute)
			eConstraints += inConstraint
			newLineStatement = "\tAND"
		}

		if rel.FromType == "groupBy" {
			gb := JSONQuery.FindG(rel.FromID)
			inConstraint := fmt.Sprintf("%v %v.%v IN %v_%v \n", newLineStatement, eFName, gb.ByAttribute, strings.Replace(eFName, "g", "", 1), gb.ByAttribute)
			eConstraints += inConstraint
		}
	}

	rName := fmt.Sprintf("r%v", part.QID)
	unwind = fmt.Sprintf("UNWIND relationships(p%v) as %v \nWITH *\n", part.PartID, rName)

	rConstraints = ""
	newLineStatement := "\tWHERE"
	for _, v := range rel.Constraints {
		rConstraints += fmt.Sprintf("%v %v \n", newLineStatement, *createConstraintBoolExpression(&v, rName, false))
		newLineStatement = "\tAND"
	}

	retString := match + eConstraints + unwind + rConstraints
	return &retString, nil

}

// createGroupByCypher takes the json and a query part, finds the group by and converts it into cypher
func createGroupByCypher(JSONQuery *entity.IncomingQueryJSON, part entity.QueryPart) (*string, error) {
	groupBy := JSONQuery.FindG(part.QID)
	var group, by string

	gName := fmt.Sprintf("%v_%v", groupBy.AppliedModifier, groupBy.GroupAttribute)
	byID := fmt.Sprint(groupBy.ByID[0])
	groupID := fmt.Sprint(groupBy.GroupID[0])
	unwindBy := ""
	unwindGroup := ""
	gType := string(groupBy.GroupType[0])
	bType := string(groupBy.ByType[0])

	// Due to this concatenation, there is a chance that there is a ID of 1, 2 and 12. Which could create a conflict
	// Thus I will add an L to the end, indicating that it is a List
	if len(groupBy.ByID) > 1 {
		byID = ""
		unwindBy = "UNWIND ["
		linestart := ""

		for _, x := range groupBy.ByID {
			byID += fmt.Sprint(x)
			unwindBy += linestart + bType + fmt.Sprint(x)
			linestart = ","
		}
		byID += "L"
		unwindBy += "] AS " + bType + byID + "\n"
	}

	if len(groupBy.GroupID) > 1 {
		groupID = ""
		unwindGroup = "UNWIND ["
		linestart := ""

		for _, x := range groupBy.GroupID {
			groupID += fmt.Sprint(x)
			unwindGroup += linestart + gType + fmt.Sprint(x)
			linestart = ","
		}

		groupID += "L"
		unwindGroup += "] AS " + gType + groupID + "\n"
	}

	group = fmt.Sprintf("%v%v.%v", gType, groupID, groupBy.GroupAttribute)
	by = fmt.Sprintf("%v%v.%v", bType, byID, groupBy.ByAttribute)
	byName := strings.Replace(by, ".", "_", 1)

	// If you do not use a *, then everything needs to be aliased
	with := fmt.Sprintf("WITH %v AS %v, %v(%v) AS %v \n", by, byName, groupBy.AppliedModifier, group, gName)

	gConstraints := ""
	newLineStatement := "\tWHERE"
	for _, v := range groupBy.Constraints {
		gConstraints += fmt.Sprintf("%v %v \n", newLineStatement, *createConstraintBoolExpression(&v, gName, true))
		newLineStatement = "\tAND"
	}

	retString := unwindBy + unwindGroup + with + gConstraints
	return &retString, nil
}

// // Manier voor groupby's op samengevoegde entities
// CALL {
// MATCH p0 = (e11:Person)-[:DIRECTED*1..1]-(e12:Movie)
// UNWIND relationships(p0) as r10
// Return e11 as e1, e12 as e2
// UNION
// MATCH p1 = (e13:Person)-[:ACTED_IN]-(e14:Movie)
// UNWIND relationships(p1) as r11
// Return e13 as e1, e14 as e2
// }
// WITH e1.bornIn AS e1_bornIn, AVG(e2.budget) AS AVG_budget
// RETURN e1_bornIn, AVG_budget
// LIMIT 5000
