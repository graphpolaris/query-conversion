/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package cypher

/*
Service implements the QueryConverter interface (in the query service)
*/
type Service struct {
}

/*
NewService creates a new Cypher conversion service
	Return: *Service, the new service
*/
// func NewService() query.Converter {
// 	return &Service{}
// }
