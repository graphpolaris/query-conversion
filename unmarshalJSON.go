/*
 This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
 © Copyright Utrecht University(Department of Information and Computing Sciences)
*/

package query

import (
	"encoding/json"

	"git.science.uu.nl/graphpolaris/query-conversion/entityv2"
)

/*
UnmarshalJSON takes an incoming message and unmarshals it
	msg: *[]byte, the incoming message
	Returns: *entity.IncomingQueryJSON, the unmarshalled message and a possible error
*/
func UnmarshalJSON(msg *[]byte) (*entityv2.IncomingQueryJSON, error) {
	var JSONQuery entityv2.IncomingQueryJSON
	err := json.Unmarshal(*msg, &JSONQuery)
	return &JSONQuery, err
}
