[![License](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)
<div align='center'>

<img src="https://git.science.uu.nl/GraphPolaris/frontend/-/raw/develop/src/presentation/view/navbar/logogp.png" align="center" width="150" alt="Project icon">
</div>

# Query Converter
This is the query-conversion package. It holds the code that translates the JSON of the visual query to the selected database language. There are currently three implementations, one for the Arango Query Language, one for Cypher and finally one for SPARQL.

## Creating a new AQL converter
```go
import "git.science.uu.nl/graphpolaris/query-conversion"

queryservice := aql.NewService()
```

## Creating a new Cypher converter
```go
import "git.science.uu.nl/graphpolaris/query-conversion"

queryservice := cypher.NewService()
```

## Creating a new SPARQL converter
```go
import "git.science.uu.nl/graphpolaris/query-conversion"

queryservice := sparql.NewService()
```

## Converting a query
A query can be made by providing the JSON of the visual query and feeding it into the `ConvertQuery` function.

```go
query, err := queryservice.ConvertQuery(JSONquery)
```

## Creating a mock converter
```go
import "git.science.uu.nl/graphpolaris/query-conversion"

mockService := NewMockService()
```

### Dependencies
This service depends on RabbitMQ

### Testing and Coverage
To test the `make test` command can be used. It will run every test in the repo. To get the code coverage the command `make coverage` can be used. This command will run all tests and display total coverage over all files. It will also generate a html file which displays exactly which lines have been covered.
