/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package aql

import (
	"errors"

	"git.science.uu.nl/graphpolaris/query-conversion/entity"
)

/*
A MockService implements the query convert usecase interface (mock)
*/
type MockService struct {
	throwError bool
}

/*
NewMockService creates a new query convert service (mock)
	Return: *MockService, returns a mock service
*/
func NewMockService() *MockService {
	return &MockService{
		throwError: false,
	}
}

/*
ConvertQuery returns a hard coded string message (mock)
	JSONQuer: *entity.IncomingQueryJSON, the incoming query in JSON format
	Return: (*string, error), the result and a potential error
*/
func (s *MockService) ConvertQuery(JSONQuery *entity.IncomingQueryJSON) (*string, error) {
	mockQuery := "Query converted"

	if !s.throwError {
		return &mockQuery, nil
	}
	return nil, errors.New("failed to convert query")
}

/*
ToggleError decides whether the convert function throws an error
*/
func (s *MockService) ToggleError() {
	s.throwError = !s.throwError
}
