/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package aql

import (
	//"encoding/json"

	"errors"
	"fmt"
	"strconv"

	"git.science.uu.nl/graphpolaris/query-conversion/entity"
)

// Version 1.13

/*
ConvertQuery converts an IncomingQueryJSON object into AQL
	JSONQuery: *entity.IncomingQueryJSON, the query to be converted to AQL
	Returns: *string, the AQL query and a possible error
*/
func (s *Service) ConvertQuery(JSONQuery *entity.IncomingQueryJSON) (*string, *[]byte, error) {
	// TODO: MICHAEL WANT A SINGLE ENTITY TO RETURN A SUMMARY OF ATTRIBUTE VALUES (HISTOGRAM THINGIES)
	// Check to make sure all indexes exist
	// The count of entities
	entityCount := len(JSONQuery.Entities)
	// The count of relations
	relationCount := len(JSONQuery.Relations)
	// There are no entities or relations, our query is empty
	if entityCount <= 0 && relationCount <= 0 {
		fmt.Println("Empty query sent, returning default response")
		return defaultReturn()
	}

	entity.Validator.Validate(JSONQuery)

	potentialErrors := entity.Validator.Validate(JSONQuery)
	// If we find the JSONQuery to be invalid we return a error
	if len(potentialErrors) != 0 {
		for _, err := range potentialErrors {
			fmt.Printf("err: %v\n", err)
		}
		return nil, nil, errors.New("JSONQuery invalid")
	}

	entityMap, relationMap, groupByMap := entity.FixIndices(JSONQuery)

	var tree entity.TreeList
	if len(JSONQuery.Entities) != 0 && len(JSONQuery.Relations) != 0 {
		var err error
		err, tree = entity.CreateHierarchy(JSONQuery, entityMap, relationMap, groupByMap)
		if err != nil {
			return nil, nil, err
		}
	}

	seperateChain := entity.Validator.Validate(tree)
	if len(seperateChain) != 0 {
		for _, err := range seperateChain {
			fmt.Printf("err: %v\n", err)
		}
		return nil, nil, errors.New("JSONQuery invalid")
	}

	//treeJson, _ := json.Marshal(tree)
	//fmt.Println(string(treeJson))

	variables := make(map[string]string)
	result := ""
	// Loop over all chains in order
	for i, subTree := range tree.Trees {
		var topPill entity.QueryGenericStruct
		if subTree.TopNode.Name != "" {
			topPill.Type = "entity"
			topPill.ID = subTree.TopNode.ID
		} else {
			topPill.Type = "groupBy"
			topPill.ID = subTree.TopGroupBy.ID
		}
		err, subResult, subVariables := createSubQuery(JSONQuery, subTree.TreeElements, topPill, i, entityMap)
		if err != nil {
			return nil, nil, err
		}
		for key, value := range subVariables {
			variables[key] = value
		}
		result += subResult
		// Generate all the group bys able to be generated after the new chain
		for _, groupBy := range subTree.GroupBys {
			result += createGroupBy(groupBy, JSONQuery, relationMap, entityMap, groupByMap, variables)
			if i < len(tree.Trees)-1 { // Not last tree
				result += fmt.Sprintf("LET in%v = (\n\tFOR x IN g%v[**]._id\n\tFOR y IN %v\n\tFILTER x == y.%v\n\tRETURN y\n)\n", 
					groupBy.ID, groupBy.ID, getGroupByByName(JSONQuery, groupBy, entityMap, groupByMap), groupBy.ByAttribute)
			}
		}
	}

	if len(tree.Trees[len(tree.Trees)-1].GroupBys) > 0 { // We have an endpoint group by, return a table
		result += fmt.Sprintf("FOR x in g%v\nRETURN {group: x.modifier, by: x._id}", tree.Trees[len(tree.Trees)-1].GroupBys[0].ID)
	} else { // Return node link diagram
		result += "LET nodes = union_distinct("
		for key, value := range variables {
			if value == "tree_"+strconv.Itoa(len(tree.Trees)-1) { // Element is found in the final tree
				if string(key[0]) == "e" || string(key[0]) == "g" { // Group by or entity, add here
					result += fmt.Sprintf("flatten(tree_%v[**].%v), ", len(tree.Trees)-1, key)
				}
			}
		}
		result += "[])\nLET edges = union_distinct("
		for key, value := range variables {
			if value == "tree_"+strconv.Itoa(len(tree.Trees)-1) { // Element is found in the final tree
				if string(key[0]) == "r" { // Relation, add here
					result += fmt.Sprintf("flatten(tree_%v[**].%v), ", len(tree.Trees)-1, key)
				}
			}
		}
		result += "[])\nRETURN {\"vertices\":nodes,\"edges\":edges}"
	}
	tripleMap := make([]byte, 0)
	return &result, &tripleMap, nil
}

/*
createSubQuery generates a query based on the json file provided
	JSONQuery: *entity.IncomingQueryJSON, this is a parsedJSON struct holding all the data needed to form a query,
	tree: []entity.TreeElement, all the chains in the current query, in order,
	topPill: entity.QueryGenericStruct, an element, either node or groupby, which has only one relation attached, from which the chain can be started,
	treeID: int, the index of the current chain in the tree,
	entityMap: map[int]int, a map to convert entity IDs to their indices in the JSONQuery.Entities
	Return: (error, string, map[string]string), an error, a string containing the corresponding AQL query and a map containing all nodes, edges and groupbys accessible in the current treeElement
*/
func createSubQuery(JSONQuery *entity.IncomingQueryJSON, tree []entity.TreeElement, topPill entity.QueryGenericStruct, treeID int, entityMap map[int]int) (error, string, map[string]string) {
	variables := make(map[string]string) // Variables is a map of all available nodes, edges and groupbys which have to be returned
	var newPill string
	thisName := "tree_" + strconv.Itoa(treeID)
	if topPill.Type == "entity" {
		newPill = fmt.Sprintf("e_%v", topPill.ID)
		variables[fmt.Sprintf("e%v", topPill.ID)] = thisName
	} else {
		newPill = fmt.Sprintf("g_%v", topPill.ID)
		variables[fmt.Sprintf("gb%v", topPill.ID)] = thisName
	}
	var output string
	if topPill.Type == "entity" {
		output = createLetFor(thisName, newPill, JSONQuery.Entities[entityMap[topPill.ID]].Name, 0)
	} else {
		output = createLetFor(thisName, newPill, "in"+strconv.Itoa(topPill.ID), 0)
	}
	if topPill.Type == "entity" {
		for constraint := range JSONQuery.Entities[entityMap[topPill.ID]].Constraints {
			output += createFilter(JSONQuery.Entities[entityMap[topPill.ID]].Constraints[constraint], newPill, true)
		}
	}
	// Recurse over the rest of the chain
	err, subQuery, subName, subvariables := createSubQueryRecurse(JSONQuery, tree, 0, topPill, 1, entityMap)
	if err != nil {
		return err, "", variables
	}
	subNames := []string{subName}
	for key, value := range subvariables {
		variables[key] = value
	}
	output += subQuery
	output += createZeroFilter(append(subNames, newPill))
	output += createReturn(variables, thisName)
	output += ")\n"

	for key := range variables {
		variables[key] = thisName
	}

	return nil, output, variables
}

/*
createSubQueryRecurse generates a query based on the json file provided
	JSONQuery: *entity.IncomingQueryJSON, this is a parsedJSON struct holding all the data needed to form a query,
	tree: []entity.TreeElement, all the chains in the current query, in order,
	currentindex: int, the index of the current element-relation-element triple in the tree
	topPill: entity.QueryGenericStruct, an element, either node or groupby, which has only one relation attached, from which the chain can be started,
	indentindex: int, the amount of indents needed in the current subquery
	entityMap: map[int]int, a map to convert entity IDs to their indices in the JSONQuery.Entities
	Return: (error, string, string, map[string]string), an error, a string containing the corresponding AQL query, a string containing the name of the newly added element to the tree, and a map containing all nodes, edges and groupbys accessible in the current treeElement
*/
func createSubQueryRecurse(JSONQuery *entity.IncomingQueryJSON, tree []entity.TreeElement, currentindex int, topPill entity.QueryGenericStruct, indentindex int, entityMap map[int]int) (error, string, string, map[string]string) {
	currentTree := tree[currentindex]
	var newPillName string
	var thisName string
	variables := make(map[string]string)
	err, newPill := getTreeNewNode(currentTree, tree, topPill)
	if err != nil {
		return err, "", "", variables
	}
	if newPill.Type == "entity" {
		newPillName = fmt.Sprintf("e_%v", newPill.ID)
		thisName = fmt.Sprintf("e%v", newPill.ID)
	} else {
		newPillName = fmt.Sprintf("g_%v", newPill.ID)
		thisName = fmt.Sprintf("gb%v", newPill.ID)
	}
	variables[thisName] = thisName
	variables["r"+strconv.Itoa(currentTree.Self.Rel.ID)] = thisName
	output := ""
	if newPill.Type == "entity" {
		output += fixIndent(createLetFor(thisName, newPillName, JSONQuery.Entities[entityMap[newPill.ID]].Name, indentindex), indentindex)
	} else {
		output += fixIndent(createLetFor(thisName, newPillName, "in"+strconv.Itoa(newPill.ID), indentindex), indentindex)
	}
	output += fixIndent(fmt.Sprintf("\tFOR r%v IN %v\n", currentTree.Self.Rel.ID, currentTree.Self.Rel.Name), indentindex)
	if newPill.Type == "entity" {
		for constraint := range JSONQuery.Entities[entityMap[newPill.ID]].Constraints {
			output += fixIndent(createFilter(JSONQuery.Entities[entityMap[newPill.ID]].Constraints[constraint], newPillName, true), indentindex)
		}
	}
	for constraint := range currentTree.Self.Rel.Constraints {
		output += fixIndent(createFilter(currentTree.Self.Rel.Constraints[constraint], fmt.Sprintf("r%v", currentTree.Self.Rel.ID), true), indentindex)
	}
	output += fixIndent(getRelationFilter(currentTree), indentindex)
	var subNames []string
	for i := range currentTree.Children {
		err, subQuery, subName, subvariables := createSubQueryRecurse(JSONQuery, tree, currentTree.Children[i], topPill, indentindex+1, entityMap)
		if err != nil {
			return err, "", "", variables
		}
		output += subQuery
		subNames = append(subNames, subName)
		for key, value := range subvariables {
			variables[key] = value
		}
	}
	output += fixIndent(createZeroFilter(append(subNames, newPillName, fmt.Sprintf("r%v", currentTree.Self.Rel.ID))), indentindex)
	output += fixIndent(createReturn(variables, thisName), indentindex)
	output += fixIndent(")\n", indentindex)

	for key := range variables {
		variables[key] = thisName
	}

	return nil, output, thisName, variables
}

/*
createGroupBy generates a group by query based on the json file provided
	element: entity.QueryGroupByStruct, the group by for which the query has to be generated
	JSONQuery: *entity.IncomingQueryJSON, this is a parsedJSON struct holding all the data needed to form a query,
	relationMap: map[int]int, a map to convert relation IDs to their indices in the JSONQuery.Relations
	entityMap: map[int]int, a map to convert entity IDs to their indices in the JSONQuery.Entities
	groupByMap: map[int]int, a map to convert groupby IDs to their indices in the JSONQuery.GroupBys
	variables: map[string]string, a map containing the elements, and which tree to find them in
	Return: string, the generated AQL subquery for the group by.
*/
func createGroupBy(element entity.QueryGroupByStruct, JSONQuery *entity.IncomingQueryJSON, relationMap map[int]int, entityMap map[int]int, groupByMap map[int]int, variables map[string]string) string {
	result := getTupleVar(element, JSONQuery, relationMap, entityMap, groupByMap, variables)
	thisname := fmt.Sprintf("g%v", element.ID)
	tuplename := fmt.Sprintf("gt%v", element.ID)
	result += createLetFor(thisname, "x", tuplename, 0)
	result += createCollect(element)
	for _, constraint := range element.Constraints {
		result += createFilter(constraint, "variable_0", false)
	}
	result += "\tRETURN {\n\t_id: c,\n\tmodifier: variable_0\n\t}\n)\n"
	return result
}

/*
getTupleVar generates the AQL query to get tuple variables (all the groups and all the bys) for a group by
	element: entity.QueryGroupByStruct, the group by for which the query has to be generated
	JSONQuery: *entity.IncomingQueryJSON, this is a parsedJSON struct holding all the data needed to form a query,
	relationMap: map[int]int, a map to convert relation IDs to their indices in the JSONQuery.Relations
	entityMap: map[int]int, a map to convert entity IDs to their indices in the JSONQuery.Entities
	groupByMap: map[int]int, a map to convert groupby IDs to their indices in the JSONQuery.GroupBys
	variables: map[string]string, a map containing the elements, and which tree to find them in
	Return: string, the generated AQL tuple query for the group by.
*/
func getTupleVar(element entity.QueryGroupByStruct, JSONQuery *entity.IncomingQueryJSON, relationMap map[int]int, entityMap map[int]int, groupByMap map[int]int, variables map[string]string) string {
	result := ""
	thisname := fmt.Sprintf("gt%v", element.ID)
	result += createLetFor(thisname, "x", JSONQuery.Relations[relationMap[element.RelationID]].Name, 0)
	var relationToName string
	if JSONQuery.Relations[relationMap[element.RelationID]].ToType == "entity" {
		relationToName = JSONQuery.Entities[entityMap[JSONQuery.Relations[relationMap[element.RelationID]].ToID]].Name
	} else {
		relationToName = getGroupByByName(JSONQuery, JSONQuery.GroupBys[groupByMap[JSONQuery.Relations[relationMap[element.RelationID]].ToID]], entityMap, groupByMap)
	}
	if getGroupByByName(JSONQuery, element, entityMap, groupByMap) == relationToName { // If by is the to
		result += createSubVariable("variable_0", "variable_1", "_id", "x._to", element.ByAttribute, element, variables, false)
		result += createSubVariable("variable_2", "variable_3", "_id", "x._from", element.GroupAttribute, element, variables, true)
	} else {
		result += createSubVariable("variable_0", "variable_1", "_id", "x._from", element.ByAttribute, element, variables, false)
		result += createSubVariable("variable_2", "variable_3", "_id", "x._to", element.GroupAttribute, element, variables, true)
	}
	result += "\tFILTER variable_1 != NULL AND variable_3 != NULL\n\tRETURN {\n\t\t\"group\": variable_1, \n\t\t\"by\": variable_3\n\t}\n)\n"
	return result
}

/*
createSubVariable generates the AQL query to get all group or by variables for a group by
	variableName: string, the temporary name the variable will get
	variableName2: string, the name the variable will get
	filter1: string, the attribute to be equal to the relation to/from
	filter2: string, the relation to/from that has to be equal to the attribute
	returnValue: string, the attribute to be returned and used in the group by later on
	groupBy: entity.QueryGroupByStruct, the groupBy which has to be generated
	variables: map[string]string, a map containing the elements, and which tree to find them in
	group: bool, whether the current subvariable will be the group or the by variable (true == group variable)
	Return: string, the generated AQL subvariable query for the group by.
*/
func createSubVariable(variableName string, variableName2 string, filter1 string, filter2 string, returnValue string, groupBy entity.QueryGroupByStruct, variables map[string]string, group bool) string {
	var prefix string
	if group {
		if groupBy.GroupType == "entity" {
			prefix = "e"
		} else {
			prefix = "gb"
		}
	} else {
		if groupBy.ByType == "entity" {
			prefix = "e"
		} else {
			prefix = "gb"
		}
	}
	result := "\tLET " + variableName + " = (\n"
	result += "\t\tLET tmp = union_distinct("
	if group {
		for _, ID := range groupBy.GroupID {
			result += fmt.Sprintf("flatten(%v[**].%v), ", variables[prefix+strconv.Itoa(ID)], prefix+strconv.Itoa(ID))
		}
	} else {
		for _, ID := range groupBy.ByID {
			result += fmt.Sprintf("flatten(%v[**].%v), ", variables[prefix+strconv.Itoa(ID)], prefix+strconv.Itoa(ID))
		}
	}
	result += "[])\n"
	result += "\t\t\tFOR y IN tmp\n"
	return result + "\t\t\tFILTER y." + filter1 + " == " + filter2 + "\n\t\t\tRETURN y." + returnValue + "\n\t) " +
		"\n\tLET " + variableName2 + " = " + variableName + "[0] \n"
}

/*
createCollect generates the AQL query to get all group or by variables for a group by
	element: entity.QueryGroupByStruct, the groupBy which has to be generated
	Return: string, the generated AQL collect query for the group by.
*/
func createCollect(element entity.QueryGroupByStruct) string {
	return "\tCOLLECT c = x.group INTO groups = x.by\n\t" +
		"LET variable_0 = " + element.AppliedModifier + "(groups) \n"
}

/*
getRelationFilter generates the AQL query to get all group or by variables for a group by
	currentTree: entity.TreeElement, the treeElement for which the filter has to be generated
	Return: string, the generated AQL filter to generate the to and from
*/
func getRelationFilter(currentTree entity.TreeElement) string {
	output := fmt.Sprintf("\tFILTER r%v._from == ", currentTree.Self.Rel.ID)
	if currentTree.Self.FromNode.Name != "" {
		output += fmt.Sprintf("e_%v._id AND r%v._to == ", currentTree.Self.FromNode.ID, currentTree.Self.Rel.ID)
	} else {
		output += fmt.Sprintf("g_%v._id AND r%v._to == ", currentTree.Self.FromGroupBy.ID, currentTree.Self.Rel.ID)
	}
	if currentTree.Self.ToNode.Name != "" {
		output += fmt.Sprintf("e_%v._id\n", currentTree.Self.ToNode.ID)
	} else {
		output += fmt.Sprintf("g_%v._id\n", currentTree.Self.ToGroupBy.ID)
	}
	return output
}

/*
getTreeNewNode finds the other element of an element-relation-element triple, given the parent of the current triple (which always has one element in common)
	currentTree: entity.TreeElement, the treeElement in which the other element has to be found
	tree: []entity.TreeElement, the list of all treeElements, in which the parent can be found
	topPill: entity.QueryGenericStruct, the topPill of the current chain
	Return: (error, entity.QueryGenericStruct), an error and the new pill
*/
func getTreeNewNode(currentTree entity.TreeElement, tree []entity.TreeElement, topPill entity.QueryGenericStruct) (error, entity.QueryGenericStruct) { // Finds either from or to pill that isn't the same as topPill
	var retval entity.QueryGenericStruct
	if currentTree.Parent < 0 { // Try to find the not top pill
		if topPill.Type == "entity" { // top pill is an entity
			if currentTree.Self.FromNode.Name == "" {
				retval.ID = currentTree.Self.FromGroupBy.ID
				retval.Type = "groupBy"
				return nil, retval
			} else if currentTree.Self.ToNode.Name == "" {
				retval.ID = currentTree.Self.ToGroupBy.ID
				retval.Type = "groupBy"
				return nil, retval
			} else if currentTree.Self.FromNode.ID == topPill.ID {
				retval.ID = currentTree.Self.ToNode.ID
				retval.Type = "entity"
				return nil, retval
			} else {
				retval.ID = currentTree.Self.FromNode.ID
				retval.Type = "entity"
				return nil, retval
			}
		} else {
			if currentTree.Self.FromNode.Name != "" {
				retval.ID = currentTree.Self.FromNode.ID
				retval.Type = "entity"
				return nil, retval
			} else if currentTree.Self.ToNode.Name != "" {
				retval.ID = currentTree.Self.ToNode.ID
				retval.Type = "entity"
				return nil, retval
			} else if currentTree.Self.FromGroupBy.ID == topPill.ID {
				retval.ID = currentTree.Self.ToGroupBy.ID
				retval.Type = "groupBy"
				return nil, retval
			} else {
				retval.ID = currentTree.Self.FromGroupBy.ID
				retval.Type = "groupBy"
				return nil, retval
			}
		}
	} else {
		parentTree := tree[currentTree.Parent]
		// Try to return to
		if currentTree.Self.ToNode.Name != "" {
			retval.ID = currentTree.Self.ToNode.ID
			retval.Type = "entity"
		} else {
			retval.ID = currentTree.Self.ToGroupBy.ID
			retval.Type = "groupBy"
		}
		if currentTree.Self.FromNode.Name != "" {
			if parentTree.Self.ToNode.Name != "" && parentTree.Self.ToNode.ID == currentTree.Self.FromNode.ID { // my from his to
				return nil, retval
			} else if parentTree.Self.FromNode.Name != "" && parentTree.Self.FromNode.ID == currentTree.Self.FromNode.ID { // my from his from
				return nil, retval
			}
		} else {
			if parentTree.Self.ToNode.Name == "" && parentTree.Self.ToGroupBy.ID == currentTree.Self.FromGroupBy.ID { // my from his to
				return nil, retval
			} else if parentTree.Self.FromNode.Name == "" && parentTree.Self.FromGroupBy.ID == currentTree.Self.FromGroupBy.ID { // my from his from
				return nil, retval
			}
		}
		// Try to return from
		if currentTree.Self.FromNode.Name != "" {
			retval.ID = currentTree.Self.FromNode.ID
			retval.Type = "entity"
		} else {
			retval.ID = currentTree.Self.FromGroupBy.ID
			retval.Type = "groupBy"
		}
		if currentTree.Self.ToNode.Name != "" {
			if parentTree.Self.ToNode.Name != "" && parentTree.Self.ToNode.ID == currentTree.Self.ToNode.ID { // my to his to
				return nil, retval
			} else if parentTree.Self.FromNode.Name != "" && parentTree.Self.FromNode.ID == currentTree.Self.ToNode.ID { // my to his from
				return nil, retval
			}
		} else {
			if parentTree.Self.ToNode.Name == "" && parentTree.Self.ToGroupBy.ID == currentTree.Self.ToGroupBy.ID { // my to his to
				return nil, retval
			} else if parentTree.Self.FromNode.Name == "" && parentTree.Self.FromGroupBy.ID == currentTree.Self.ToGroupBy.ID { // my to his from
				return nil, retval
			}
		}
	}
	return errors.New("No overlapping nodes between currentTree and parentTree"), retval
}

/*
createLetFor generates the AQL let for
	variableName: string, the name which the let will be given
	forName: string, the name the for will be given
	enumerableName: string, the enumerable the for will iterate over
	indentindex: int, the amount of indents needed in the current subquery
	Return: string, the AQL let for
*/
func createLetFor(variableName string, forName string, enumerableName string, indentindex int) string {
	output := "LET " + variableName + " = (\n"
	for i := 0; i < indentindex; i++ {
		output += "\t"
	}
	output += "\tFOR " + forName + " IN " + enumerableName + "\n"
	return output
}

/*
createFilter generates the AQL query for a constraint
	constraint: entity.QueryConstraintStruct, the constraint that has to be generated
	filtered: string, the name of the element the constraint applies to
	includeAttribute: bool, whether the constraint attribute needs to be used in the filter
	Return: string, the AQL query for a constraint
*/
func createFilter(constraint entity.QueryConstraintStruct, filtered string, includeAttribute bool) string {
	output := "\tFILTER " + filtered
	if includeAttribute {
		output += "." + constraint.Attribute
	}
	output += " " + wordsToLogicalSign(constraint)
	if constraint.DataType == "string" {
		output += " \""
	} else {
		output += " "
	}
	if constraint.MatchType == "contains" {
		output += "%"
	}
	output += constraint.Value
	if constraint.MatchType == "contains" {
		output += "%"
	}
	if constraint.DataType == "string" {
		output += "\" "
	} else {
		output += " "
	}
	output += " \n"
	return output
}

/*
createZeroFilter generates the AQL query for a filter of length zero for each subName
	subNames: []string, all the elements which have to be filtered for length greater than zero
	Return: string, the AQL query for a filter of length zero for each subName
*/
func createZeroFilter(subNames []string) string {
	output := "\tFILTER"
	for i := range subNames {
		output += fmt.Sprintf(" length(%v) != 0", subNames[i])
		if i < len(subNames)-1 {
			output += " AND"
		}
	}
	output += "\n"
	return output
}

/*
createReturn generates the AQL query for a return of a treeElement
	variables: map[string]string, a map containing the elements, and which subtree to find them in
	thisName: string, the name of the current subTree
	Return: string, the AQL query for a return of a treeElement
*/
func createReturn(variables map[string]string, thisName string) string {
	output := "\tRETURN {"
	for varName, treeName := range variables {
		if treeName == thisName {
			if varName[0] == 'r' { // entity
				output += fmt.Sprintf("\"%v\": union_distinct([%v], []), ", varName, varName)
			} else if varName[0] == 'e' { // entity
				output += fmt.Sprintf("\"%v\": union_distinct([e_%v], []), ", varName, string(varName[1:])) // varName[1] is the ID of the entity
			} else { // group by
				output += fmt.Sprintf("\"%v\": union_distinct([g_%v], []), ", varName, string(varName[2:])) // varName[2] is the ID of the group by
			}
		} else {
			output += fmt.Sprintf("\"%v\": union_distinct(flatten(%v[**].%v), []), ", varName, treeName, varName)
		}
	}
	output = output[:len(output)-2]
	output += "}\n"
	return output
}

/*
getGroupByByName recurses over group bys until the by is an entity, for which it then returns the name
	JSONQuery: *entity.IncomingQueryJSON, this is a parsed JSON struct holding all the data needed to form a query
	groupBy: entity.QueryGroupByStruct, the groupBy for which the byname has to be generated
	entityMap: map[int]int, a map to convert entity IDs to their indices in the JSONQuery.Entities
	groupByMap: map[int]int, a map to convert groupby IDs to their indices in the JSONQuery.GroupBys
	Return: string, the name of the groupby by element
*/
func getGroupByByName(JSONQuery *entity.IncomingQueryJSON, groupBy entity.QueryGroupByStruct, entityMap map[int]int, groupByMap map[int]int) string {
	if groupBy.ByType == "entity" {
		return JSONQuery.Entities[entityMap[groupBy.ByID[0]]].Name
	} else {
		return getGroupByByName(JSONQuery, JSONQuery.GroupBys[groupByMap[groupBy.ByID[0]]], entityMap, groupByMap)
	}
}

/*
wordsToLogicalSign converts the MatchType in a constraint to the logical signs used in AQL
	element: entity.QueryConstraintStruct, the constraint for which the mathtype must be converted
	Return: string, the converted matchtype
*/
func wordsToLogicalSign(element entity.QueryConstraintStruct) string {
	var match string
	switch element.DataType {
	case "string":
		switch element.MatchType {
		case "NEQ":
			match = "!="
		case "contains":
			match = "LIKE"
		case "excludes":
			match = "NOT LIKE"
		default: //EQ
			match = "=="
		}
	case "int", "float":
		switch element.MatchType {
		case "NEQ":
			match = "!="
		case "GT":
			match = ">"
		case "LT":
			match = "<"
		case "GET":
			match = ">="
		case "LET":
			match = "<="
		default: //EQ
			match = "=="
		}
	default: /*bool*/
		switch element.MatchType {
		case "NEQ":
			match = "!="
		default: //EQ
			match = "=="
		}
	}
	return match
}

/*
fixIndent adds a certain amount of indents to a string
	input: string, the string for which the tabs must be prepended (the input must be one line only, otherwise the next lines won't have proper indentation)
	indentCount: int, the amount of tabs to be added
	Return: string, the input with prepended tabs
*/
func fixIndent(input string, indentCount int) string {
	output := ""
	for i := 0; i < indentCount; i++ {
		output += "\t"
	}
	output += input
	return output
}

/*
defaultReturn generates a default AQL query
	Return: string, the default AQL query
*/
func defaultReturn() (*string, *[]byte, error) {
	defaultReturn := `LET nodes = first(RETURN UNION_DISTINCT([],[]))
		LET edges = first(RETURN UNION_DISTINCT([],[]))
		RETURN {"vertices":nodes, "edges":edges }`
	defaultMap := make([]byte, 0)
	return &defaultReturn, &defaultMap, nil
}
