/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package aql

// import (
// 	"encoding/json"
// 	"testing"

// 	"git.science.uu.nl/graphpolaris/query-conversion/entity"
// )

// /*
// Benchmarks converting an empty query
// 	t: *testing.T, makes go recognise this as a test
// */
// func BenchmarkConvertEmptyQuery(b *testing.B) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	query := []byte(`{
// 		"return": {
// 			"entities": [],
// 			"relations": []
// 		},
// 		"entities": [],
// 		"relations": [],
// 		"limit": 5000
// 	}`)

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(query, &JSONQuery)

// 	b.ResetTimer()

// 	for i := 0; i < b.N; i++ {
// 		service.ConvertQuery(&JSONQuery)
// 	}
// }

// /*
// Benchmarks converting a one attribute query
// 	t: *testing.T, makes go recognise this as a test
// */
// func BenchmarkConvertOneAttributeQuery(b *testing.B) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	query := []byte(`{
// 		"return": {
// 			"entities": [
// 				0
// 			],
// 			"relations": []
// 		},
// 		"entities": [
// 			{
// 				"type": "airports",
// 				"constraints": [
// 					{
// 						"attribute": "state",
// 						"value": "HI",
// 						"dataType": "string",
// 						"matchType": "exact"
// 					}
// 				]
// 			}
// 		],
// 		"relations": [],
// 		"limit": 5000
// 	}`)

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(query, &JSONQuery)

// 	b.ResetTimer()

// 	for i := 0; i < b.N; i++ {
// 		service.ConvertQuery(&JSONQuery)
// 	}
// }

// /*
// Benchmarks converting a two relation query
// 	t: *testing.T, makes go recognise this as a test
// */
// func BenchmarkConvertTwoRelationQuery(b *testing.B) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	query := []byte(`{
// 		"return": {
// 			"entities": [
// 				0,
// 				1,
// 				2
// 			],
// 			"relations": [
// 				0,
// 				1
// 			]
// 		},
// 		"entities": [
// 			{
// 				"type": "airports",
// 				"constraints": [
// 					{
// 						"attribute": "city",
// 						"value": "New York",
// 						"dataType": "string",
// 						"matchType": "exact"
// 					}
// 				]
// 			},
// 			{
// 				"type": "airports",
// 				"constraints": [
// 					{
// 						"attribute": "city",
// 						"value": "San Francisco",
// 						"dataType": "string",
// 						"matchType": "exact"
// 					}
// 				]
// 			},
// 			{
// 				"type": "airports",
// 				"constraints": [
// 					{
// 						"attribute": "state",
// 						"value": "HI",
// 						"dataType": "string",
// 						"matchType": "exact"
// 					}
// 				]
// 			}
// 		],
// 		"relations": [
// 			{
// 				"type": "flights",
// 				"depth": {
// 					"min": 1,
// 					"max": 3
// 				},
// 				"entityFrom": 2,
// 				"entityTo": 1,
// 				"constraints": [
// 					{
// 						"attribute": "Day",
// 						"value": "15",
// 						"dataType": "int",
// 						"matchType": "EQ"
// 					}
// 				]
// 			},
// 			{
// 				"type": "flights",
// 				"depth": {
// 					"min": 1,
// 					"max": 1
// 				},
// 				"entityFrom": 0,
// 				"entityTo": -1,
// 				"constraints": []
// 			}
// 		],
// 		"limit": 5000
// 	}`)

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(query, &JSONQuery)

// 	b.ResetTimer()

// 	for i := 0; i < b.N; i++ {
// 		service.ConvertQuery(&JSONQuery)
// 	}
// }
