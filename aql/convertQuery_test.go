/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package aql

// import (
// 	"encoding/json"
// 	"errors"
// 	"regexp"
// 	"strings"
// 	"testing"

// 	"git.science.uu.nl/graphpolaris/query-conversion/entity"
// 	"github.com/stretchr/testify/assert"
// )

// /*
// Tests an empty query
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestEmptyQueryConversion(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(emptyQuery, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &EmptyQueryResult)
// }

// /*
// Tests two entities (two types) without a filter
// Query description: Give me all parties connected to their respective parliament members
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestTwoEntitiesNoFilter(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(TwoEntitiesNoFilter, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &TwoEntitiesNoFilterResult)
// }

// /*
// Tests two entities (two types) with one entity filter
// Query description: Give me all parties, with less than 10 seats, connected to their respective parliament members
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestTwoEntitiesOneEntityFilter(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(TwoEntitiesOneEntityFilter, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &TwoEntitiesOneEntityFilterResult)
// }

// /*
// Tests two entities (two types) with two entity filters
// Query description: Give me all parties, with less than 10 seats, connected to their respective parliament members, who are more than 45 years old
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestTwoEntitiesTwoEntityFilters(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(TwoEntitiesTwoEntityFilters, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &TwoEntitiesTwoEntityFiltersResult)
// }

// /*
// Tests three entities (three types) without a filter
// Query description: Give me all parties, connected to their respective parliament members, who are then connected to the resolutions they submitted
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestThreeEntitiesNoFilter(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(ThreeEntitiesNoFilter, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &ThreeEntitiesNoFilterResult)
// }

// /*
// Tests three entities (three types) with one entity filter
// Query description: Give me all parties, connected to their respective parliament members, whose name has "Geert" in it (this results in only "Geert Wilders"), who are/is then connected to the resolutions they submitted
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestThreeEntitiesOneEntityFilter(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(ThreeEntitiesOneEntityFilter, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &ThreeEntitiesOneEntityFilterResult)
// }

// /*
// Tests three entities (three types) with two entity filters
// Query description: Give me all parties, connected to their respective parliament members, whose name has "Geert" in it (this results in only "Geert Wilders"), who are/is then connected to the resolutions they submitted, but only those submitted in May
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestThreeEntitiesTwoEntityFilters(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(ThreeEntitiesTwoEntityFilters, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &ThreeEntitiesTwoEntityFiltersResult)
// }

// /*
// Tests five entities (three types) with four entity filters
// Query description: Give me all parties, which have less than 10 seats, connected to their respective parliament members, whose name has "A" in it, who are/is then connected to the resolutions they submitted, but only those submitted in May, which are then also connected to all other persons who were part of that submission, who are part of the "VVD"
// Translator's note: This returns a member of the PvdA who submitted a motion alongside a member of the VVD
// t: *testing.T, makes go recognise this as a test
// */
// func TestFiveEntitiesFourEntityFilters(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(FiveEntitiesFourEntityFilters, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &FiveEntitiesFourEntityFiltersResult)
// }

// /*
// Tests five entities (four types) with three entity filters and one junction
// Query description: Give me all parties, with less than 10 seats, connected to their respective parliament members, who are then connected to the resolutions they submitted, but only those submitted in May, and connected to the comissions they're in, which are then connected to all of their members, but only those with "Geert" in their name (resulting in only "Geert Wilders")
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestSingleJunctionFiveEntitiesThreeEntityFilters(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(SingleJunctionFiveEntitiesThreeEntityFilters, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &SingleJunctionFiveEntitiesThreeEntityFiltersResult)
// }

// /*
// Tests nine entities (four types) with three entity filters and two junctions
// Query description: Give me all parties, with less than 10 seats, connected to their respective parliament members, who are then connected to the resolutions they submitted, but only those submitted in May, and connected to the comissions they're in, which are then connected to all of their members, but only those with "Geert" in their name (resulting in only "Geert Wilders"), who is then connected to their submited resolutions and their party, which is connected to all of its members
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestDoubleJunctionNineEntitiesThreeEntityFilters(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(DoubleJunctionNineEntitiesThreeEntityFilters, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &DoubleJunctionNineEntitiesThreeEntityFiltersResult)
// }

// /*
// Tests two entities (one type) with one entity filter and one relation filter
// Query description: Give me all airports, in the state "HI", connected to any other airport by flight, but only the flights on "day" 15
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestTwoEntitiesOneEntityFilterOneRelationFilter(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(TwoEntitiesOneEntityFilterOneRelationFilter, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &TwoEntitiesOneEntityFilterOneRelationFilterResult)
// }

// //TODO
// //FIX THESE TESTS, THEY'RE NOT THAT INTERESTING BUT SHOULD BE FIXED ANYWAY

// /*
// Tests a query with no relation field
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestNoRelationsField(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(NoRelationsField, &JSONQuery)

// 	_, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is an error
// 	assert.Equal(t, errors.New("JSONQuery invalid"), err)
// }

// /*
// Tests a query with a single group by as an endpoint
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestSingleEndPointGroupBy(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(SingleEndPointGroupBy, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &SingleEndPointGroupByResult)
// }

// /*
// Tests a query with a single group by which is connected to a relation
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestSingleGroupBy(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(SingleGroupBy, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &SingleGroupByResult)
// }

// /*
// Tests a query with a group by with multiple inputs which is an endpoint
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestMultipleInputGroupByEndpoint(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(MultipleInputGroupByEndpoint, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &MultipleInputGroupByEndpointResult)
// }

// /*
// Tests a query with a group by with multiple inputs which is connected to a relation
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestMultipleInputGroupBy(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(MultipleInputGroupBy, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &MultipleInputGroupByResult)
// }

// /*
// Tests a query with a group by with a group by as top pill
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestTopGroupBy(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(TopGroupBy, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &TopGroupByResult)
// }

// /*
// Tests a large query with multiple groupbys nested and multiple chained filters and chained subqueries
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestWayTooLargeQuery(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(WayTooLargeQuery, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &WayTooLargeQueryResult)
// }

// /*
// Tests an entity with a lower than -1 in a relation
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestIncorrectRelationFrom(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(IncorrectRelationFrom, &JSONQuery)

// 	_, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is an error
// 	assert.Equal(t, errors.New("JSONQuery invalid"), err)
// }

// func CheckResults(t *testing.T, convertedResult *string, correctConvertedResult *string) {
// 	inTree := false
// 	convertedResultSplit := strings.Split(*convertedResult, "\n")
// 	correctConvertedResultSplit := strings.Split(*correctConvertedResult, "\n")
// 	for x := range convertedResultSplit {
// 		convertedLine := convertedResultSplit[x]
// 		correctLine := correctConvertedResultSplit[x]
// 		regExCleaner := regexp.MustCompile(`\s+`)
// 		convertedCleanedLine := strings.TrimSpace(regExCleaner.ReplaceAllString(convertedLine, " "))
// 		correctCleanedLine := strings.TrimSpace(regExCleaner.ReplaceAllString(correctLine, " "))
// 		if len(correctCleanedLine) > 8 && correctCleanedLine[:8] == "LET tree" {
// 			inTree = true
// 		}
// 		if inTree && len(correctCleanedLine) > 6 && correctCleanedLine[:5] == "LET g" && correctCleanedLine[:6] != "LET gb" {
// 			inTree = false
// 		}
// 		if inTree && len(correctCleanedLine) > 6 && correctCleanedLine[:6] == "RETURN" {
// 			assert.Equal(t, convertedCleanedLine[:8], correctCleanedLine[:8])
// 			assert.Equal(t, convertedCleanedLine[len(convertedCleanedLine)-1:], correctCleanedLine[len(correctCleanedLine)-1:])
// 			convertedReturn := strings.Split(strings.ReplaceAll(convertedCleanedLine[8:][:len(convertedCleanedLine)-9], ", \"", "< \""), "< ")
// 			correctReturn := strings.Split(strings.ReplaceAll(correctCleanedLine[8:][:len(correctCleanedLine)-9], ", \"", "< \""), "< ")
// 			for _, element := range convertedReturn {
// 				assert.Contains(t, correctReturn, element)
// 			}
// 			for _, element := range correctReturn {
// 				assert.Contains(t, convertedReturn, element)
// 			}
// 		} else if inTree && len(correctCleanedLine) > 27 && (correctCleanedLine[:27] == "LET nodes = union_distinct(" || correctCleanedLine[:27] == "LET edges = union_distinct(") {
// 			assert.Equal(t, convertedCleanedLine[:27], correctCleanedLine[:27])
// 			assert.Equal(t, convertedCleanedLine[len(convertedCleanedLine)-3:], correctCleanedLine[len(correctCleanedLine)-3:])
// 			convertedReturn := strings.Split(convertedCleanedLine[27:][:len(convertedCleanedLine)-30], ", ")
// 			correctReturn := strings.Split(correctCleanedLine[27:][:len(correctCleanedLine)-30], ", ")
// 			for _, element := range convertedReturn {
// 				assert.Contains(t, correctReturn, element)
// 			}
// 		} else {
// 			assert.Equal(t, convertedCleanedLine, correctCleanedLine)
// 		}
// 	}
// }

// /*
// Tests two separated chains consisting of 1 relation each
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestSeparatedChainSingleRelationPerChain(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(SeparatedChainSingleRelationPerChain, &JSONQuery)

// 	_, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is an error
// 	assert.Equal(t, errors.New("JSONQuery invalid"), err)
// }

// /*
// Tests two separated chains consisting of 1 relation each
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestSeparatedChainDoubleRelationPerChain(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(SeparatedChainDoubleRelationPerChain, &JSONQuery)

// 	_, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is an error
// 	assert.Equal(t, errors.New("JSONQuery invalid"), err)
// }

// /*
// Tests two separated chains
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestSeperatedChainWithGroupBys(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(SeperatedChainWithGroupBys, &JSONQuery)

// 	_, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is an error
// 	assert.Equal(t, errors.New("JSONQuery invalid"), err)
// }

// /*
// Tests a group by which has different nodes on its group and by
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestDifferentNodesOnGroupBy(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(DifferentNodesOnGroupBy, &JSONQuery)

// 	_, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is an error
// 	assert.Equal(t, errors.New("JSONQuery invalid"), err)
// }

// /*
// Tests a group by which has a modifier which aql doesn't recognise
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestIncorrectGroupByModifier(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(IncorrectGroupByModifier, &JSONQuery)

// 	_, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is an error
// 	assert.Equal(t, errors.New("JSONQuery invalid"), err)
// }

// /*
// Tests a query with nested group bys
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestAllStreamers(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(AllStreamers, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &AllStreamersResult)
// }

// /*
// Tests the string match types
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestStringMatchTypes(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(StringMatchTypes, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &StringMatchTypesResult)
// }

// /*
// Tests a query where a top pill is the to, and is connected to a from group by
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestTopEntityToFromGroupBy(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(TopEntityToFromGroupBy, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &TopEntityToFromGroupByResult)
// }

// /*
// Tests in not equals filter
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestIntNotEquals(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(IntNotEquals, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &IntNotEqualsResult)
// }

// /*
// Tests bool equals filter
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestBoolEqual(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(BoolEqual, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &BoolEqualResult)
// }

// /*
// Tests bool not equals filter
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestBoolNotEqual(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(BoolNotEqual, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &BoolNotEqualResult)
// }

// /*
// Tests a different nested group by which will have to propagate to get group by type
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestGroupNestedGroupBys2(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(NestedGroupBys2, &JSONQuery)

// 	convertedResult, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	// Assert that the result and the expected result are the same
// 	CheckResults(t, convertedResult, &NestedGroupBys2Result)
// }
