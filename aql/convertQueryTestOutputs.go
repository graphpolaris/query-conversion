/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package aql

var EmptyQueryResult = string(`LET nodes = first(RETURN UNION_DISTINCT([],[]))
LET edges = first(RETURN UNION_DISTINCT([],[]))
RETURN {"vertices":nodes, "edges":edges }`)

var TwoEntitiesNoFilterResult = string(`LET tree_0 = (
	FOR e_0 IN parliament
	LET e1 = (
			FOR e_1 IN parties
			FOR r0 IN member_of
			FILTER r0._from == e_0._id AND r0._to == e_1._id
			FILTER length(e_1) != 0 AND length(r0) != 0
			RETURN {"e1": union_distinct([e_1], []), "r0": union_distinct([r0], [])}
	)
	FILTER length(e1) != 0 AND length(e_0) != 0
	RETURN {"e0": union_distinct([e_0], []), "e1": union_distinct(flatten(e1[**].e1), []), "r0": union_distinct(flatten(e1[**].r0), [])}
)
LET nodes = union_distinct(flatten(tree_0[**].e0), flatten(tree_0[**].e1), [])
LET edges = union_distinct(flatten(tree_0[**].r0), [])
RETURN {"vertices":nodes,"edges":edges}`)

var TwoEntitiesOneEntityFilterResult = string(`LET tree_0 = (
	FOR e_0 IN parliament
	LET e1 = (
			FOR e_1 IN parties
			FOR r0 IN member_of
			FILTER e_1.seats < 10
			FILTER r0._from == e_0._id AND r0._to == e_1._id
			FILTER length(e_1) != 0 AND length(r0) != 0
			RETURN {"e1": union_distinct([e_1], []), "r0": union_distinct([r0], [])}
	)
	FILTER length(e1) != 0 AND length(e_0) != 0
	RETURN {"e0": union_distinct([e_0], []), "e1": union_distinct(flatten(e1[**].e1), []), "r0": union_distinct(flatten(e1[**].r0), [])}
)
LET nodes = union_distinct(flatten(tree_0[**].e0), flatten(tree_0[**].e1), [])
LET edges = union_distinct(flatten(tree_0[**].r0), [])
RETURN {"vertices":nodes,"edges":edges}`)

var TwoEntitiesTwoEntityFiltersResult = string(`LET tree_0 = (
	FOR e_0 IN parliament
	FILTER e_0.age > 45
	LET e1 = (
			FOR e_1 IN parties
			FOR r0 IN member_of
			FILTER e_1.seats < 10
			FILTER r0._from == e_0._id AND r0._to == e_1._id
			FILTER length(e_1) != 0 AND length(r0) != 0
			RETURN {"e1": union_distinct([e_1], []), "r0": union_distinct([r0], [])}
	)
	FILTER length(e1) != 0 AND length(e_0) != 0
	RETURN {"e0": union_distinct([e_0], []), "e1": union_distinct(flatten(e1[**].e1), []), "r0": union_distinct(flatten(e1[**].r0), [])}
)
LET nodes = union_distinct(flatten(tree_0[**].e0), flatten(tree_0[**].e1), [])
LET edges = union_distinct(flatten(tree_0[**].r0), [])
RETURN {"vertices":nodes,"edges":edges}`)

var ThreeEntitiesNoFilterResult = string(`LET tree_0 = (
	FOR e_1 IN parties
	LET e0 = (
			FOR e_0 IN parliament
			FOR r0 IN member_of
			FILTER r0._from == e_0._id AND r0._to == e_1._id
			LET e2 = (
					FOR e_2 IN resolutions
					FOR r1 IN submits
					FILTER r1._from == e_0._id AND r1._to == e_2._id
					FILTER length(e_2) != 0 AND length(r1) != 0
					RETURN {"e2": union_distinct([e_2], []), "r1": union_distinct([r1], [])}
			)
			FILTER length(e2) != 0 AND length(e_0) != 0 AND length(r0) != 0
			RETURN {"e0": union_distinct([e_0], []), "r0": union_distinct([r0], []), "e2": union_distinct(flatten(e2[**].e2), []), "r1": union_distinct(flatten(e2[**].r1), [])}
	)
	FILTER length(e0) != 0 AND length(e_1) != 0
	RETURN {"r0": union_distinct(flatten(e0[**].r0), []), "e2": union_distinct(flatten(e0[**].e2), []), "r1": union_distinct(flatten(e0[**].r1), []), "e1": union_distinct([e_1], []), "e0": union_distinct(flatten(e0[**].e0), [])}
)
LET nodes = union_distinct(flatten(tree_0[**].e1), flatten(tree_0[**].e0), flatten(tree_0[**].e2), [])
LET edges = union_distinct(flatten(tree_0[**].r0), flatten(tree_0[**].r1), [])
RETURN {"vertices":nodes,"edges":edges}`)

var ThreeEntitiesOneEntityFilterResult = string(`LET tree_0 = (
	FOR e_1 IN parties
	LET e0 = (
			FOR e_0 IN parliament
			FOR r0 IN member_of
			FILTER e_0.name LIKE "%Geert%"
			FILTER r0._from == e_0._id AND r0._to == e_1._id
			LET e2 = (
					FOR e_2 IN resolutions
					FOR r1 IN submits
					FILTER r1._from == e_0._id AND r1._to == e_2._id
					FILTER length(e_2) != 0 AND length(r1) != 0
					RETURN {"e2": union_distinct([e_2], []), "r1": union_distinct([r1], [])}
			)
			FILTER length(e2) != 0 AND length(e_0) != 0 AND length(r0) != 0
			RETURN {"e2": union_distinct(flatten(e2[**].e2), []), "r1": union_distinct(flatten(e2[**].r1), []), "e0": union_distinct([e_0], []), "r0": union_distinct([r0], [])}
	)
	FILTER length(e0) != 0 AND length(e_1) != 0
	RETURN {"e1": union_distinct([e_1], []), "e0": union_distinct(flatten(e0[**].e0), []), "e2": union_distinct(flatten(e0[**].e2), []), "r0": union_distinct(flatten(e0[**].r0), []), "r1": union_distinct(flatten(e0[**].r1), [])}
)
LET nodes = union_distinct(flatten(tree_0[**].e1), flatten(tree_0[**].e0), flatten(tree_0[**].e2), [])
LET edges = union_distinct(flatten(tree_0[**].r0), flatten(tree_0[**].r1), [])
RETURN {"vertices":nodes,"edges":edges}`)

var ThreeEntitiesTwoEntityFiltersResult = string(`LET tree_0 = (
	FOR e_1 IN parties
	LET e0 = (
			FOR e_0 IN parliament
			FOR r0 IN member_of
			FILTER e_0.name LIKE "%Geert%"
			FILTER r0._from == e_0._id AND r0._to == e_1._id
			LET e2 = (
					FOR e_2 IN resolutions
					FOR r1 IN submits
					FILTER e_2.date LIKE "%mei%"
					FILTER r1._from == e_0._id AND r1._to == e_2._id
					FILTER length(e_2) != 0 AND length(r1) != 0
					RETURN {"e2": union_distinct([e_2], []), "r1": union_distinct([r1], [])}
			)
			FILTER length(e2) != 0 AND length(e_0) != 0 AND length(r0) != 0
			RETURN {"e0": union_distinct([e_0], []), "r0": union_distinct([r0], []), "e2": union_distinct(flatten(e2[**].e2), []), "r1": union_distinct(flatten(e2[**].r1), [])}
	)
	FILTER length(e0) != 0 AND length(e_1) != 0
	RETURN {"e1": union_distinct([e_1], []), "e0": union_distinct(flatten(e0[**].e0), []), "e2": union_distinct(flatten(e0[**].e2), []), "r0": union_distinct(flatten(e0[**].r0), []), "r1": union_distinct(flatten(e0[**].r1), [])}
)
LET nodes = union_distinct(flatten(tree_0[**].e1), flatten(tree_0[**].e0), flatten(tree_0[**].e2), [])
LET edges = union_distinct(flatten(tree_0[**].r0), flatten(tree_0[**].r1), [])
RETURN {"vertices":nodes,"edges":edges}`)

var FiveEntitiesFourEntityFiltersResult = string(`LET tree_0 = (
	FOR e_1 IN parties
	FILTER e_1.seats < 10
	LET e0 = (
			FOR e_0 IN parliament
			FOR r0 IN member_of
			FILTER e_0.name LIKE "%A%"
			FILTER r0._from == e_0._id AND r0._to == e_1._id
			LET e2 = (
					FOR e_2 IN resolutions
					FOR r1 IN submits
					FILTER e_2.date LIKE "%mei%"
					FILTER r1._from == e_0._id AND r1._to == e_2._id
					LET e3 = (
							FOR e_3 IN parliament
							FOR r2 IN submits
							FILTER r2._from == e_3._id AND r2._to == e_2._id
							LET e4 = (
									FOR e_4 IN parties
									FOR r3 IN member_of
									FILTER e_4.name == "Volkspartij voor Vrijheid en Democratie"
									FILTER r3._from == e_3._id AND r3._to == e_4._id
									FILTER length(e_4) != 0 AND length(r3) != 0
									RETURN {"e4": union_distinct([e_4], []), "r3": union_distinct([r3], [])}
							)
							FILTER length(e4) != 0 AND length(e_3) != 0 AND length(r2) != 0
							RETURN {"e3": union_distinct([e_3], []), "e4": union_distinct(flatten(e4[**].e4), []), "r2": union_distinct([r2], []), "r3": union_distinct(flatten(e4[**].r3), [])}
					)
					FILTER length(e3) != 0 AND length(e_2) != 0 AND length(r1) != 0
					RETURN {"e2": union_distinct([e_2], []), "e3": union_distinct(flatten(e3[**].e3), []), "e4": union_distinct(flatten(e3[**].e4), []), "r1": union_distinct([r1], []), "r2": union_distinct(flatten(e3[**].r2), []), "r3": union_distinct(flatten(e3[**].r3), [])}
			)
			FILTER length(e2) != 0 AND length(e_0) != 0 AND length(r0) != 0
			RETURN {"e0": union_distinct([e_0], []), "e2": union_distinct(flatten(e2[**].e2), []), "e3": union_distinct(flatten(e2[**].e3), []), "e4": union_distinct(flatten(e2[**].e4), []), "r0": union_distinct([r0], []), "r1": union_distinct(flatten(e2[**].r1), []), "r2": union_distinct(flatten(e2[**].r2), []), "r3": union_distinct(flatten(e2[**].r3), [])}
	)
	FILTER length(e0) != 0 AND length(e_1) != 0
	RETURN {"e1": union_distinct([e_1], []), "e0": union_distinct(flatten(e0[**].e0), []), "e2": union_distinct(flatten(e0[**].e2), []), "e3": union_distinct(flatten(e0[**].e3), []), "e4": union_distinct(flatten(e0[**].e4), []), "r0": union_distinct(flatten(e0[**].r0), []), "r1": union_distinct(flatten(e0[**].r1), []), "r2": union_distinct(flatten(e0[**].r2), []), "r3": union_distinct(flatten(e0[**].r3), [])}
)
LET nodes = union_distinct(flatten(tree_0[**].e0), flatten(tree_0[**].e1), flatten(tree_0[**].e2), flatten(tree_0[**].e3), flatten(tree_0[**].e4), [])
LET edges = union_distinct(flatten(tree_0[**].r0), flatten(tree_0[**].r1), flatten(tree_0[**].r2), flatten(tree_0[**].r3), [])
RETURN {"vertices":nodes,"edges":edges}
`)

var SingleJunctionFiveEntitiesThreeEntityFiltersResult = string(`LET tree_0 = (
	FOR e_0 IN parliament
	FILTER e_0.name LIKE "%Geert%"
	LET e1 = (
			FOR e_1 IN commissions
			FOR r0 IN part_of
			FILTER r0._from == e_0._id AND r0._to == e_1._id
			LET e2 = (
					FOR e_2 IN parliament
					FOR r1 IN part_of
					FILTER r1._from == e_2._id AND r1._to == e_1._id
					LET e3 = (
							FOR e_3 IN parties
							FOR r2 IN member_of
							FILTER e_3.seats < 10
							FILTER r2._from == e_2._id AND r2._to == e_3._id
							FILTER length(e_3) != 0 AND length(r2) != 0
							RETURN {"e3": union_distinct([e_3], []), "r2": union_distinct([r2], [])}
					)
					LET e4 = (
							FOR e_4 IN resolutions
							FOR r3 IN submits
							FILTER e_4.date LIKE "%mei%"
							FILTER r3._from == e_2._id AND r3._to == e_4._id
							FILTER length(e_4) != 0 AND length(r3) != 0
							RETURN {"e4": union_distinct([e_4], []), "r3": union_distinct([r3], [])}
					)
					FILTER length(e3) != 0 AND length(e4) != 0 AND length(e_2) != 0 AND length(r1) != 0
					RETURN {"e2": union_distinct([e_2], []), "r1": union_distinct([r1], []), "e3": union_distinct(flatten(e3[**].e3), []), "r2": union_distinct(flatten(e3[**].r2), []), "e4": union_distinct(flatten(e4[**].e4), []), "r3": union_distinct(flatten(e4[**].r3), [])}
			)
			FILTER length(e2) != 0 AND length(e_1) != 0 AND length(r0) != 0
			RETURN {"e1": union_distinct([e_1], []), "r0": union_distinct([r0], []), "e2": union_distinct(flatten(e2[**].e2), []), "e3": union_distinct(flatten(e2[**].e3), []), "e4": union_distinct(flatten(e2[**].e4), []), "r1": union_distinct(flatten(e2[**].r1), []), "r2": union_distinct(flatten(e2[**].r2), []), "r3": union_distinct(flatten(e2[**].r3), [])}
	)
	FILTER length(e1) != 0 AND length(e_0) != 0
	RETURN {"e0": union_distinct([e_0], []), "e1": union_distinct(flatten(e1[**].e1), []), "e2": union_distinct(flatten(e1[**].e2), []), "e3": union_distinct(flatten(e1[**].e3), []), "e4": union_distinct(flatten(e1[**].e4), []), "r0": union_distinct(flatten(e1[**].r0), []), "r1": union_distinct(flatten(e1[**].r1), []), "r2": union_distinct(flatten(e1[**].r2), []), "r3": union_distinct(flatten(e1[**].r3), [])}
)
LET nodes = union_distinct(flatten(tree_0[**].e0), flatten(tree_0[**].e1), flatten(tree_0[**].e2), flatten(tree_0[**].e3), flatten(tree_0[**].e4), [])
LET edges = union_distinct(flatten(tree_0[**].r0), flatten(tree_0[**].r1), flatten(tree_0[**].r2), flatten(tree_0[**].r3), [])
RETURN {"vertices":nodes,"edges":edges}
`)

var DoubleJunctionNineEntitiesThreeEntityFiltersResult = string(`LET tree_0 = (
	FOR e_3 IN parties
	FILTER e_3.seats < 10
	LET e2 = (
			FOR e_2 IN parliament
			FOR r2 IN member_of
			FILTER r2._from == e_2._id AND r2._to == e_3._id
			LET e1 = (
					FOR e_1 IN commissions
					FOR r1 IN part_of
					FILTER r1._from == e_2._id AND r1._to == e_1._id
					LET e0 = (
							FOR e_0 IN parliament
							FOR r0 IN part_of
							FILTER e_0.name LIKE "%Geert%"
							FILTER r0._from == e_0._id AND r0._to == e_1._id
							LET e5 = (
									FOR e_5 IN resolutions
									FOR r4 IN submits
									FILTER r4._from == e_0._id AND r4._to == e_5._id
									FILTER length(e_5) != 0 AND length(r4) != 0
									RETURN {"e5": union_distinct([e_5], []), "r4": union_distinct([r4], [])}
							)
							LET e6 = (
									FOR e_6 IN parties
									FOR r5 IN member_of
									FILTER r5._from == e_0._id AND r5._to == e_6._id
									LET e7 = (
											FOR e_7 IN parliament
											FOR r6 IN member_of
											FILTER r6._from == e_7._id AND r6._to == e_6._id
											FILTER length(e_7) != 0 AND length(r6) != 0
											RETURN {"e7": union_distinct([e_7], []), "r6": union_distinct([r6], [])}
									)
									FILTER length(e7) != 0 AND length(e_6) != 0 AND length(r5) != 0
									RETURN {"e6": union_distinct([e_6], []), "e7": union_distinct(flatten(e7[**].e7), []), "r6": union_distinct(flatten(e7[**].r6), []), "r5": union_distinct([r5], [])}
							)
							FILTER length(e5) != 0 AND length(e6) != 0 AND length(e_0) != 0 AND length(r0) != 0
							RETURN {"e0": union_distinct([e_0], []), "r0": union_distinct([r0], []), "r6": union_distinct(flatten(e6[**].r6), []), "r4": union_distinct(flatten(e5[**].r4), []), "r5": union_distinct(flatten(e6[**].r5), []), "r6": union_distinct(flatten(e6[**].r6), []), "e5": union_distinct(flatten(e5[**].e5), []), "e6": union_distinct(flatten(e6[**].e6), []), "e7": union_distinct(flatten(e6[**].e7), [])}
					)
					FILTER length(e0) != 0 AND length(e_1) != 0 AND length(r1) != 0
					RETURN {"e1": union_distinct([e_1], []), "r6": union_distinct(flatten(e0[**].r6), []), "r0": union_distinct(flatten(e0[**].r0), []), "r4": union_distinct(flatten(e0[**].r4), []), "r5": union_distinct(flatten(e0[**].r5), []), "r6": union_distinct(flatten(e0[**].r6), []), "e5": union_distinct(flatten(e0[**].e5), []), "e6": union_distinct(flatten(e0[**].e6), []), "e7": union_distinct(flatten(e0[**].e7), []), "e0": union_distinct(flatten(e0[**].e0), []), "r1": union_distinct([r1], [])}
			)
			LET e4 = (
					FOR e_4 IN resolutions
					FOR r3 IN submits
					FILTER e_4.date LIKE "%mei%"
					FILTER r3._from == e_2._id AND r3._to == e_4._id
					FILTER length(e_4) != 0 AND length(r3) != 0
					RETURN {"e4": union_distinct([e_4], []), "r3": union_distinct([r3], [])}
			)
			FILTER length(e1) != 0 AND length(e4) != 0 AND length(e_2) != 0 AND length(r2) != 0
			RETURN {"e2": union_distinct([e_2], []), "r2": union_distinct([r2], []), "r6": union_distinct(flatten(e1[**].r6), []), "r0": union_distinct(flatten(e1[**].r0), []), "r5": union_distinct(flatten(e1[**].r5), []), "r4": union_distinct(flatten(e1[**].r4), []), "r1": union_distinct(flatten(e1[**].r1), []), "e1": union_distinct(flatten(e1[**].e1), []), "e0": union_distinct(flatten(e1[**].e0), []), "e7": union_distinct(flatten(e1[**].e7), []), "e6": union_distinct(flatten(e1[**].e6), []), "e5": union_distinct(flatten(e1[**].e5), []), "r3": union_distinct(flatten(e4[**].r3), []), "e4": union_distinct(flatten(e4[**].e4), [])}
	)
	FILTER length(e2) != 0 AND length(e_3) != 0
	RETURN {"e0": union_distinct(flatten(e2[**].e0), []), "e1": union_distinct(flatten(e2[**].e1), []), "e2": union_distinct(flatten(e2[**].e2), []), "e3": union_distinct([e_3], []), "e4": union_distinct(flatten(e2[**].e4), []), "e5": union_distinct(flatten(e2[**].e5), []), "e6": union_distinct(flatten(e2[**].e6), []), "e7": union_distinct(flatten(e2[**].e7), []), "r0": union_distinct(flatten(e2[**].r0), []), "r1": union_distinct(flatten(e2[**].r1), []), "r2": union_distinct(flatten(e2[**].r2), []), "r3": union_distinct(flatten(e2[**].r3), []), "r4": union_distinct(flatten(e2[**].r4), []), "r5": union_distinct(flatten(e2[**].r5), []), "r6": union_distinct(flatten(e2[**].r6), [])}
)
LET nodes = union_distinct(flatten(tree_0[**].e0), flatten(tree_0[**].e1), flatten(tree_0[**].e2), flatten(tree_0[**].e3), flatten(tree_0[**].e4), flatten(tree_0[**].e5), flatten(tree_0[**].e6), flatten(tree_0[**].e7), [])
LET edges = union_distinct(flatten(tree_0[**].r0), flatten(tree_0[**].r1), flatten(tree_0[**].r2), flatten(tree_0[**].r3), flatten(tree_0[**].r4), flatten(tree_0[**].r5), flatten(tree_0[**].r6), [])
RETURN {"vertices":nodes,"edges":edges}
`)

var TwoEntitiesOneEntityFilterOneRelationFilterResult = string(`LET tree_0 = (
	FOR e_0 IN airports
	FILTER e_0.state == "HI"
	LET e1 = (
			FOR e_1 IN airports
			FOR r0 IN flights
			FILTER r0.Day == 15
			FILTER r0._from == e_0._id AND r0._to == e_1._id
			FILTER length(e_1) != 0 AND length(r0) != 0
			RETURN {"e1": union_distinct([e_1], []), "r0": union_distinct([r0], [])}
	)
	FILTER length(e1) != 0 AND length(e_0) != 0
	RETURN {"e0": union_distinct([e_0], []), "e1": union_distinct(flatten(e1[**].e1), []), "r0": union_distinct(flatten(e1[**].r0), [])}
)
LET nodes = union_distinct(flatten(tree_0[**].e0), flatten(tree_0[**].e1), [])
LET edges = union_distinct(flatten(tree_0[**].r0), [])
RETURN {"vertices":nodes,"edges":edges}
`)

var SingleEndPointGroupByResult = string(`LET tree_0 = (
	FOR e_2 IN parliament
	LET e3 = (
			FOR e_3 IN commissions
			FOR r1 IN part_of
			FILTER r1._from == e_2._id AND r1._to == e_3._id
			FILTER length(e_3) != 0 AND length(r1) != 0
			RETURN {"e3": union_distinct([e_3], []), "r1": union_distinct([r1], [])}
	)
	FILTER length(e3) != 0 AND length(e_2) != 0
	RETURN {"e2": union_distinct([e_2], []), "e3": union_distinct(flatten(e3[**].e3), []), "r1": union_distinct(flatten(e3[**].r1), [])}
)
LET gt6 = (
	FOR x IN part_of
	LET variable_0 = (
			LET tmp = union_distinct(flatten(tree_0[**].e3), [])
					FOR y IN tmp
					FILTER y._id == x._to
					RETURN y.name
	)
	LET variable_1 = variable_0[0]
	LET variable_2 = (
			LET tmp = union_distinct(flatten(tree_0[**].e2), [])
					FOR y IN tmp
					FILTER y._id == x._from
					RETURN y.age
	)
	LET variable_3 = variable_2[0]
	FILTER variable_1 != NULL AND variable_3 != NULL
	RETURN {
			"group": variable_1,
			"by": variable_3
	}
)
LET g6 = (
	FOR x IN gt6
	COLLECT c = x.group INTO groups = x.by
	LET variable_0 = AVG(groups)
	FILTER variable_0 > 45
	RETURN {
	_id: c,
	modifier: variable_0
	}
)
FOR x in g6
RETURN {group: x.modifier, by: x._id}
`)

var SingleGroupByResult = string(`LET tree_0 = (
	FOR e_2 IN parliament
	LET e3 = (
			FOR e_3 IN commissions
			FOR r1 IN part_of
			FILTER r1._from == e_2._id AND r1._to == e_3._id
			FILTER length(e_3) != 0 AND length(r1) != 0
			RETURN {"e3": union_distinct([e_3], []), "r1": union_distinct([r1], [])}
	)
	FILTER length(e3) != 0 AND length(e_2) != 0
	RETURN {"e2": union_distinct([e_2], []), "e3": union_distinct(flatten(e3[**].e3), []), "r1": union_distinct(flatten(e3[**].r1), [])}
)
LET gt6 = (
	FOR x IN part_of
	LET variable_0 = (
			LET tmp = union_distinct(flatten(tree_0[**].e3), [])
					FOR y IN tmp
					FILTER y._id == x._to
					RETURN y.name
	)
	LET variable_1 = variable_0[0]
	LET variable_2 = (
			LET tmp = union_distinct(flatten(tree_0[**].e2), [])
					FOR y IN tmp
					FILTER y._id == x._from
					RETURN y.age
	)
	LET variable_3 = variable_2[0]
	FILTER variable_1 != NULL AND variable_3 != NULL
	RETURN {
			"group": variable_1,
			"by": variable_3
	}
)
LET g6 = (
	FOR x IN gt6
	COLLECT c = x.group INTO groups = x.by
	LET variable_0 = avg(groups)
	FILTER variable_0 > 45
	RETURN {
	_id: c,
	modifier: variable_0
	}
)
LET in6 = (
	FOR x IN g6[**]._id
	FOR y IN commissions
	FILTER x == y.name
	RETURN y
)
LET tree_1 = (
	FOR e_4 IN parliament
	LET gb6 = (
			FOR g_6 IN in6
			FOR r2 IN part_of
			FILTER r2._from == e_4._id AND r2._to == g_6._id
			FILTER length(g_6) != 0 AND length(r2) != 0
			RETURN {"gb6": union_distinct([g_6], []), "r2": union_distinct([r2], [])}
	)
	FILTER length(gb6) != 0 AND length(e_4) != 0
	RETURN {"r2": union_distinct(flatten(gb6[**].r2), []), "e4": union_distinct([e_4], []), "gb6": union_distinct(flatten(gb6[**].gb6), [])}
)
LET nodes = union_distinct(flatten(tree_1[**].e4), flatten(tree_1[**].gb6), [])
LET edges = union_distinct(flatten(tree_1[**].r2), [])
RETURN {"vertices":nodes,"edges":edges}
`)

var MultipleInputGroupByEndpointResult = string(`LET tree_0 = (
	FOR e_43 IN parliament
	FILTER e_43.age < 42
	LET e44 = (
			FOR e_44 IN parties
			FOR r42 IN member_of
			FILTER e_44.seats > 6
			FILTER r42._from == e_43._id AND r42._to == e_44._id
			FILTER length(e_44) != 0 AND length(r42) != 0
			RETURN {"e44": union_distinct([e_44], []), "r42": union_distinct([r42], [])}
	)
	FILTER length(e44) != 0 AND length(e_43) != 0
	RETURN {"e43": union_distinct([e_43], []), "e44": union_distinct(flatten(e44[**].e44), []), "r42": union_distinct(flatten(e44[**].r42), [])}
)
LET tree_1 = (
	FOR e_48 IN parliament
	FILTER e_48.age >= 42
	LET e49 = (
			FOR e_49 IN parties
			FOR r47 IN member_of
			FILTER e_49.seats <= 6
			FILTER r47._from == e_48._id AND r47._to == e_49._id
			FILTER length(e_49) != 0 AND length(r47) != 0
			RETURN {"e49": union_distinct([e_49], []), "r47": union_distinct([r47], [])}
	)
	FILTER length(e49) != 0 AND length(e_48) != 0
	RETURN {"e48": union_distinct([e_48], []), "e49": union_distinct(flatten(e49[**].e49), []), "r47": union_distinct(flatten(e49[**].r47), [])}
)
LET gt31 = (
	FOR x IN member_of
	LET variable_0 = (
			LET tmp = union_distinct(flatten(tree_1[**].e49), flatten(tree_0[**].e44), [])
					FOR y IN tmp
					FILTER y._id == x._to
					RETURN y._id
	)
	LET variable_1 = variable_0[0]
	LET variable_2 = (
			LET tmp = union_distinct(flatten(tree_0[**].e43), flatten(tree_1[**].e48), [])
					FOR y IN tmp
					FILTER y._id == x._from
					RETURN y.age
	)
	LET variable_3 = variable_2[0]
	FILTER variable_1 != NULL AND variable_3 != NULL
	RETURN {
			"group": variable_1,
			"by": variable_3
	}
)
LET g31 = (
	FOR x IN gt31
	COLLECT c = x.group INTO groups = x.by
	LET variable_0 = AVG(groups)
	FILTER variable_0 >= 40
	RETURN {
	_id: c,
	modifier: variable_0
	}
)
FOR x in g31
RETURN {group: x.modifier, by: x._id}
`)

var MultipleInputGroupByResult = string(`LET tree_0 = (
	FOR e_43 IN parliament
	FILTER e_43.age < 42
	LET e44 = (
			FOR e_44 IN parties
			FOR r42 IN member_of
			FILTER e_44.seats > 6
			FILTER r42._from == e_43._id AND r42._to == e_44._id
			FILTER length(e_44) != 0 AND length(r42) != 0
			RETURN {"e44": union_distinct([e_44], []), "r42": union_distinct([r42], [])}
	)
	FILTER length(e44) != 0 AND length(e_43) != 0
	RETURN {"e44": union_distinct(flatten(e44[**].e44), []), "r42": union_distinct(flatten(e44[**].r42), []), "e43": union_distinct([e_43], [])}
)
LET tree_1 = (
	FOR e_48 IN parliament
	FILTER e_48.age >= 42
	LET e49 = (
			FOR e_49 IN parties
			FOR r47 IN member_of
			FILTER e_49.seats <= 6
			FILTER r47._from == e_48._id AND r47._to == e_49._id
			FILTER length(e_49) != 0 AND length(r47) != 0
			RETURN {"e49": union_distinct([e_49], []), "r47": union_distinct([r47], [])}
	)
	FILTER length(e49) != 0 AND length(e_48) != 0
	RETURN {"e48": union_distinct([e_48], []), "e49": union_distinct(flatten(e49[**].e49), []), "r47": union_distinct(flatten(e49[**].r47), [])}
)
LET gt31 = (
	FOR x IN member_of
	LET variable_0 = (
			LET tmp = union_distinct(flatten(tree_1[**].e49), flatten(tree_0[**].e44), [])
					FOR y IN tmp
					FILTER y._id == x._to
					RETURN y._id
	)
	LET variable_1 = variable_0[0]
	LET variable_2 = (
			LET tmp = union_distinct(flatten(tree_0[**].e43), flatten(tree_1[**].e48), [])
					FOR y IN tmp
					FILTER y._id == x._from
					RETURN y.age
	)
	LET variable_3 = variable_2[0]
	FILTER variable_1 != NULL AND variable_3 != NULL
	RETURN {
			"group": variable_1,
			"by": variable_3
	}
)
LET g31 = (
	FOR x IN gt31
	COLLECT c = x.group INTO groups = x.by
	LET variable_0 = AVG(groups)
	FILTER variable_0 >= 40
	RETURN {
	_id: c,
	modifier: variable_0
	}
)
LET in31 = (
	FOR x IN g31[**]._id
	FOR y IN parties
	FILTER x == y._id
	RETURN y
)
LET tree_2 = (
	FOR e_73 IN parliament
	LET gb31 = (
			FOR g_31 IN in31
			FOR r71 IN member_of
			FILTER r71._from == e_73._id AND r71._to == g_31._id
			FILTER length(g_31) != 0 AND length(r71) != 0
			RETURN {"gb31": union_distinct([g_31], []), "r71": union_distinct([r71], [])}
	)
	FILTER length(gb31) != 0 AND length(e_73) != 0
	RETURN {"e73": union_distinct([e_73], []), "gb31": union_distinct(flatten(gb31[**].gb31), []), "r71": union_distinct(flatten(gb31[**].r71), [])}
)
LET nodes = union_distinct(flatten(tree_2[**].e73), flatten(tree_2[**].gb31), [])
LET edges = union_distinct(flatten(tree_2[**].r71), [])
RETURN {"vertices":nodes,"edges":edges}
`)

var TopGroupByResult = string(`LET tree_0 = (
	FOR e_9 IN parliament
	LET e10 = (
			FOR e_10 IN parties
			FOR r8 IN member_of
			FILTER r8._from == e_9._id AND r8._to == e_10._id
			FILTER length(e_10) != 0 AND length(r8) != 0
			RETURN {"e10": union_distinct([e_10], []), "r8": union_distinct([r8], [])}
	)
	FILTER length(e10) != 0 AND length(e_9) != 0
	RETURN {"e9": union_distinct([e_9], []), "e10": union_distinct(flatten(e10[**].e10), []), "r8": union_distinct(flatten(e10[**].r8), [])}
)
LET gt13 = (
	FOR x IN member_of
	LET variable_0 = (
			LET tmp = union_distinct(flatten(tree_0[**].e10), [])
					FOR y IN tmp
					FILTER y._id == x._to
					RETURN y._id
	)
	LET variable_1 = variable_0[0]
	LET variable_2 = (
			LET tmp = union_distinct(flatten(tree_0[**].e9), [])
					FOR y IN tmp
					FILTER y._id == x._from
					RETURN y.age
	)
	LET variable_3 = variable_2[0]
	FILTER variable_1 != NULL AND variable_3 != NULL
	RETURN {
			"group": variable_1,
			"by": variable_3
	}
)
LET g13 = (
	FOR x IN gt13
	COLLECT c = x.group INTO groups = x.by
	LET variable_0 = AVG(groups)
	FILTER variable_0 >= 42
	RETURN {
	_id: c,
	modifier: variable_0
	}
)
LET in13 = (
	FOR x IN g13[**]._id
	FOR y IN parties
	FILTER x == y._id
	RETURN y
)
LET tree_1 = (
	FOR e_68 IN parliament
	LET e69 = (
			FOR e_69 IN parties
			FOR r67 IN member_of
			FILTER r67._from == e_68._id AND r67._to == e_69._id
			FILTER length(e_69) != 0 AND length(r67) != 0
			RETURN {"e69": union_distinct([e_69], []), "r67": union_distinct([r67], [])}
	)
	FILTER length(e69) != 0 AND length(e_68) != 0
	RETURN {"e69": union_distinct(flatten(e69[**].e69), []), "r67": union_distinct(flatten(e69[**].r67), []), "e68": union_distinct([e_68], [])}
)
LET gt72 = (
	FOR x IN member_of
	LET variable_0 = (
			LET tmp = union_distinct(flatten(tree_1[**].e68), [])
					FOR y IN tmp
					FILTER y._id == x._from
					RETURN y._id
	)
	LET variable_1 = variable_0[0]
	LET variable_2 = (
			LET tmp = union_distinct(flatten(tree_1[**].e69), [])
					FOR y IN tmp
					FILTER y._id == x._to
					RETURN y.seats
	)
	LET variable_3 = variable_2[0]
	FILTER variable_1 != NULL AND variable_3 != NULL
	RETURN {
			"group": variable_1,
			"by": variable_3
	}
)
LET g72 = (
	FOR x IN gt72
	COLLECT c = x.group INTO groups = x.by
	LET variable_0 = AVG(groups)
	FILTER variable_0 < 13
	RETURN {
	_id: c,
	modifier: variable_0
	}
)
LET in72 = (
	FOR x IN g72[**]._id
	FOR y IN parliament
	FILTER x == y._id
	RETURN y
)
LET tree_2 = (
	FOR g_13 IN in13
	LET gb72 = (
			FOR g_72 IN in72
			FOR r76 IN member_of
			FILTER r76._from == g_72._id AND r76._to == g_13._id
			FILTER length(g_72) != 0 AND length(r76) != 0
			RETURN {"gb72": union_distinct([g_72], []), "r76": union_distinct([r76], [])}
	)
	FILTER length(gb72) != 0 AND length(g_13) != 0
	RETURN {"gb13": union_distinct([g_13], []), "gb72": union_distinct(flatten(gb72[**].gb72), []), "r76": union_distinct(flatten(gb72[**].r76), [])}
)
LET nodes = union_distinct(flatten(tree_2[**].gb13), flatten(tree_2[**].gb72), [])
LET edges = union_distinct(flatten(tree_2[**].r76), [])
RETURN {"vertices":nodes,"edges":edges}
`)

var WayTooLargeQueryResult = string(`LET tree_0 = (
	FOR e_9 IN parliament
	LET e10 = (
			FOR e_10 IN parties
			FOR r8 IN member_of
			FILTER r8._from == e_9._id AND r8._to == e_10._id
			FILTER length(e_10) != 0 AND length(r8) != 0
			RETURN {"e10": union_distinct([e_10], []), "r8": union_distinct([r8], [])}
	)
	FILTER length(e10) != 0 AND length(e_9) != 0
	RETURN {"e9": union_distinct([e_9], []), "e10": union_distinct(flatten(e10[**].e10), []), "r8": union_distinct(flatten(e10[**].r8), [])}
)
LET gt13 = (
	FOR x IN member_of
	LET variable_0 = (
			LET tmp = union_distinct(flatten(tree_0[**].e10), [])
					FOR y IN tmp
					FILTER y._id == x._to
					RETURN y._id
	)
	LET variable_1 = variable_0[0]
	LET variable_2 = (
			LET tmp = union_distinct(flatten(tree_0[**].e9), [])
					FOR y IN tmp
					FILTER y._id == x._from
					RETURN y.age
	)
	LET variable_3 = variable_2[0]
	FILTER variable_1 != NULL AND variable_3 != NULL
	RETURN {
			"group": variable_1,
			"by": variable_3
	}
)
LET g13 = (
	FOR x IN gt13
	COLLECT c = x.group INTO groups = x.by
	LET variable_0 = AVG(groups)
	FILTER variable_0 >= 42
	RETURN {
	_id: c,
	modifier: variable_0
	}
)
LET in13 = (
	FOR x IN g13[**]._id
	FOR y IN parties
	FILTER x == y._id
	RETURN y
)
LET tree_1 = (
	FOR e_68 IN parliament
	LET e69 = (
			FOR e_69 IN parties
			FOR r67 IN member_of
			FILTER r67._from == e_68._id AND r67._to == e_69._id
			FILTER length(e_69) != 0 AND length(r67) != 0
			RETURN {"e69": union_distinct([e_69], []), "r67": union_distinct([r67], [])}
	)
	FILTER length(e69) != 0 AND length(e_68) != 0
	RETURN {"e68": union_distinct([e_68], []), "e69": union_distinct(flatten(e69[**].e69), []), "r67": union_distinct(flatten(e69[**].r67), [])}
)
LET gt72 = (
	FOR x IN member_of
	LET variable_0 = (
			LET tmp = union_distinct(flatten(tree_1[**].e68), [])
					FOR y IN tmp
					FILTER y._id == x._from
					RETURN y._id
	)
	LET variable_1 = variable_0[0]
	LET variable_2 = (
			LET tmp = union_distinct(flatten(tree_1[**].e69), [])
					FOR y IN tmp
					FILTER y._id == x._to
					RETURN y.seats
	)
	LET variable_3 = variable_2[0]
	FILTER variable_1 != NULL AND variable_3 != NULL
	RETURN {
			"group": variable_1,
			"by": variable_3
	}
)
LET g72 = (
	FOR x IN gt72
	COLLECT c = x.group INTO groups = x.by
	LET variable_0 = AVG(groups)
	FILTER variable_0 < 13
	RETURN {
	_id: c,
	modifier: variable_0
	}
)
LET in72 = (
	FOR x IN g72[**]._id
	FOR y IN parliament
	FILTER x == y._id
	RETURN y
)
LET tree_2 = (
	FOR e_104 IN commissions
	LET e93 = (
			FOR e_93 IN parliament
			FOR r98 IN part_of
			FILTER e_93.age >= 32
			FILTER r98._from == e_93._id AND r98._to == e_104._id
			LET gb13 = (
					FOR g_13 IN in13
					FOR r92 IN member_of
					FILTER r92._from == e_93._id AND r92._to == g_13._id
					LET gb72 = (
							FOR g_72 IN in72
							FOR r76 IN member_of
							FILTER r76._from == g_72._id AND r76._to == g_13._id
							FILTER length(g_72) != 0 AND length(r76) != 0
							RETURN {"gb72": union_distinct([g_72], []), "r76": union_distinct([r76], [])}
					)
					FILTER length(gb72) != 0 AND length(g_13) != 0 AND length(r92) != 0
					RETURN {"r92": union_distinct([r92], []), "gb72": union_distinct(flatten(gb72[**].gb72), []), "r76": union_distinct(flatten(gb72[**].r76), []), "gb13": union_distinct([g_13], [])}
			)
			FILTER length(gb13) != 0 AND length(e_93) != 0 AND length(r98) != 0
			RETURN {"e93": union_distinct([e_93], []), "r98": union_distinct([r98], []), "gb13": union_distinct(flatten(gb13[**].gb13), []), "r92": union_distinct(flatten(gb13[**].r92), []), "gb72": union_distinct(flatten(gb13[**].gb72), []), "r76": union_distinct(flatten(gb13[**].r76), [])}
	)
	FILTER length(e93) != 0 AND length(e_104) != 0
	RETURN {"r98": union_distinct(flatten(e93[**].r98), []), "gb13": union_distinct(flatten(e93[**].gb13), []), "r92": union_distinct(flatten(e93[**].r92), []), "gb72": union_distinct(flatten(e93[**].gb72), []), "r76": union_distinct(flatten(e93[**].r76), []), "e104": union_distinct([e_104], []), "e93": union_distinct(flatten(e93[**].e93), [])}
)
LET gt106 = (
	FOR x IN part_of
	LET variable_0 = (
			LET tmp = union_distinct(flatten(tree_2[**].e104), [])
					FOR y IN tmp
					FILTER y._id == x._to
					RETURN y._id
	)
	LET variable_1 = variable_0[0]
	LET variable_2 = (
			LET tmp = union_distinct(flatten(tree_2[**].e93), [])
					FOR y IN tmp
					FILTER y._id == x._from
					RETURN y.age
	)
	LET variable_3 = variable_2[0]
	FILTER variable_1 != NULL AND variable_3 != NULL
	RETURN {
			"group": variable_1,
			"by": variable_3
	}
)
LET g106 = (
	FOR x IN gt106
	COLLECT c = x.group INTO groups = x.by
	LET variable_0 = AVG(groups)
	FILTER variable_0 <= 69
	RETURN {
	_id: c,
	modifier: variable_0
	}
)
LET in106 = (
	FOR x IN g106[**]._id
	FOR y IN commissions
	FILTER x == y._id
	RETURN y
)
LET tree_3 = (
	FOR e_126 IN parties
	FILTER e_126.seats <= 14
	LET e119 = (
			FOR e_119 IN parliament
			FOR r124 IN member_of
			FILTER e_119.age <= 67
			FILTER r124._from == e_119._id AND r124._to == e_126._id
			LET gb106 = (
					FOR g_106 IN in106
					FOR r118 IN part_of
					FILTER r118._from == e_119._id AND r118._to == g_106._id
					FILTER length(g_106) != 0 AND length(r118) != 0
					RETURN {"gb106": union_distinct([g_106], []), "r118": union_distinct([r118], [])}
			)
			FILTER length(gb106) != 0 AND length(e_119) != 0 AND length(r124) != 0
			RETURN {"gb106": union_distinct(flatten(gb106[**].gb106), []), "r118": union_distinct(flatten(gb106[**].r118), []), "e119": union_distinct([e_119], []), "r124": union_distinct([r124], [])}
	)
	FILTER length(e119) != 0 AND length(e_126) != 0
	RETURN {"e119": union_distinct(flatten(e119[**].e119), []), "r124": union_distinct(flatten(e119[**].r124), []), "gb106": union_distinct(flatten(e119[**].gb106), []), "r118": union_distinct(flatten(e119[**].r118), []), "e126": union_distinct([e_126], [])}
)
LET gt138 = (
	FOR x IN member_of
	LET variable_0 = (
			LET tmp = union_distinct(flatten(tree_3[**].e126), [])
					FOR y IN tmp
					FILTER y._id == x._to
					RETURN y._id
	)
	LET variable_1 = variable_0[0]
	LET variable_2 = (
			LET tmp = union_distinct(flatten(tree_3[**].e119), [])
					FOR y IN tmp
					FILTER y._id == x._from
					RETURN y.age
	)
	LET variable_3 = variable_2[0]
	FILTER variable_1 != NULL AND variable_3 != NULL
	RETURN {
			"group": variable_1,
			"by": variable_3
	}
)
LET g138 = (
	FOR x IN gt138
	COLLECT c = x.group INTO groups = x.by
	LET variable_0 = PRODUCT(groups)
	FILTER variable_0 >= 8008
	RETURN {
	_id: c,
	modifier: variable_0
	}
)
LET in138 = (
	FOR x IN g138[**]._id
	FOR y IN parties
	FILTER x == y._id
	RETURN y
)
LET tree_4 = (
	FOR e_145 IN parliament
	LET gb138 = (
			FOR g_138 IN in138
			FOR r144 IN member_of
			FILTER r144._from == e_145._id AND r144._to == g_138._id
			FILTER length(g_138) != 0 AND length(r144) != 0
			RETURN {"gb138": union_distinct([g_138], []), "r144": union_distinct([r144], [])}
	)
	FILTER length(gb138) != 0 AND length(e_145) != 0
	RETURN {"e145": union_distinct([e_145], []), "gb138": union_distinct(flatten(gb138[**].gb138), []), "r144": union_distinct(flatten(gb138[**].r144), [])}
)
LET nodes = union_distinct(flatten(tree_4[**].gb138), flatten(tree_4[**].e145), [])
LET edges = union_distinct(flatten(tree_4[**].r144), [])
RETURN {"vertices":nodes,"edges":edges}
`)

var AllStreamersResult = string(`LET tree_0 = (
	FOR e_2 IN Streamer
	LET e3 = (
			FOR e_3 IN Streamer
			FOR r1 IN viewerOverlap
			FILTER r1._from == e_2._id AND r1._to == e_3._id
			FILTER length(e_3) != 0 AND length(r1) != 0
			RETURN {"e3": union_distinct([e_3], []), "r1": union_distinct([r1], [])}
	)
	FILTER length(e3) != 0 AND length(e_2) != 0
	RETURN {"e3": union_distinct(flatten(e3[**].e3), []), "r1": union_distinct(flatten(e3[**].r1), []), "e2": union_distinct([e_2], [])}
)
LET gt6 = (
	FOR x IN viewerOverlap
	LET variable_0 = (
			LET tmp = union_distinct(flatten(tree_0[**].e3), [])
					FOR y IN tmp
					FILTER y._id == x._to
					RETURN y._id
	)
	LET variable_1 = variable_0[0]
	LET variable_2 = (
			LET tmp = union_distinct(flatten(tree_0[**].e2), [])
					FOR y IN tmp
					FILTER y._id == x._from
					RETURN y.Count
	)
	LET variable_3 = variable_2[0]
	FILTER variable_1 != NULL AND variable_3 != NULL
	RETURN {
			"group": variable_1,
			"by": variable_3
	}
)
LET g6 = (
	FOR x IN gt6
	COLLECT c = x.group INTO groups = x.by
	LET variable_0 = AVG(groups)
	RETURN {
	_id: c,
	modifier: variable_0
	}
)
LET in6 = (
	FOR x IN g6[**]._id
	FOR y IN Streamer
	FILTER x == y._id
	RETURN y
)
LET gt7 = (
	FOR x IN viewerOverlap
	LET variable_0 = (
			LET tmp = union_distinct(flatten(tree_0[**].e3), [])
					FOR y IN tmp
					FILTER y._id == x._to
					RETURN y._id
	)
	LET variable_1 = variable_0[0]
	LET variable_2 = (
			LET tmp = union_distinct(flatten(tree_0[**].e2), [])
					FOR y IN tmp
					FILTER y._id == x._from
					RETURN y.Count
	)
	LET variable_3 = variable_2[0]
	FILTER variable_1 != NULL AND variable_3 != NULL
	RETURN {
			"group": variable_1,
			"by": variable_3
	}
)
LET g7 = (
	FOR x IN gt7
	COLLECT c = x.group INTO groups = x.by
	LET variable_0 = AVG(groups)
	RETURN {
	_id: c,
	modifier: variable_0
	}
)
LET in7 = (
	FOR x IN g7[**]._id
	FOR y IN Streamer
	FILTER x == y._id
	RETURN y
)
LET tree_1 = (
	FOR g_6 IN in6
	LET gb7 = (
			FOR g_7 IN in7
			FOR r14 IN viewerOverlap
			FILTER r14._from == g_6._id AND r14._to == g_7._id
			FILTER length(g_7) != 0 AND length(r14) != 0
			RETURN {"gb7": union_distinct([g_7], []), "r14": union_distinct([r14], [])}
	)
	FILTER length(gb7) != 0 AND length(g_6) != 0
	RETURN {"gb6": union_distinct([g_6], []), "gb7": union_distinct(flatten(gb7[**].gb7), []), "r14": union_distinct(flatten(gb7[**].r14), [])}
)
LET gt21 = (
	FOR x IN viewerOverlap
	LET variable_0 = (
			LET tmp = union_distinct(flatten(tree_1[**].gb7), [])
					FOR y IN tmp
					FILTER y._id == x._to
					RETURN y._id
	)
	LET variable_1 = variable_0[0]
	LET variable_2 = (
			LET tmp = union_distinct(flatten(tree_1[**].gb6), [])
					FOR y IN tmp
					FILTER y._id == x._from
					RETURN y.Count
	)
	LET variable_3 = variable_2[0]
	FILTER variable_1 != NULL AND variable_3 != NULL
	RETURN {
			"group": variable_1,
			"by": variable_3
	}
)
LET g21 = (
	FOR x IN gt21
	COLLECT c = x.group INTO groups = x.by
	LET variable_0 = AVG(groups)
	RETURN {
	_id: c,
	modifier: variable_0
	}
)
FOR x in g21
RETURN {group: x.modifier, by: x._id}`)

var StringMatchTypesResult = string(`LET tree_0 = (
	FOR e_3 IN parties
	FILTER e_3.name NOT LIKE "v"
	LET e2 = (
			FOR e_2 IN parliament
			FOR r1 IN member_of
			FILTER e_2.name != "Geert Wilders"
			FILTER r1._from == e_2._id AND r1._to == e_3._id
			LET e8 = (
					FOR e_8 IN commissions
					FOR r6 IN part_of
					FILTER e_8.name NOT LIKE "groep"
					FILTER r6._from == e_2._id AND r6._to == e_8._id
					FILTER length(e_8) != 0 AND length(r6) != 0
					RETURN {"e8": union_distinct([e_8], []), "r6": union_distinct([r6], [])}
			)
			FILTER length(e8) != 0 AND length(e_2) != 0 AND length(r1) != 0
			RETURN {"r1": union_distinct([r1], []), "e8": union_distinct(flatten(e8[**].e8), []), "r6": union_distinct(flatten(e8[**].r6), []), "e2": union_distinct([e_2], [])}
	)
	FILTER length(e2) != 0 AND length(e_3) != 0
	RETURN {"r6": union_distinct(flatten(e2[**].r6), []), "e3": union_distinct([e_3], []), "e2": union_distinct(flatten(e2[**].e2), []), "r1": union_distinct(flatten(e2[**].r1), []), "e8": union_distinct(flatten(e2[**].e8), [])}
)
LET nodes = union_distinct(flatten(tree_0[**].e3), flatten(tree_0[**].e2), flatten(tree_0[**].e8), [])
LET edges = union_distinct(flatten(tree_0[**].r6), flatten(tree_0[**].r1), [])
RETURN {"vertices":nodes,"edges":edges}`)

var TopEntityToFromGroupByResult = string(`LET tree_0 = (
	FOR e_26 IN Streamer
	LET e27 = (
			FOR e_27 IN Streamer
			FOR r25 IN viewerOverlap
			FILTER r25._from == e_26._id AND r25._to == e_27._id
			FILTER length(e_27) != 0 AND length(r25) != 0
			RETURN {"e27": union_distinct([e_27], []), "r25": union_distinct([r25], [])}
	)
	FILTER length(e27) != 0 AND length(e_26) != 0
	RETURN {"e26": union_distinct([e_26], []), "e27": union_distinct(flatten(e27[**].e27), []), "r25": union_distinct(flatten(e27[**].r25), [])}
)
LET gt30 = (
	FOR x IN viewerOverlap
	LET variable_0 = (
			LET tmp = union_distinct(flatten(tree_0[**].e26), [])
					FOR y IN tmp
					FILTER y._id == x._to
					RETURN y._id
	)
	LET variable_1 = variable_0[0]
	LET variable_2 = (
			LET tmp = union_distinct(flatten(tree_0[**].e27), [])
					FOR y IN tmp
					FILTER y._id == x._from
					RETURN y.Count
	)
	LET variable_3 = variable_2[0]
	FILTER variable_1 != NULL AND variable_3 != NULL
	RETURN {
			"group": variable_1,
			"by": variable_3
	}
)
LET g30 = (
	FOR x IN gt30
	COLLECT c = x.group INTO groups = x.by
	LET variable_0 = avg(groups)
	RETURN {
	_id: c,
	modifier: variable_0
	}
)
LET in30 = (
	FOR x IN g30[**]._id
	FOR y IN Streamer
	FILTER x == y._id
	RETURN y
)
LET tree_1 = (
	FOR e_36 IN Streamer
	LET gb30 = (
			FOR g_30 IN in30
			FOR r34 IN viewerOverlap
			FILTER r34._from == g_30._id AND r34._to == e_36._id
			FILTER length(g_30) != 0 AND length(r34) != 0
			RETURN {"gb30": union_distinct([g_30], []), "r34": union_distinct([r34], [])}
	)
	FILTER length(gb30) != 0 AND length(e_36) != 0
	RETURN {"r34": union_distinct(flatten(gb30[**].r34), []), "e36": union_distinct([e_36], []), "gb30": union_distinct(flatten(gb30[**].gb30), [])}
)
LET nodes = union_distinct(flatten(tree_1[**].e36), flatten(tree_1[**].gb30), [])
LET edges = union_distinct(flatten(tree_1[**].r34), [])
RETURN {"vertices":nodes,"edges":edges}`)

var IntNotEqualsResult = string(`LET tree_0 = (
	FOR e_2 IN parliament
	FILTER e_2.seniority != 57
	LET e3 = (
			FOR e_3 IN parties
			FOR r1 IN member_of
			FILTER r1._from == e_2._id AND r1._to == e_3._id
			FILTER length(e_3) != 0 AND length(r1) != 0
			RETURN {"e3": union_distinct([e_3], []), "r1": union_distinct([r1], [])}
	)
	FILTER length(e3) != 0 AND length(e_2) != 0
	RETURN {"e2": union_distinct([e_2], []), "e3": union_distinct(flatten(e3[**].e3), []), "r1": union_distinct(flatten(e3[**].r1), [])}
)
LET nodes = union_distinct(flatten(tree_0[**].e3), flatten(tree_0[**].e2), [])
LET edges = union_distinct(flatten(tree_0[**].r1), [])
RETURN {"vertices":nodes,"edges":edges}`)

var BoolEqualResult = string(`LET tree_0 = (
	FOR e_8 IN parliament
	LET e9 = (
			FOR e_9 IN parties
			FOR r7 IN member_of
			FILTER r7.isChairman == true
			FILTER r7._from == e_8._id AND r7._to == e_9._id
			FILTER length(e_9) != 0 AND length(r7) != 0
			RETURN {"e9": union_distinct([e_9], []), "r7": union_distinct([r7], [])}
	)
	FILTER length(e9) != 0 AND length(e_8) != 0
	RETURN {"e8": union_distinct([e_8], []), "e9": union_distinct(flatten(e9[**].e9), []), "r7": union_distinct(flatten(e9[**].r7), [])}
)
LET nodes = union_distinct(flatten(tree_0[**].e8), flatten(tree_0[**].e9), [])
LET edges = union_distinct(flatten(tree_0[**].r7), [])
RETURN {"vertices":nodes,"edges":edges}`)

var BoolNotEqualResult = string(`LET tree_0 = (
	FOR e_8 IN parliament
	LET e9 = (
			FOR e_9 IN parties
			FOR r7 IN member_of
			FILTER r7.isChairman != true
			FILTER r7._from == e_8._id AND r7._to == e_9._id
			FILTER length(e_9) != 0 AND length(r7) != 0
			RETURN {"e9": union_distinct([e_9], []), "r7": union_distinct([r7], [])}
	)
	FILTER length(e9) != 0 AND length(e_8) != 0
	RETURN {"e8": union_distinct([e_8], []), "e9": union_distinct(flatten(e9[**].e9), []), "r7": union_distinct(flatten(e9[**].r7), [])}
)
LET nodes = union_distinct(flatten(tree_0[**].e9), flatten(tree_0[**].e8), [])
LET edges = union_distinct(flatten(tree_0[**].r7), [])
RETURN {"vertices":nodes,"edges":edges}`)

var NestedGroupBys2Result = string(`LET tree_0 = (
	FOR e_9 IN parliament
	LET e10 = (
			FOR e_10 IN commissions
			FOR r8 IN part_of
			FILTER r8._from == e_9._id AND r8._to == e_10._id
			FILTER length(e_10) != 0 AND length(r8) != 0
			RETURN {"e10": union_distinct([e_10], []), "r8": union_distinct([r8], [])}
	)
	FILTER length(e10) != 0 AND length(e_9) != 0
	RETURN {"r8": union_distinct(flatten(e10[**].r8), []), "e9": union_distinct([e_9], []), "e10": union_distinct(flatten(e10[**].e10), [])}
)
LET gt6 = (
	FOR x IN part_of
	LET variable_0 = (
			LET tmp = union_distinct(flatten(tree_0[**].e10), [])
					FOR y IN tmp
					FILTER y._id == x._to
					RETURN y._id
	)
	LET variable_1 = variable_0[0]
	LET variable_2 = (
			LET tmp = union_distinct(flatten(tree_0[**].e9), [])
					FOR y IN tmp
					FILTER y._id == x._from
					RETURN y.age
	)
	LET variable_3 = variable_2[0]
	FILTER variable_1 != NULL AND variable_3 != NULL
	RETURN {
			"group": variable_1,
			"by": variable_3
	}
)
LET g6 = (
	FOR x IN gt6
	COLLECT c = x.group INTO groups = x.by
	LET variable_0 = AVG(groups)
	FILTER variable_0 <= 41
	RETURN {
	_id: c,
	modifier: variable_0
	}
)
LET in6 = (
	FOR x IN g6[**]._id
	FOR y IN commissions
	FILTER x == y._id
	RETURN y
)
LET tree_1 = (
	FOR e_25 IN parliament
	LET gb6 = (
			FOR g_6 IN in6
			FOR r24 IN part_of
			FILTER r24._from == e_25._id AND r24._to == g_6._id
			FILTER length(g_6) != 0 AND length(r24) != 0
			RETURN {"gb6": union_distinct([g_6], []), "r24": union_distinct([r24], [])}
	)
	FILTER length(gb6) != 0 AND length(e_25) != 0
	RETURN {"e25": union_distinct([e_25], []), "gb6": union_distinct(flatten(gb6[**].gb6), []), "r24": union_distinct(flatten(gb6[**].r24), [])}
)
LET gt40 = (
	FOR x IN part_of
	LET variable_0 = (
			LET tmp = union_distinct(flatten(tree_1[**].gb6), [])
					FOR y IN tmp
					FILTER y._id == x._to
					RETURN y._id
	)
	LET variable_1 = variable_0[0]
	LET variable_2 = (
			LET tmp = union_distinct(flatten(tree_1[**].e25), [])
					FOR y IN tmp
					FILTER y._id == x._from
					RETURN y.age
	)
	LET variable_3 = variable_2[0]
	FILTER variable_1 != NULL AND variable_3 != NULL
	RETURN {
			"group": variable_1,
			"by": variable_3
	}
)
LET g40 = (
	FOR x IN gt40
	COLLECT c = x.group INTO groups = x.by
	LET variable_0 = MAX(groups)
	RETURN {
	_id: c,
	modifier: variable_0
	}
)
LET in40 = (
	FOR x IN g40[**]._id
	FOR y IN commissions
	FILTER x == y._id
	RETURN y
)
LET tree_2 = (
	FOR e_52 IN parliament
	LET gb40 = (
			FOR g_40 IN in40
			FOR r51 IN part_of
			FILTER r51._from == e_52._id AND r51._to == g_40._id
			FILTER length(g_40) != 0 AND length(r51) != 0
			RETURN {"gb40": union_distinct([g_40], []), "r51": union_distinct([r51], [])}
	)
	FILTER length(gb40) != 0 AND length(e_52) != 0
	RETURN {"e52": union_distinct([e_52], []), "gb40": union_distinct(flatten(gb40[**].gb40), []), "r51": union_distinct(flatten(gb40[**].r51), [])}
)
LET nodes = union_distinct(flatten(tree_2[**].e52), flatten(tree_2[**].gb40), [])
LET edges = union_distinct(flatten(tree_2[**].r51), [])
RETURN {"vertices":nodes,"edges":edges}`)
