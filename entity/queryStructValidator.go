/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entity

import (
	"errors"
	"strconv"
	"strings"
)

const ENTITYSTRING = "entity"
const RELATIONSTRING = "relation"
const GROUPBYSTRING = "groupBy"

/*
validateStruct validates the query JSON and makes sure it has no errors
	JSONQuery: IncomingQueryJSON, the query in JSON to be validated
	Return: []error, a list of specific transgression in this specific JSON query
*/
func validateStruct(JSONQuery *IncomingQueryJSON) []error {
	ret := make([]error, 0)
	minEntityID, maxEntityID := getMinAndMaxEntityID(JSONQuery.Entities)
	minRelationID, maxRelationID := getMinAndMaxRelationID(JSONQuery.Relations)
	minGroupByID, maxGroupByID := getMinAndMaxGroupByID(JSONQuery.GroupBys)
	ret = append(ret, getIllegalToFromInRelation(*JSONQuery, minEntityID, maxEntityID, minGroupByID, maxGroupByID)...)
	if len(JSONQuery.GroupBys) != 0 {
		ret = append(ret, getIllegalGroupByInGroupBy(*JSONQuery, minEntityID, maxEntityID, minRelationID, maxRelationID, minGroupByID, maxGroupByID)...)
		ret = append(ret, checkGroupByNames(*JSONQuery)...)
		ret = append(ret, checkGroupByModifiers(*JSONQuery)...)
	}
	if len(JSONQuery.Entities) == 0 || len(JSONQuery.Relations) == 0 {
		ret = append(ret, errors.New("No entities or relations"))
	}
	return ret
}

/*
getIllegalToFromInRelation validates the relations, and makes sure the to and from is valid
	JSONQuery: IncomingQueryJSON, the query in JSON to be validated
	minEntityID: int, the minimum value an entityID can have
	maxEntityID: int, the maximum value an entityID can have
	minGroupByID: int, the minimum value a groupByID can have
	maxGroupByID: int, the maximum value a groupByID can have
	Return: []error, a list of specific transgression in this specific JSON query
*/
func getIllegalToFromInRelation(JSONQuery IncomingQueryJSON, minEntityID int, maxEntityID int, minGroupByID int, maxGroupByID int) []error {
	ret := make([]error, 0)
	for _, rel := range JSONQuery.Relations {
		toEntityValid := relationToValid(rel, ENTITYSTRING, minEntityID, maxEntityID)
		toGroupByValid := relationToValid(rel, GROUPBYSTRING, minGroupByID, maxGroupByID)
		fromEntityValid := relationFromValid(rel, ENTITYSTRING, minEntityID, maxEntityID)
		fromGroupByValid := relationFromValid(rel, GROUPBYSTRING, minGroupByID, maxGroupByID)
		if !(toEntityValid || toGroupByValid) {
			err := errors.New("relation has invalid TO type and/or ID")
			ret = append(ret, err)
		}
		if !(fromEntityValid || fromGroupByValid) {
			err := errors.New("relation has invalid FROM type and/or ID")
			ret = append(ret, err)
		}
	}
	return ret
}

/*
getIllegalGroupByInGroupBy validates the groupBys and whether the group-, by- and relationIDs are all valid
	JSONQuery: IncomingQueryJSON, the query in JSON to be validated
	minEntityID: int, the minimum value an entityID can have
	maxEntityID: int, the maximum value an entityID can have
	minRelationID: int, the minimum value an relationID can have
	maxRelationID: int, the maximum value an relationID can have
	minGroupByID: int, the minimum value a groupByID can have
	maxGroupByID: int, the maximum value a groupByID can have
	Return: []error, a list of specific transgression in this specific JSON query
*/
func getIllegalGroupByInGroupBy(JSONQuery IncomingQueryJSON, minEntityID int, maxEntityID int, minRelationID int, maxRelationID int, minGroupByID int, maxGroupByID int) []error {
	ret := make([]error, 0)
	for _, groupBy := range JSONQuery.GroupBys {
		groupEntityValid := groupByGroupValid(groupBy, ENTITYSTRING, minEntityID, maxEntityID)
		groupGroupByValid := groupByGroupValid(groupBy, GROUPBYSTRING, minGroupByID, maxGroupByID)
		byEntityValid := groupByByValid(groupBy, ENTITYSTRING, minEntityID, maxEntityID)
		byGroupByValid := groupByByValid(groupBy, GROUPBYSTRING, minGroupByID, maxGroupByID)
		if groupBy.RelationID < minRelationID || groupBy.RelationID > maxRelationID {
			err := errors.New("group by has invalid relation")
			ret = append(ret, err)
		}
		if !(groupEntityValid || groupGroupByValid) {
			err := errors.New("group by has invalid group type and/or ID")
			ret = append(ret, err)
		}
		if !(byEntityValid || byGroupByValid) {
			err := errors.New("group by has invalid by type and/or ID")
			ret = append(ret, err)
		}
	}
	return ret
}

/*
checkGroupByNames validates the groupBys and whether the groupBy has different type of nodes connected to the by
	JSONQuery: IncomingQueryJSON, the query in JSON to be validated
	Return: []error, a list of specific transgression in this specific JSON query
*/
func checkGroupByNames(JSONQuery IncomingQueryJSON) []error {
	ret := make([]error, 0)
	entityMap, _, groupByMap := FixIndices(&JSONQuery)
	for _, groupBy := range JSONQuery.GroupBys {
		name := getGroupByByIDName(JSONQuery, groupBy, entityMap, groupByMap, groupBy.ByID[0])
		if len(groupBy.ByID) > 1 {
			for _, ID := range groupBy.ByID[1:] {
				if getGroupByByIDName(JSONQuery, groupBy, entityMap, groupByMap, ID) != name {
					ret = append(ret, errors.New("Group by "+strconv.Itoa(groupBy.ID)+" has different nodes connected to the by"))
				}
			}
		}
		name = getGroupByGroupIDName(JSONQuery, groupBy, entityMap, groupByMap, groupBy.GroupID[0])
		if len(groupBy.GroupID) > 1 {
			for _, ID := range groupBy.GroupID[1:] {
				if getGroupByGroupIDName(JSONQuery, groupBy, entityMap, groupByMap, ID) != name {
					ret = append(ret, errors.New("Group by "+strconv.Itoa(groupBy.ID)+" has different nodes connected to the group"))
				}
			}
		}
	}
	return ret
}

/*
checkGroupByModifiers validates the groupBys and whether the groupBy has incorrect modifiers
	JSONQuery: IncomingQueryJSON, the query in JSON to be validated
	Return: []error, a list of specific transgression in this specific JSON query
*/
func checkGroupByModifiers(JSONQuery IncomingQueryJSON) []error {
	ret := make([]error, 0)
	for _, groupBy := range JSONQuery.GroupBys {
		switch strings.ToLower(groupBy.AppliedModifier) {
		case "average", "avg", "max", "median", "min", "product", "stddev_population", "stddev_sample", "stddev", "sum", "variance_population", "variance_sample", "variance":
		default:
			ret = append(ret, errors.New("Group by "+strconv.Itoa(groupBy.ID)+" has an incorrect modifier"))

		}
	}
	return ret
}

/*
relationToValid checks if a relation.ToType and relation.ToID are valid
	rel: QueryRelationStruct, the relation to check
	typeString: string, the typeString the relation.ToType should be
	minID: int, the minimum ToID
	maxID: int, the maximum ToID
	Return: bool, whether the ToType and ToID are valid
*/
func relationToValid(rel QueryRelationStruct, typeString string, minID int, maxID int) bool {
	if rel.ToType == typeString && rel.ToID >= minID && rel.ToID <= maxID {
		return true
	}
	return false
}

/*
relationFromValid checks if a relation.FromType and relation.FromID are valid
	rel: QueryRelationStruct, the relation to check
	typeString: string, the typeString the relation.FromType should be
	minID: int, the minimum FromID
	maxID: int, the maximum FromID
	Return: bool, whether the FromType and FromID are valid
*/
func relationFromValid(rel QueryRelationStruct, typeString string, minID int, maxID int) bool {
	if rel.FromType == typeString && rel.FromID >= minID && rel.FromID <= maxID {
		return true
	}
	return false
}

/*
groupByGroupValid checks if a groupBy.GroupType and groupBy.GroupID are valid
	groupBy: QueryGroupByStruct, the groupBy to check
	typeString: string, the typeString the groupBy.GroupType should be
	minID: int, the minimum GroupID
	maxID: int, the maximum GroupID
	Return: bool, whether the GroupType and GroupID are valid
*/
func groupByGroupValid(groupBy QueryGroupByStruct, typeString string, minID int, maxID int) bool {
	// if groupBy.GroupType == typeString && groupBy.GroupID >= minID && groupBy.GroupID <= maxID {
	// 	return true
	// }
	// return false
	return true
}

/*
groupByByValid checks if a groupBy.ByType and groupBy.ByID are valid
	groupBy: QueryGroupByStruct, the groupBy to check
	typeString: string, the typeString the groupBy.GroupType should be
	minID: int, the minimum ByID
	maxID: int, the maximum ByID
	Return: bool, whether the ByType and ByID are valid
*/
func groupByByValid(groupBy QueryGroupByStruct, typeString string, minID int, maxID int) bool {
	// if groupBy.ByType == typeString && groupBy.ByID >= minID && groupBy.ByID <= maxID {
	// 	return true
	// }
	// return false
	return true
}

/*
getMinAndMaxEntityID gets the minimum and maximum entity IDs
	entities: []QueryEntityStruct, the entities for which to get the min and max IDs
	Return: (int, int), the min and max IDs
*/
func getMinAndMaxEntityID(entities []QueryEntityStruct) (int, int) {
	min := 65535
	max := -65535
	for _, e := range entities {
		if e.ID < min {
			min = e.ID
		}
		if e.ID > max {
			max = e.ID
		}
	}
	// If the min/max values didn't change the query would be invalid, and all consequent validationsteps will fail
	return min, max
}

/*
getMinAndMaxRelationID gets the minimum and maximum relation IDs
	relations: []QueryRelationStruct, the relations for which to get the min and max IDs
	Return: (int, int), the min and max IDs
*/
func getMinAndMaxRelationID(relations []QueryRelationStruct) (int, int) {
	min := 65535
	max := -65535
	for _, e := range relations {
		if e.ID < min {
			min = e.ID
		}
		if e.ID > max {
			max = e.ID
		}
	}
	// If the min/max values didn't change the query would be invalid, and all consequent validationsteps will fail

	return min, max
}

/*
getMinAndMaxGroupByID gets the minimum and maximum groupby IDs
	groupBys: []QueryGroupByStruct, the groupbys for which to get the min and max IDs
	Return: (int, int), the min and max IDs
*/
func getMinAndMaxGroupByID(groupBys []QueryGroupByStruct) (int, int) {
	min := 65535
	max := -65535
	for _, e := range groupBys {
		if e.ID < min {
			min = e.ID
		}
		if e.ID > max {
			max = e.ID
		}
	}
	// If the min/max values didn't change the query would be invalid, and all consequent validationsteps will fail

	return min, max
}

/*
hasSeperateChains checks whether a hierarchy has two completely seperated chains
	treeList: TreeList, the treeList to check
	Return: []error, the possible errors with the hierarchy
*/
func hasSeperateChains(treeList TreeList) []error {
	ret := make([]error, 0)
	groupByToTrees, treeToGroupBys := getGroupByToTreeAndTreeToGroupBy(treeList.Trees)
	discovered := findMainChain(0, groupByToTrees, treeToGroupBys, make(map[int]bool))
	if len(discovered) < len(treeList.Trees) {
		ret = append(ret, errors.New("Seperate chains detected"))
	}
	return ret
}

/*
getGroupByToTreeAndTreeToGroupBy generates two maps to quickly get the groupbys used or generated in chains
	treeLists: []Tree, the trees to incorporate in the maps
	Return: (map[int][]int, map[int][]int), the groupByToTrees and treeToGroupBys maps
*/
func getGroupByToTreeAndTreeToGroupBy(treeLists []Tree) (map[int][]int, map[int][]int) {
	groupByToTrees := make(map[int][]int)
	treeToGroupBys := make(map[int][]int)
	for i, treeList := range treeLists {
		treeToGroupBys[i] = make([]int, 0)
		for _, groupBy := range treeList.AllGroupBys {
			treeToGroupBys[i] = append(treeToGroupBys[i], groupBy.ID)
			if _, ok := groupByToTrees[groupBy.ID]; !ok {
				groupByToTrees[groupBy.ID] = make([]int, 0)
			}
			groupByToTrees[groupBy.ID] = append(groupByToTrees[groupBy.ID], i)
		}
		for _, tree := range treeList.TreeElements {
			if tree.Self.FromNode.Name == "" {
				groupBy := tree.Self.FromGroupBy
				treeToGroupBys[i] = append(treeToGroupBys[i], groupBy.ID)
				if _, ok := groupByToTrees[groupBy.ID]; !ok {
					groupByToTrees[groupBy.ID] = make([]int, 0)
				}
				groupByToTrees[groupBy.ID] = append(groupByToTrees[groupBy.ID], i)
			}
			if tree.Self.ToNode.Name == "" {
				groupBy := tree.Self.ToGroupBy
				treeToGroupBys[i] = append(treeToGroupBys[i], groupBy.ID)
				if _, ok := groupByToTrees[groupBy.ID]; !ok {
					groupByToTrees[groupBy.ID] = make([]int, 0)
				}
				groupByToTrees[groupBy.ID] = append(groupByToTrees[groupBy.ID], i)
			}
		}
	}
	return groupByToTrees, treeToGroupBys
}

/*
findMainChain starts at the first chain, and depth first searches for every chain connected to it
	i: int, the current tree index
	groupByToTrees: map[int][]int, the map to quickly get from groupby id to tree id
	treesToGroupBys: map[int][]int, the map to quickly get from tree id to groupby id
	discovered: map[int]bool, the map of already discovered trees
	Return: map[int]bool, an updated map of discovered trees
*/
func findMainChain(i int, groupByToTrees map[int][]int, treesToGroupBys map[int][]int, discovered map[int]bool) map[int]bool {
	discovered[i] = true
	for _, ID := range treesToGroupBys[i] {
		for _, nextTree := range groupByToTrees[ID] {
			if _, ok := discovered[nextTree]; nextTree != i && !ok {
				discovered = findMainChain(nextTree, groupByToTrees, treesToGroupBys, discovered)
			}
		}
	}
	return discovered
}

/*
getGroupByByIDName recurses over groupby by until it finds an entity and returns its name
	JSONQuery: IncomingQueryJSON, the converted JSON to be validated
	groupBy: QueryGroupByStruct, the groupBy to get the by name for
	entityMap: map[int]int, a map to convert entity IDs to their indices in the JSONQuery.Entities
	groupByMap: map[int]int, a map to convert groupby IDs to their indices in the JSONQuery.GroupBys
	Return: string, the name of the by of the groupby
*/
func getGroupByByIDName(JSONQuery IncomingQueryJSON, groupBy QueryGroupByStruct, entityMap map[int]int, groupByMap map[int]int, id int) string {
	if groupBy.ByType == "entity" {
		return JSONQuery.Entities[entityMap[id]].Name
	}
	return getGroupByByIDName(JSONQuery, JSONQuery.GroupBys[groupByMap[id]], entityMap, groupByMap, JSONQuery.GroupBys[groupByMap[id]].ByID[0])
}

/*
getGroupByGroupIDName recurses over groupby group until it finds an entity and returns its name
	JSONQuery: IncomingQueryJSON, the converted JSON to be validated
	groupBy: QueryGroupByStruct, the groupBy to get the group name for
	entityMap: map[int]int, a map to convert entity IDs to their indices in the JSONQuery.Entities
	groupByMap: map[int]int, a map to convert groupby IDs to their indices in the JSONQuery.GroupBys
	Return: string, the name of the group of the groupby
*/
func getGroupByGroupIDName(JSONQuery IncomingQueryJSON, groupBy QueryGroupByStruct, entityMap map[int]int, groupByMap map[int]int, id int) string {
	if groupBy.GroupType == "entity" {
		return JSONQuery.Entities[entityMap[id]].Name
	}
	return getGroupByGroupIDName(JSONQuery, JSONQuery.GroupBys[groupByMap[id]], entityMap, groupByMap, JSONQuery.GroupBys[groupByMap[id]].GroupID[0])
}
