/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entity

type Validator interface {
	Validate() []error
}

func (JSONQuery *IncomingQueryJSON) Validate() []error {
	return validateStruct(JSONQuery)
}

func (treeLists TreeList) Validate() []error {
	return hasSeperateChains(treeLists)
}
