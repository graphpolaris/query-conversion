/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entity

import (
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHierarchyBasic(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(HierarchyBasic, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &HierarchyBasicTreeList)
}

/*
Tests two entities (two types) without a filter
Query description: Give me all parties connected to their respective parliament members
	t: *testing.T, makes go recognise this as a test
*/
func TestTwoEntitiesNoFilterHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(TwoEntitiesNoFilter, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &TwoEntitiesNoFilterTreeList)
}

/*
Tests two entities (two types) with one entity filter
Query description: Give me all parties, with less than 10 seats, connected to their respective parliament members
	t: *testing.T, makes go recognise this as a test
*/
func TestTwoEntitiesOneEntityFilterHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(TwoEntitiesOneEntityFilter, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &TwoEntitiesOneEntityFilterTreeList)
}

/*
Tests two entities (two types) with two entity filters
Query description: Give me all parties, with less than 10 seats, connected to their respective parliament members, who are more than 45 years old
	t: *testing.T, makes go recognise this as a test
*/
func TestTwoEntitiesTwoEntityFiltersHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(TwoEntitiesTwoEntityFilters, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &TwoEntitiesTwoEntityFiltersTreeList)
}

/*
Tests three entities (three types) without a filter
Query description: Give me all parties, connected to their respective parliament members, who are then connected to the resolutions they submitted
	t: *testing.T, makes go recognise this as a test
*/
func TestThreeEntitiesNoFilterHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(ThreeEntitiesNoFilter, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &ThreeEntitiesNoFilterTreeList)
}

/*
Tests three entities (three types) with one entity filter
Query description: Give me all parties, connected to their respective parliament members, whose name has "Geert" in it (this results in only "Geert Wilders"), who are/is then connected to the resolutions they submitted
	t: *testing.T, makes go recognise this as a test
*/
func TestThreeEntitiesOneEntityFilterHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(ThreeEntitiesOneEntityFilter, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &ThreeEntitiesOneEntityFilterTreeList)
}

/*
Tests three entities (three types) with two entity filters
Query description: Give me all parties, connected to their respective parliament members, whose name has "Geert" in it (this results in only "Geert Wilders"), who are/is then connected to the resolutions they submitted, but only those submitted in May
	t: *testing.T, makes go recognise this as a test
*/
func TestThreeEntitiesTwoEntityFiltersHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(ThreeEntitiesTwoEntityFilters, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &ThreeEntitiesTwoEntityFiltersTreeList)
}

/*
Tests five entities (three types) with four entity filters
Query description: Give me all parties, which have less than 10 seats, connected to their respective parliament members, whose name has "A" in it, who are/is then connected to the resolutions they submitted, but only those submitted in May, which are then also connected to all other persons who were part of that submission, who are part of the "VVD"
Translator's note: This returns a member of the PvdA who submitted a motion alongside a member of the VVD
t: *testing.T, makes go recognise this as a test
*/
func TestFiveEntitiesFourEntityFiltersHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(FiveEntitiesFourEntityFilters, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &FiveEntitiesFourEntityFiltersTreeList)
}

/*
Tests five entities (four types) with three entity filters and one junction
Query description: Give me all parties, with less than 10 seats, connected to their respective parliament members, who are then connected to the resolutions they submitted, but only those submitted in May, and connected to the comissions they're in, which are then connected to all of their members, but only those with "Geert" in their name (resulting in only "Geert Wilders")
	t: *testing.T, makes go recognise this as a test
*/
func TestSingleJunctionFiveEntitiesThreeEntityFiltersHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(SingleJunctionFiveEntitiesThreeEntityFilters, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &SingleJunctionFiveEntitiesThreeEntityFiltersTreeList)
}

/*
Tests nine entities (four types) with three entity filters and two junctions
Query description: Give me all parties, with less than 10 seats, connected to their respective parliament members, who are then connected to the resolutions they submitted, but only those submitted in May, and connected to the comissions they're in, which are then connected to all of their members, but only those with "Geert" in their name (resulting in only "Geert Wilders"), who is then connected to their submited resolutions and their party, which is connected to all of its members
	t: *testing.T, makes go recognise this as a test
*/
func TestDoubleJunctionNineEntitiesThreeEntityFiltersHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(DoubleJunctionNineEntitiesThreeEntityFilters, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &DoubleJunctionNineEntitiesThreeEntityFiltersTreeList)
}

/*
Tests two entities (one type) with one entity filter and one relation filter
Query description: Give me all airports, in the state "HI", connected to any other airport by flight, but only the flights on "day" 15
	t: *testing.T, makes go recognise this as a test
*/
func TestTwoEntitiesOneEntityFilterOneRelationFilterHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(TwoEntitiesOneEntityFilterOneRelationFilter, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &TwoEntitiesOneEntityFilterOneRelationFilterTreeList)
}

/*
Tests a query with a single group by as an endpoint
	t: *testing.T, makes go recognise this as a test
*/
func TestSingleEndPointGroupByHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(SingleEndPointGroupBy, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &SingleEndPointGroupByTreeList)
}

/*
Tests a query with a single group by which is connected to a relation
	t: *testing.T, makes go recognise this as a test
*/
func TestSingleGroupByHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(SingleGroupBy, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &SingleGroupByTreeList)
}

/*
Tests a query with a group by with multiple inputs which is an endpoint
	t: *testing.T, makes go recognise this as a test
*/
func TestMultipleInputGroupByEndpointHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(MultipleInputGroupByEndpoint, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &MultipleInputGroupByEndpointTreeList)
}

/*
Tests a query with a group by with multiple inputs which is connected to a relation
	t: *testing.T, makes go recognise this as a test
*/
func TestMultipleInputGroupByHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(MultipleInputGroupBy, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &MultipleInputGroupByTreeList)
}

/*
Tests a query with a group by as the top pill
	t: *testing.T, makes go recognise this as a test
*/
func TestTopGroupByHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(TopGroupBy, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &TopGroupByTreeList)
}

/*
Tests a large query with multiple groupbys nested and multiple chained filters and chained subqueries
	t: *testing.T, makes go recognise this as a test
*/
func TestWayTooLargeQueryHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(WayTooLargeQuery, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &WayTooLargeQueryTreeList)
}

/*
Tests a query with nested group bys
	t: *testing.T, makes go recognise this as a test
*/
func TestAllStreamersHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(AllStreamers, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &AllStreamersTreeList)
}

/*
Tests the string match types
	t: *testing.T, makes go recognise this as a test
*/
func TestStringMatchTypesHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(StringMatchTypes, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &StringMatchTypesTreeList)
}

/*
Tests a query where a top pill is the to, and is connected to a from group by
	t: *testing.T, makes go recognise this as a test
*/
func TestTopEntityToFromGroupByHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(TopEntityToFromGroupBy, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &TopEntityToFromGroupByTreeList)
}

/*
Tests in not equals filter
	t: *testing.T, makes go recognise this as a test
*/
func TestIntNotEqualsHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(IntNotEquals, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &IntNotEqualsTreeList)
}

/*
Tests bool equals filter
	t: *testing.T, makes go recognise this as a test
*/
func TestBoolEqualHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(BoolEqual, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &BoolEqualTreeList)
}

/*
Tests bool not equals filter
	t: *testing.T, makes go recognise this as a test
*/
func TestBoolNotEqualHierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(BoolNotEqual, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &BoolNotEqualTreeList)
}

/*
Tests a different nested group by which will have to propagate to get group by type
	t: *testing.T, makes go recognise this as a test
*/
func TestNestedGroupBys2Hierarchy(t *testing.T) {
	// Unmarshall the incoming message into an IncomingJSONQuery object
	var JSONQuery IncomingQueryJSON
	json.Unmarshal(NestedGroupBys2, &JSONQuery)
	entityMap, relationMap, groupByMap := FixIndices(&JSONQuery)
	// Get the hierarchy and turn it into JSON so we can turn it into strings later
	_, treeList := CreateHierarchy(&JSONQuery, entityMap, relationMap, groupByMap)
	//jsonTopNode, err := json.Marshal(topNode)
	//if err != nil {
	//	fmt.Println("Marshalling went wrong")
	//}
	jsonTreeList, err := json.Marshal(treeList)
	if err != nil {
		fmt.Println("Marshalling went wrong")
	}
	// These are the expected (correct) outputs
	CheckTreeResults(t, &jsonTreeList, &NestedGroupBys2TreeList)
}

func CheckTreeResults(t *testing.T, jsonTreeList *[]byte, correctTreeList *string) {
	cleanedTreeList := strings.ReplaceAll(string(*jsonTreeList), "\n", "")
	cleanedTreeList = strings.ReplaceAll(cleanedTreeList, "\t", "")
	cleanedTreeList = strings.ReplaceAll(cleanedTreeList, " ", "")

	cleanedCorrectTreeList := strings.ReplaceAll(*correctTreeList, "\n", "")
	cleanedCorrectTreeList = strings.ReplaceAll(cleanedCorrectTreeList, "\t", "")
	cleanedCorrectTreeList = strings.ReplaceAll(cleanedCorrectTreeList, " ", "")

	assert.Equal(t, cleanedCorrectTreeList, cleanedTreeList)
}
