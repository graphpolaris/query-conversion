/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entity

/*
FixIndices maps pill IDs to the JSONQuery slice indices
	JSONQuery: *IncomingQueryJSON, the converted query JSON for which the maps must be generated
	Return: (map[int]int, map[int]int, map[int]int), the entity, relation and groupBy conversion maps
*/
func FixIndices(JSONQuery *IncomingQueryJSON) (map[int]int, map[int]int, map[int]int) {
	entityMap := make(map[int]int)
	for i, e := range JSONQuery.Entities {
		entityMap[e.ID] = i
	}
	relationMap := make(map[int]int)
	for i, r := range JSONQuery.Relations {
		relationMap[r.ID] = i
	}
	groupByMap := make(map[int]int)
	for i, g := range JSONQuery.GroupBys {
		groupByMap[g.ID] = i
	}
	return entityMap, relationMap, groupByMap
}
