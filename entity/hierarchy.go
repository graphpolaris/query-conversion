/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entity

import (
	"errors"
)

var relationdone map[int]bool
var groupbydone map[int]bool
var nodedone map[int]bool

/*
CreateHierarchy creates the hierarchy for a query json
	JSONQuery: *IncomingQueryJSON, the converted query JSON for which the hierarchy must be generated
	entityMap: map[int]int, a map to convert entity IDs to their indices in the JSONQuery.Entities
	relationMap: map[int]int, a map to convert relation IDs to their indices in the JSONQuery.Relations
	groupByMap: map[int]int, a map to convert groupby IDs to their indices in the JSONQuery.GroupBys
	Return: (error, TreeList), and error and a hierarchy in the form of a TreeList
*/
func CreateHierarchy(JSONQuery *IncomingQueryJSON, entityMap map[int]int, relationMap map[int]int, groupByMap map[int]int) (error, TreeList) {
	var fullTree TreeList
	relationdone = make(map[int]bool) // Makes sure relations don't get explored twice
	groupbydone = make(map[int]bool)  // Makes sure groupbys don't get explored twice
	nodedone = make(map[int]bool)     // Makes sure nodes don't get explored twice
	// While the full json hasn't been explored yet, keep exploring
	for len(groupbydone) < len(JSONQuery.GroupBys) || len(nodedone) < len(JSONQuery.Entities) {
		// One iteration of this loop explores a single query chain. A query chain is a chain of elements and relations connected together. A query can contain multiple chains, which are seperated by groupbys.
		var treeList Tree
		var topTree TreeElement
		var hasTriple bool
		nodeFound, topNode := getTopNode(JSONQuery, entityMap)
		if nodeFound {
			nodedone[entityMap[topNode.ID]] = true
			topTreeSelfTriple := getTripleFromNode(JSONQuery, topNode, entityMap, relationMap, groupByMap)
			topTree = TreeElement{
				Self:     topTreeSelfTriple,
				Parent:   -1,
				Children: []int{},
			}
			treeList.TopNode = topNode
			hasTriple = true
		} else {
			err, topGroupBy := getTopGroupBy(JSONQuery, groupByMap)
			if err != nil {
				return err, fullTree
			}
			groupbydone[groupByMap[topGroupBy.ID]] = true
			tripleFound, topTreeSelfTriple := getTripleFromGroupBy(JSONQuery, topGroupBy, entityMap, relationMap, groupByMap)
			if tripleFound {
				topTree = TreeElement{
					Self:     topTreeSelfTriple,
					Parent:   -1,
					Children: []int{},
				}
			}
			hasTriple = tripleFound
			if tripleFound {
				treeList.TopGroupBy = topGroupBy
			}
		}
		if hasTriple {
			treeList.TreeElements = append(treeList.TreeElements, topTree)
			treeListIndex := len(treeList.TreeElements) - 1
			treeList.TreeElements = getChildrenFromTree(JSONQuery, treeList.TreeElements, treeListIndex, 0, entityMap, relationMap, groupByMap)
			fullTree.Trees = append(fullTree.Trees, treeList)
		}
	}
	// If there are groupbys, there must be more than one chain. The order in which chains are generated is important, therefore they must be sorted.
	if len(JSONQuery.GroupBys) > 0 {
		fullTree = sortTreeList(JSONQuery, fullTree, groupByMap)
	}
	return nil, fullTree
}

/*
getTopNode gets an entity that has only 1 relation attached
	JSONQuery: *IncomingQueryJSON, the converted query JSON for which the top node must be found
	entityMap: map[int]int, a map to convert entity IDs to their indices in the JSONQuery.Entities
	Return: (bool, QueryEntityStruct), a bool which signifies whether a top node has been found, and the top node found
*/
func getTopNode(JSONQuery *IncomingQueryJSON, entityMap map[int]int) (bool, QueryEntityStruct) {
	indexOfNodeToReturn := -1
	for i, node := range JSONQuery.Entities {
		if nodedone[entityMap[node.ID]] {
			continue
		}
		connectionCount := 0
		for _, relation := range JSONQuery.Relations {
			if (relation.FromType == ENTITYSTRING && relation.FromID == node.ID) || (relation.ToType == ENTITYSTRING && relation.ToID == node.ID) {
				connectionCount++
			}
		}
		if connectionCount == 1 {
			indexOfNodeToReturn = i
			return true, JSONQuery.Entities[indexOfNodeToReturn]
		}
	}
	return false, JSONQuery.Entities[0]
}

/*
getTopGroupBy gets a group by that has only 1 relation attached
	JSONQuery: *IncomingQueryJSON, the converted query JSON for which the top groupby must be found
	groupByMap: map[int]int, a map to convert groupby IDs to their indices in the JSONQuery.GroupBy
	Return: (bool, QueryGroupByStruct), a bool which signifies whether a top node has been found, and the top groupby found
*/
func getTopGroupBy(JSONQuery *IncomingQueryJSON, groupByMap map[int]int) (error, QueryGroupByStruct) {
	indexOfGroupByToReturn := -1
	for i, groupBy := range JSONQuery.GroupBys {
		if groupbydone[i] {
			continue
		}
		connectionCount := 0
		for _, relation := range JSONQuery.Relations {
			if (relation.FromType == GROUPBYSTRING && relation.FromID == groupBy.ID) || (relation.ToType == GROUPBYSTRING && relation.ToID == groupBy.ID) {
				connectionCount++
			}
		}
		if connectionCount <= 1 {
			indexOfGroupByToReturn = i
			return nil, JSONQuery.GroupBys[indexOfGroupByToReturn]
		}
	}
	return errors.New("No top groupby found"), JSONQuery.GroupBys[0]
}

/*
getTripleFromNode gets an element-relation-element triple containing a certain node
	JSONQuery: *IncomingQueryJSON, the converted query JSON
	node: QueryEntityStruct, the node for which the triple must be found
	entityMap: map[int]int, a map to convert entity IDs to their indices in the JSONQuery.Entities
	relationMap: map[int]int, a map to convert relation IDs to their indices in the JSONQuery.Relations
	groupByMap: map[int]int, a map to convert groupby IDs to their indices in the JSONQuery.GroupBys
	Return: Triple, the triple containing the node
*/
func getTripleFromNode(JSONQuery *IncomingQueryJSON, node QueryEntityStruct, entityMap map[int]int, relationMap map[int]int, groupByMap map[int]int) Triple {
	var tripleToReturn Triple
	for _, relation := range JSONQuery.Relations {
		if (relation.FromType == ENTITYSTRING && relation.FromID == node.ID) && relation.ToType == ENTITYSTRING { // Original is the from and to is entity
			tripleToReturn.FromNode = node
			tripleToReturn.Rel = relation
			tripleToReturn.ToNode = JSONQuery.Entities[entityMap[relation.ToID]]
		} else if (relation.ToType == ENTITYSTRING && relation.ToID == node.ID) && relation.FromType == ENTITYSTRING { // Original is the to and from is entity
			tripleToReturn.FromNode = JSONQuery.Entities[entityMap[relation.FromID]]
			tripleToReturn.Rel = relation
			tripleToReturn.ToNode = node
		} else if (relation.FromType == ENTITYSTRING && relation.FromID == node.ID) && relation.ToType == GROUPBYSTRING { // Original is the from and to is groupby
			tripleToReturn.FromNode = node
			tripleToReturn.Rel = relation
			tripleToReturn.ToGroupBy = JSONQuery.GroupBys[groupByMap[relation.ToID]]
		} else if (relation.ToType == ENTITYSTRING && relation.ToID == node.ID) && relation.FromType == GROUPBYSTRING { // Original is the to and from is groupby
			tripleToReturn.FromGroupBy = JSONQuery.GroupBys[groupByMap[relation.FromID]]
			tripleToReturn.Rel = relation
			tripleToReturn.ToNode = node
		}
	}
	relationdone[relationMap[tripleToReturn.Rel.ID]] = true
	return tripleToReturn
}

/*
getTripleFromGroupBy gets an element-relation-element triple containing a certain groupby
	JSONQuery: *IncomingQueryJSON, the converted query JSON
	groupBy: QueryGroupByStruct, the groupby for which the triple must be found
	entityMap: map[int]int, a map to convert entity IDs to their indices in the JSONQuery.Entities
	relationMap: map[int]int, a map to convert relation IDs to their indices in the JSONQuery.Relations
	groupByMap: map[int]int, a map to convert groupby IDs to their indices in the JSONQuery.GroupBys
	Return: (bool, Triple), whether a triple has been found, and the triple containing the groupby
*/
func getTripleFromGroupBy(JSONQuery *IncomingQueryJSON, groupBy QueryGroupByStruct, entityMap map[int]int, relationMap map[int]int, groupByMap map[int]int) (bool, Triple) {
	tripleFound := false
	var tripleToReturn Triple
	for _, relation := range JSONQuery.Relations {
		// The initial node was our From so we set the Triple accordingly
		if (relation.FromType == GROUPBYSTRING && relation.FromID == groupBy.ID) && relation.ToType == ENTITYSTRING { // Original is the from and to is entity
			tripleToReturn.FromGroupBy = groupBy
			tripleToReturn.Rel = relation
			tripleToReturn.ToNode = JSONQuery.Entities[entityMap[relation.ToID]]
			tripleFound = true
		} else if (relation.ToType == GROUPBYSTRING && relation.ToID == groupBy.ID) && relation.FromType == ENTITYSTRING { // Original is the to and from is entity
			tripleToReturn.FromNode = JSONQuery.Entities[entityMap[relation.FromID]]
			tripleToReturn.Rel = relation
			tripleToReturn.ToGroupBy = groupBy
			tripleFound = true
		} else if (relation.FromType == GROUPBYSTRING && relation.FromID == groupBy.ID) && relation.ToType == GROUPBYSTRING { // Original is the from and to is groupby
			tripleToReturn.FromGroupBy = groupBy
			tripleToReturn.Rel = relation
			tripleToReturn.ToGroupBy = JSONQuery.GroupBys[groupByMap[relation.ToID]]
			tripleFound = true
		} else if (relation.ToType == GROUPBYSTRING && relation.ToID == groupBy.ID) && relation.FromType == GROUPBYSTRING { // Original is the to and from is groupby
			tripleToReturn.FromGroupBy = JSONQuery.GroupBys[groupByMap[relation.FromID]]
			tripleToReturn.Rel = relation
			tripleToReturn.ToGroupBy = groupBy
			tripleFound = true
		}
	}
	relationdone[relationMap[tripleToReturn.Rel.ID]] = true
	return tripleFound, tripleToReturn
}

/*
getTripleFromRelation gets an element-relation-element triple containing a certain relation
	JSONQuery: *IncomingQueryJSON, the converted query JSON
	relation: QueryRelationStruct, the relation for which the triple must be found
	entityMap: map[int]int, a map to convert entity IDs to their indices in the JSONQuery.Entities
	groupByMap: map[int]int, a map to convert groupby IDs to their indices in the JSONQuery.GroupBys
	Return: Triple, the triple containing the relation
*/
func getTripleFromRelation(JSONQuery *IncomingQueryJSON, relation QueryRelationStruct, entityMap map[int]int, groupByMap map[int]int) Triple {
	var tripleToReturn Triple
	if relation.FromType == ENTITYSTRING {
		tripleToReturn.FromNode = JSONQuery.Entities[entityMap[relation.FromID]]
	} else {
		tripleToReturn.FromGroupBy = JSONQuery.GroupBys[groupByMap[relation.FromID]]
	}
	tripleToReturn.Rel = relation
	if relation.ToType == ENTITYSTRING {
		tripleToReturn.ToNode = JSONQuery.Entities[entityMap[relation.ToID]]
	} else {
		tripleToReturn.ToGroupBy = JSONQuery.GroupBys[groupByMap[relation.ToID]]
	}
	return tripleToReturn
}

/*
getChildrenFromTree given a parent element, gets all triples containing that element, and recurses over the other side of those triples to explore the full query chain
	JSONQuery: *IncomingQueryJSON, the converted query JSON
	treeList: []TreeElement, the list of treeElements which must be appended to
	treeListIndex: int, the location the elements must be appended to
	parentIndex: int, the location of the parent in the treeList
	entityMap: map[int]int, a map to convert entity IDs to their indices in the JSONQuery.Entities
	relationMap: map[int]int, a map to convert relation IDs to their indices in the JSONQuery.Relations
	groupByMap: map[int]int, a map to convert groupby IDs to their indices in the JSONQuery.GroupBys
	Return: []TreeElement, the fully explored list of triples for the current chain
*/
func getChildrenFromTree(JSONQuery *IncomingQueryJSON, treeList []TreeElement, treeListIndex int, parentIndex int, entityMap map[int]int, relationMap map[int]int, groupByMap map[int]int) []TreeElement {
	var childRelationTriples []Triple
	parent := treeList[parentIndex]
	// Get the ID of the not done part of the relation
	var ID int
	var isentity bool
	var newType string
	if parent.Self.FromNode.Name == "" && !groupbydone[groupByMap[parent.Self.FromGroupBy.ID]] { // We aren't a node and the groupby isn't done
		ID = parent.Self.FromGroupBy.ID
		isentity = false
		newType = "groupBy"
	} else if parent.Self.FromNode.Name != "" && !nodedone[entityMap[parent.Self.FromNode.ID]] { // We are a node and the node isn't done
		ID = parent.Self.FromNode.ID
		isentity = true
		newType = "entity"
	} else if parent.Self.ToNode.Name == "" && !groupbydone[groupByMap[parent.Self.ToGroupBy.ID]] { // We aren't a node and the groupby isn't done
		ID = parent.Self.ToGroupBy.ID
		isentity = false
		newType = "groupBy"
	} else {
		ID = parent.Self.ToNode.ID
		isentity = true
		newType = "entity"
	}
	for i, relation := range JSONQuery.Relations {
		// We found a relation that is not our parent relation so we can check if it matches our not done
		// If it matches our not done
		if _, ok := relationdone[i]; !ok {
			if relation.FromID == ID && relation.FromType == newType || relation.ToID == ID && relation.ToType == newType {
				triple := getTripleFromRelation(JSONQuery, relation, entityMap, groupByMap)
				childRelationTriples = append(childRelationTriples, triple)
				relationdone[i] = true
			}
		}
	}
	if isentity {
		nodedone[entityMap[ID]] = true
	} else {
		groupbydone[groupByMap[ID]] = true
	}
	// We now have all our children, so we can now make those trees and find their children
	// We can now also add the indices to the list of children from the tree calling this function
	if len(childRelationTriples) != 0 {
		for _, triple := range childRelationTriples {
			childTree := TreeElement{
				Self:     triple,
				Parent:   parentIndex,
				Children: []int{},
			}
			treeList = append(treeList, childTree)
			// We get the new treeListIndex, which we can now add to the list of children from the tree calling this function
			treeListIndex = len(treeList) - 1
			treeList[parentIndex].Children = append(treeList[parentIndex].Children, treeListIndex)
			treeList = getChildrenFromTree(JSONQuery, treeList, treeListIndex, treeListIndex, entityMap, relationMap, groupByMap)
		}
	}
	return treeList
}

/*
sortTreeList sorts chains to ensure proper order in group by generation
	JSONQuery: *IncomingQueryJSON, the converted query JSON
	treeList: TreeList, the treeList that must be sorted
	groupByMap: map[int]int, a map to convert groupby IDs to their indices in the JSONQuery.GroupBys
	Return: TreeList, the sorted treeList
*/
func sortTreeList(JSONQuery *IncomingQueryJSON, treeList TreeList, groupByMap map[int]int) TreeList {
	groupbyTree := make(map[int]int)
	groupbydone = make(map[int]bool)
	groupByDoneMap := createGroupByDoneMap(JSONQuery)
	treeListDone := make(map[int]bool)
	var output TreeList
	// Keep lopping until all chains have been sorted.
	for len(treeListDone) < len(treeList.Trees) {
		// Since the chains can be in any order, once a new chain has been put into the list start back at the top.
		// Basically selection sort.
		for i, tree := range treeList.Trees { // List of relations (trees/chains)
			groupByDoneForTreeList := make(map[int]bool)
			allGroupByDoneForTreeList := make(map[int]bool)
			if treeListDone[i] {
				continue
			}
			if treeListReady(groupbydone, tree, groupByMap) {
				for _, treeElement := range tree.TreeElements { // Single relation
					hasGroupBy, groupBys := tryGetGroupByFromRelation(JSONQuery, treeElement.Self.Rel) // Gets the group bys that use the current relation
					if hasGroupBy {
						for _, groupBy := range groupBys {
							groupbyTree[groupByMap[groupBy.ID]] = i
							groupBySlowDone := groupByDoneMap[groupByMap[groupBy.ID]] // A thorough but slow implementation to check whether entire group by is done
							if groupBy.RelationID == treeElement.Self.Rel.ID {
								groupBySlowDone.RelationDone = true
							}
							if _, ok := groupBySlowDone.ByIDDone[treeElement.Self.Rel.FromID]; ok && groupBy.ByType == treeElement.Self.Rel.FromType { // Groupby by not yet done and by is current from
								groupBySlowDone.ByIDDone[treeElement.Self.Rel.FromID] = true
							}
							if _, ok := groupBySlowDone.ByIDDone[treeElement.Self.Rel.ToID]; ok && groupBy.ByType == treeElement.Self.Rel.ToType { // Groupby by not yet done and by is current to
								groupBySlowDone.ByIDDone[treeElement.Self.Rel.ToID] = true
							}
							if _, ok := groupBySlowDone.GroupIDDone[treeElement.Self.Rel.FromID]; ok && groupBy.GroupType == treeElement.Self.Rel.FromType { // Groupby group not yet done and by is current from
								groupBySlowDone.GroupIDDone[treeElement.Self.Rel.FromID] = true
							}
							if _, ok := groupBySlowDone.GroupIDDone[treeElement.Self.Rel.ToID]; ok && groupBy.GroupType == treeElement.Self.Rel.ToType { // Groupby group not yet done and by is current to
								groupBySlowDone.GroupIDDone[treeElement.Self.Rel.ToID] = true
							}
							groupByDoneMap[groupByMap[groupBy.ID]] = groupBySlowDone
							groupbydone[groupByMap[groupBy.ID]] = groupByDone(groupBySlowDone)
							if _, ok := groupByDoneForTreeList[groupByMap[groupBy.ID]]; !ok && groupbydone[groupByMap[groupBy.ID]] {
								tree.GroupBys = append(tree.GroupBys, groupBy)
								groupByDoneForTreeList[groupByMap[groupBy.ID]] = true // This field is used for implementing the group bys later
							}
							if _, ok := allGroupByDoneForTreeList[groupByMap[groupBy.ID]]; !ok {
								tree.AllGroupBys = append(tree.AllGroupBys, groupBy)
								allGroupByDoneForTreeList[groupByMap[groupBy.ID]] = true // This field is used for validation of the hierarchy later
							}

						}
					}
				}
				treeListDone[i] = true
				output.Trees = append(output.Trees, tree)
				break
			}
		}
	}
	// Set parents
	for i, tree := range output.Trees {
		parents := make([]int, 0)
		for _, treeElement := range tree.TreeElements {
			if treeElement.Self.FromNode.Name == "" {
				tree.Parents = append(parents, groupbyTree[groupByMap[treeElement.Self.FromGroupBy.ID]])
			}
			if treeElement.Self.ToNode.Name == "" {
				tree.Parents = append(parents, groupbyTree[groupByMap[treeElement.Self.ToGroupBy.ID]])
			}
		}
		output.Trees[i] = tree
	}
	return output
}

/*
treeListReady checks for every group by used in a chain if the dependent chains to generate it have been sorted yet. If so, this chain can be sorted too
	groupbydone: map[int]bool, the map of groupbys which are done and can be used in triples
	tree: Tree, the tree that must be checked for incomplete groupbys
	groupByMap: map[int]int, a map to convert groupby IDs to their indices in the JSONQuery.GroupBys
	Return: bool, whether the treeList is ready or not (ready == true)
*/
func treeListReady(groupbydone map[int]bool, tree Tree, groupByMap map[int]int) bool {
	for _, treeElement := range tree.TreeElements {
		if treeElement.Self.FromNode.Name == "" { // We have a group by
			if !groupbydone[groupByMap[treeElement.Self.FromGroupBy.ID]] {
				return false
			}
		}
		if treeElement.Self.ToNode.Name == "" { // We have a group by
			if !groupbydone[groupByMap[treeElement.Self.ToGroupBy.ID]] {
				return false
			}
		}
	}
	return true
}

/*
tryGetGroupByFromRelation returns every group by that either has the relation ID, or group- or byIDs used in the relation
	JSONQuery: *IncomingQueryJSON, the converted query JSON
	relation: QueryRelationStruct, the relation to check for groupbys
	Return: (bool, []QueryGroupByStruct), whether the relation is used by groupbys, and the groupbys it is used by
*/
func tryGetGroupByFromRelation(JSONQuery *IncomingQueryJSON, relation QueryRelationStruct) (bool, []QueryGroupByStruct) {
	groupByFound := false
	var retval []QueryGroupByStruct
iteration:
	for _, groupBy := range JSONQuery.GroupBys {
		if groupBy.RelationID == relation.ID {
			groupByFound = true
			retval = append(retval, groupBy)
			continue iteration
		}
		if relation.FromType == groupBy.GroupType {
			for _, ID := range groupBy.GroupID {
				if relation.FromID == ID {
					groupByFound = true
					retval = append(retval, groupBy)
					continue iteration
				}
			}
		}
		if relation.ToType == groupBy.GroupType {
			for _, ID := range groupBy.GroupID {
				if relation.ToID == ID {
					groupByFound = true
					retval = append(retval, groupBy)
					continue iteration
				}
			}
		}
		if relation.FromType == groupBy.ByType {
			for _, ID := range groupBy.ByID {
				if relation.FromID == ID {
					groupByFound = true
					retval = append(retval, groupBy)
					continue iteration
				}
			}
		}
		if relation.ToType == groupBy.ByType {
			for _, ID := range groupBy.ByID {
				if relation.ToID == ID {
					groupByFound = true
					retval = append(retval, groupBy)
					continue iteration
				}
			}
		}
	}
	return groupByFound, retval
}

/*
createGroupByDoneMap generates an item in the group by done map with everything set to false (not done) for every group by
	JSONQuery: *IncomingQueryJSON, the converted query JSON
	Return: map[int]GroupByDone, a map from groupByID to GroupByDone with everything set to false
*/
func createGroupByDoneMap(JSONQuery *IncomingQueryJSON) map[int]GroupByDone {
	retval := make(map[int]GroupByDone)
	for _, groupBy := range JSONQuery.GroupBys {
		var groupByDone GroupByDone
		groupByDone.ByIDDone = make(map[int]bool)
		groupByDone.GroupIDDone = make(map[int]bool)
		groupByDone.RelationDone = false
		for _, ID := range groupBy.ByID {
			groupByDone.ByIDDone[ID] = false
		}
		for _, ID := range groupBy.GroupID {
			groupByDone.GroupIDDone[ID] = false
		}
		retval[groupBy.ID] = groupByDone
	}
	return retval
}

/*
groupByDone given a groupbydone, checks if everything is set to true (done)
	groupBy: GroupByDone, the groupByDone that needs to be checked for all done
	Return: bool, whether the group by is done
*/
func groupByDone(groupBy GroupByDone) bool {
	for _, done := range groupBy.ByIDDone {
		if !done {
			return false
		}
	}
	for _, done := range groupBy.GroupIDDone {
		if !done {
			return false
		}
	}
	return groupBy.RelationDone
}
