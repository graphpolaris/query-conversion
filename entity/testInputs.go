/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entity

var emptyQuery = []byte(`{
	"return": {
		"entities": [],
		"relations": []
	},
	"entities": [],
	"relations": [],
	"groupBys": [],
	"filters": [],
	"limit": 5000
}`)

var TwoEntitiesNoFilter = []byte(`{
	"databaseName": "TweedeKamer",
	"return": {
		"entities": [
			0,
			1
		],
		"relations": [
			0
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 1,
			"constraints": []
		}
	],
	"relations": [
		{
			"ID": 0,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 1,
			"constraints":[]
		}
	],
	"groupBys": [],		
	"limit": 5000,
	"modifiers": []
}`)

var TwoEntitiesOneEntityFilter = []byte(`{
	"databaseName": "TweedeKamer",
	"return": {
		"entities": [
			0,
			1
		],
		"relations": [
			0
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 1,
			"constraints": [
				{
					"attribute": "seats",
					"value": "10",
					"dataType": "int",
					"matchType": "LT"
				}
			]
		}
	],
	"relations": [
		{
			"ID": 0,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 1,
			"constraints":[]
		}
	],
	"groupBys": [],		
	"limit": 5000,
	"modifiers": []
}`)

var TwoEntitiesTwoEntityFilters = []byte(`{
	"databaseName": "TweedeKamer",
	"return": {
		"entities": [
			0,
			1
		],
		"relations": [
			0
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,
			"constraints": [
				{
					"attribute": "age",
					"value": "45",
					"dataType": "int",
					"matchType": "GT"
				}
			]
		},
		{
			"name": "parties",
			"ID": 1,
			"constraints": [
				{
					"attribute": "seats",
					"value": "10",
					"dataType": "int",
					"matchType": "LT"
				}
			]
		}
	],
	"relations": [
		{
			"ID": 0,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 1,
			"constraints":[]
		}
	],
	"groupBys": [],		
	"limit": 5000,
	"modifiers": []
}`)

var ThreeEntitiesNoFilter = []byte(`{
	"databaseName": "TweedeKamer",
	"return": {
		"entities": [
			0,
			1,
			2
		],
		"relations": [
			0,
			1
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 1,
			"constraints": []
		},
		{
			"name": "resolutions",
			"ID": 2,
			"constraints": []
		}
	],
	"relations": [
		{
			"ID": 0,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 1,
			"constraints":[]
		},
		{
			"ID": 1,
			"name": "submits",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 2,
			"constraints":[]
		}
	],
	"groupBys": [],		
	"limit": 5000,
	"modifiers": []
}`)

var ThreeEntitiesOneEntityFilter = []byte(`{
	"databaseName": "TweedeKamer",
	"return": {
		"entities": [
			0,
			1,
			2
		],
		"relations": [
			0,
			1
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,				
			"constraints": [
				{
					"attribute": "name",
					"value": "Geert",
					"dataType": "string",
					"matchType": "contains"
				}
			]
		},
		{
			"name": "parties",
			"ID": 1,
			"constraints": []
		},
		{
			"name": "resolutions",
			"ID": 2,
			"constraints": []
		}
	],
	"relations": [
		{
			"ID": 0,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 1,
			"constraints":[]
		},
		{
			"ID": 1,
			"name": "submits",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 2,
			"constraints":[]
		}
	],
	"groupBys": [],		
	"limit": 5000,
	"modifiers": []
}`)

var ThreeEntitiesTwoEntityFilters = []byte(`{
	"databaseName": "TweedeKamer",
	"return": {
		"entities": [
			0,
			1,
			2
		],
		"relations": [
			0,
			1
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,				
			"constraints": [
				{
					"attribute": "name",
					"value": "Geert",
					"dataType": "string",
					"matchType": "contains"
				}
			]
		},
		{
			"name": "parties",
			"ID": 1,
			"constraints": []
		},
		{
			"name": "resolutions",
			"ID": 2,
			"constraints": [
				{
					"attribute": "date",
					"value": "mei",
					"dataType": "string",
					"matchType": "contains"
				}
			]
		}
	],
	"relations": [
		{
			"ID": 0,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 1,
			"constraints":[]
		},
		{
			"ID": 1,
			"name": "submits",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 2,
			"constraints":[]
		}
	],
	"groupBys": [],		
	"limit": 5000,
	"modifiers": []
}`)

var FiveEntitiesFourEntityFilters = []byte(`{
	"databaseName": "TweedeKamer",
	"return": {
		"entities": [
			0,
			1,
			2,
			3,
			4
		],
		"relations": [
			0,
			1,
			2,
			3
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,
			"constraints": [
				{
					"attribute": "name",
					"value": "A",
					"dataType": "string",
					"matchType": "contains"
				}
			]
		},
		{
			"name": "parties",
			"ID": 1,
			"constraints": [
				{
					"attribute": "seats",
					"value": "10",
					"dataType": "int",
					"matchType": "LT"
				}
			]
		},
		{
			"name": "resolutions",
			"ID": 2,
			"constraints": [
				{
					"attribute": "date",
					"value": "mei",
					"dataType": "string",
					"matchType": "contains"
				}
			]
		},
		{
			"name": "parliament",
			"ID": 3,
			"constraints":[]
		},
		{
			"name": "parties",
			"ID": 4,
			"constraints": [
				{
					"attribute": "name",
					"value": "Volkspartij voor Vrijheid en Democratie",
					"dataType": "string",
					"matchType": "=="
				}
			]
		}
	],
	"relations": [
		{
			"ID": 0,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 1,
			"constraints": []
		},
		{
			"ID": 1,
			"name": "submits",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 2,
			"constraints": []
		},
		{
			"ID": 2,
			"name": "submits",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 3,
			"toType": "entity",
			"toID": 2,
			"constraints": []
		},
		{
			"ID": 3,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 3,
			"toType": "entity",
			"toID": 4,
			"constraints": []
		}
	],
	"groupBys": [],
	"limit": 5000,
	"modifiers": []
}`)

var SingleJunctionFiveEntitiesThreeEntityFilters = []byte(`{
	"return": {
		"entities": [
			0,
			1,
			2,
			3,
			4
		],
		"relations": [
			0,
			1,
			2,
			3
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,
			"constraints": [
				{
					"attribute": "name",
					"value": "Geert",
					"dataType": "string",
					"matchType": "contains"
				}
			]
		},
		{
			"name": "commissions",
			"ID": 1,
			"constraints": []
		},
		{
			"name": "parliament",
			"ID": 2,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 3,
			"constraints": [
				{
					"attribute": "seats",
					"value": "10",
					"dataType": "int",
					"matchType": "LT"
				}
			]
		},
		{
			"name": "resolutions",
			"ID": 4,
			"constraints": [
				{
					"attribute": "date",
					"value": "mei",
					"dataType": "string",
					"matchType": "contains"
				}
			]
		}
		
	],
	"groupBys": [],
	"relations": [
		{
			"ID": 0,
			"name": "part_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromId": 0,
			"toType": "entity",
			"toID": 1,
			"constraints": []
		},
		{
			"ID": 1,
			"name": "part_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromId": 2,
			"toType": "entity",
			"toID": 1,
			"constraints": []
		},
		{
			"ID": 2,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromId": 2,
			"toType": "entity",
			"toID": 3,
			"constraints": []
		},
		{
			"ID": 3,
			"name": "submits",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromId": 2,
			"toType": "entity",
			"toID": 4,
			"constraints": []
		}
	],
	"limit": 5000
}`)

var DoubleJunctionNineEntitiesThreeEntityFilters = []byte(`{
	"return": {
		"entities": [
			0,
			1,
			2,
			3,
			4,
			5,
			6,
			7
		],
		"relations": [
			0,
			1,
			2,
			3,
			4,
			5,
			6
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,
			"constraints": [
				{
					"attribute": "name",
					"value": "Geert",
					"dataType": "string",
					"matchType": "contains"
				}
			]
		},
		{
			"name": "commissions",
			"ID": 1,
			"constraints": []
		},
		{
			"name": "parliament",
			"ID": 2,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 3,
			"constraints": [
				{
					"attribute": "seats",
					"value": "10",
					"dataType": "int",
					"matchType": "LT"
				}
			]
		},
		{
			"name": "resolutions",
			"ID": 4,
			"constraints": [
				{
					"attribute": "date",
					"value": "mei",
					"dataType": "string",
					"matchType": "contains"
				}
			]
		},
		{
			"name": "resolutions",
			"ID": 5,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 6,
			"constraints": []
		}
		,
		{
			"name": "parliament",
			"ID": 7,
			"constraints": []
		}
		
	],
	"groupBys": [],
	"relations": [
		{
			"ID": 0,
			"name": "part_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromId": 0,
			"toType": "entity",
			"toID": 1,
			"constraints": []
		},
		{
			"ID": 1,
			"name": "part_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromId": 2,
			"toType": "entity",
			"toID": 1,
			"constraints": []
		},
		{
			"ID": 2,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromId": 2,
			"toType": "entity",
			"toID": 3,
			"constraints": []
		},
		{
			"ID": 3,
			"name": "submits",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromId": 2,
			"toType": "entity",
			"toID": 4,
			"constraints": []
		},
		{
			"ID": 4,
			"name": "submits",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromId": 0,
			"toType": "entity",
			"toID": 5,
			"constraints": []
		},
		{
			"ID": 5,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromId": 0,
			"toType": "entity",
			"toID": 6,
			"constraints": []
		}
		,
		{
			"ID": 6,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromId": 7,
			"toType": "entity",
			"toID": 6,
			"constraints": []
		}
	],
	"limit": 5000
}`)

var TwoEntitiesOneEntityFilterOneRelationFilter = []byte(`{
	"return": {
		"entities": [
			0,
			1
		],
		"relations": [
			0
		]
	},
	"entities": [
		{
			"ID": 0,
			"name": "airports",
			"constraints": [
				{
					"attribute": "state",
					"value": "HI",
					"dataType": "string",
					"matchType": "exact"
				}
			]
		},
		{
			"ID": 1,
			"name": "airports",
			"constraints":[]
		}
	],
	"relations": [
		{
			"ID": 0,
			"name": "flights",
			"depth": {
				"min": 1,
				"max": 1
			},
			"FromType": "entity",
			"fromID": 0,
			"ToType": "entity",
			"toID": 1,
			"constraints": [
				{	
					"attribute": "Day",
					"value": "15",
					"dataType": "int",
					"matchType": "EQ",
					"inType": "",
					"inID": -1
				}
			]
		}
	],
	"groupBys": [],
	"limit": 5000
}`)

var NoRelationsField = []byte(`{
	"databaseName": "TweedeKamer",
	"return": {
		"entities": [
			0,
			1
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 1,
			"constraints": []
		}
	],		
	"groupBys": [],		
	"limit": 5000,
	"modifiers": []
}`)

var SingleEndPointGroupBy = []byte(`{
	"return": {
		"entities": [
			2,
			3
		],
		"relations": [
			1
		],
		"groupBys": [
			6
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 2,
			"constraints": []
		},
		{
			"name": "commissions",
			"ID": 3,
			"constraints": []
		}
	],
	"relations": [
		{
			"ID": 1,
			"name": "part_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 2,
			"toType": "entity",
			"toID": 3,
			"constraints": []
		}
	],
	"groupBys": [
		{
			"ID": 6,
			"groupType": "entity",
			"groupID": [
				2
			],
			"groupAttribute": "age",
			"byType": "entity",
			"byID": [
				3
			],
			"byAttribute": "name",
			"appliedModifier": "AVG",
			"relationID": 1,
			"constraints": [
				{
					"attribute": "age",
					"value": "45",
					"dataType": "int",
					"matchType": "GT"
				}
			]
		}
	],
	"machineLearning": [],
	"limit": 5000
}`)

var SingleGroupBy = []byte(`{
	"return": {
		"entities": [
			2,
			3,
			4
		],
		"relations": [
			1,
			2
		],
		"groupBys": [
			6
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 2,
			"constraints": []
		},
		{
			"name": "commissions",
			"ID": 3,
			"constraints": []
		},
		{
			"name": "parliament",
			"ID": 4,
			"constraints": []
		}
	],
	"relations": [
		{
			"ID": 1,
			"name": "part_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 2,
			"toType": "entity",
			"toID": 3,
			"constraints": []
		},
		{
			"ID": 2,
			"name": "part_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 4,
			"toType": "groupBy",
			"toID": 6,
			"constraints": []
		}
	],
	"groupBys": [
		{
			"ID": 6,
			"groupType": "entity",
			"groupID": [
				2
			],
			"groupAttribute": "age",
			"byType": "entity",
			"byID": [
				3
			],
			"byAttribute": "name",
			"appliedModifier": "avg",
			"relationID": 1,
			"constraints": [
				{
					"attribute": "age",
					"value": "45",
					"dataType": "int",
					"matchType": "GT"
				}
			]
		}
	],
	"machineLearning": [],
	"limit": 5000
}
`)

var MultipleInputGroupByEndpoint = []byte(`{
	"return": {
	  "entities": [43, 44, 48, 49],
	  "relations": [42, 47],
	  "groupBys": [31]
	},
	"entities": [
	  {
		"name": "parliament",
		"ID": 43,
		"constraints": [
		  {
			"attribute": "age",
			"value": "42",
			"dataType": "int",
			"matchType": "LT"
		  }
		]
	  },
	  {
		"name": "parties",
		"ID": 44,
		"constraints": [
		  {
			"attribute": "seats",
			"value": "6",
			"dataType": "int",
			"matchType": "GT"
		  }
		]
	  },
	  {
		"name": "parliament",
		"ID": 48,
		"constraints": [
		  {
			"attribute": "age",
			"value": "42",
			"dataType": "int",
			"matchType": "GET"
		  }
		]
	  },
	  {
		"name": "parties",
		"ID": 49,
		"constraints": [
		  {
			"attribute": "seats",
			"value": "6",
			"dataType": "int",
			"matchType": "LET"
		  }
		]
	  }
	],
	"relations": [
	  {
		"ID": 42,
		"name": "member_of",
		"depth": { "min": 1, "max": 1 },
		"fromType": "entity",
		"fromID": 43,
		"toType": "entity",
		"toID": 44,
		"constraints": []
	  },
	  {
		"ID": 47,
		"name": "member_of",
		"depth": { "min": 1, "max": 1 },
		"fromType": "entity",
		"fromID": 48,
		"toType": "entity",
		"toID": 49,
		"constraints": []
	  }
	],
	"groupBys": [
	  {
		"ID": 31,
		"groupType": "entity",
		"groupID": [43, 48],
		"groupAttribute": "age",
		"byType": "entity",
		"byID": [49, 44],
		"byAttribute": "_id",
		"appliedModifier": "AVG",
		"relationID": 47,
		"constraints": [
		  {
			"attribute": "age",
			"value": "40",
			"dataType": "int",
			"matchType": "GET"
		  }
		]
	  }
	],
	"machineLearning": [],
	"limit": 5000
  }
`)

var MultipleInputGroupBy = []byte(`{
	"return": {
	  "entities": [43, 44, 48, 49, 73],
	  "relations": [42, 47, 71],
	  "groupBys": [31]
	},
	"entities": [
	  {
		"name": "parliament",
		"ID": 43,
		"constraints": [
		  {
			"attribute": "age",
			"value": "42",
			"dataType": "int",
			"matchType": "LT"
		  }
		]
	  },
	  {
		"name": "parties",
		"ID": 44,
		"constraints": [
		  {
			"attribute": "seats",
			"value": "6",
			"dataType": "int",
			"matchType": "GT"
		  }
		]
	  },
	  {
		"name": "parliament",
		"ID": 48,
		"constraints": [
		  {
			"attribute": "age",
			"value": "42",
			"dataType": "int",
			"matchType": "GET"
		  }
		]
	  },
	  {
		"name": "parties",
		"ID": 49,
		"constraints": [
		  {
			"attribute": "seats",
			"value": "6",
			"dataType": "int",
			"matchType": "LET"
		  }
		]
	  },
	  { "name": "parliament", "ID": 73, "constraints": [] }
	],
	"relations": [
	  {
		"ID": 42,
		"name": "member_of",
		"depth": { "min": 1, "max": 1 },
		"fromType": "entity",
		"fromID": 43,
		"toType": "entity",
		"toID": 44,
		"constraints": []
	  },
	  {
		"ID": 47,
		"name": "member_of",
		"depth": { "min": 1, "max": 1 },
		"fromType": "entity",
		"fromID": 48,
		"toType": "entity",
		"toID": 49,
		"constraints": []
	  },
	  {
		"ID": 71,
		"name": "member_of",
		"depth": { "min": 1, "max": 1 },
		"fromType": "entity",
		"fromID": 73,
		"toType": "groupBy",
		"toID": 31,
		"constraints": []
	  }
	],
	"groupBys": [
	  {
		"ID": 31,
		"groupType": "entity",
		"groupID": [43, 48],
		"groupAttribute": "age",
		"byType": "entity",
		"byID": [49, 44],
		"byAttribute": "_id",
		"appliedModifier": "AVG",
		"relationID": 47,
		"constraints": [
		  {
			"attribute": "age",
			"value": "40",
			"dataType": "int",
			"matchType": "GET"
		  }
		]
	  }
	],
	"machineLearning": [],
	"limit": 5000
  }
`)

var TopGroupBy = []byte(`{
	"return": {
	  "entities": [
		9,
		10,
		68,
		69
	  ],
	  "relations": [
		8,
		67,
		76
	  ],
	  "groupBys": [
		13,
		72
	  ]
	},
	"entities": [
	  {
		"name": "parliament",
		"ID": 9,
		"constraints": []
	  },
	  {
		"name": "parties",
		"ID": 10,
		"constraints": []
	  },
	  {
		"name": "parliament",
		"ID": 68,
		"constraints": []
	  },
	  {
		"name": "parties",
		"ID": 69,
		"constraints": []
	  }
	],
	"relations": [
	  {
		"ID": 8,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 9,
		"toType": "entity",
		"toID": 10,
		"constraints": []
	  },
	  {
		"ID": 67,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 68,
		"toType": "entity",
		"toID": 69,
		"constraints": []
	  },
	  {
		"ID": 76,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "groupBy",
		"fromID": 72,
		"toType": "groupBy",
		"toID": 13,
		"constraints": []
	  }
	],
	"groupBys": [
	  {
		"ID": 13,
		"groupType": "entity",
		"groupID": [
		  9
		],
		"groupAttribute": "age",
		"byType": "entity",
		"byID": [
		  10
		],
		"byAttribute": "_id",
		"appliedModifier": "AVG",
		"relationID": 8,
		"constraints": [
		  {
			"attribute": "age",
			"value": "42",
			"dataType": "int",
			"matchType": "GET"
		  }
		]
	  },
	  {
		"ID": 72,
		"groupType": "entity",
		"groupID": [
		  69
		],
		"groupAttribute": "seats",
		"byType": "entity",
		"byID": [
		  68
		],
		"byAttribute": "_id",
		"appliedModifier": "AVG",
		"relationID": 67,
		"constraints": [
		  {
			"attribute": "seats",
			"value": "13",
			"dataType": "int",
			"matchType": "LT"
		  }
		]
	  }
	],
	"machineLearning": [],
	"limit": 5000
  }
`)

var WayTooLargeQuery = []byte(`{
	"return": {
	  "entities": [
		9,
		10,
		68,
		69,
		93,
		104,
		119,
		126,
		145
	  ],
	  "relations": [
		8,
		67,
		76,
		92,
		98,
		118,
		124,
		144
	  ],
	  "groupBys": [
		13,
		72,
		106,
		138
	  ]
	},
	"entities": [
	  {
		"name": "parliament",
		"ID": 9,
		"constraints": []
	  },
	  {
		"name": "parties",
		"ID": 10,
		"constraints": []
	  },
	  {
		"name": "parliament",
		"ID": 68,
		"constraints": []
	  },
	  {
		"name": "parties",
		"ID": 69,
		"constraints": []
	  },
	  {
		"name": "parliament",
		"ID": 93,
		"constraints": [
		  {
			"attribute": "age",
			"value": "32",
			"dataType": "int",
			"matchType": "GET"
		  }
		]
	  },
	  {
		"name": "commissions",
		"ID": 104,
		"constraints": []
	  },
	  {
		"name": "parliament",
		"ID": 119,
		"constraints": [
		  {
			"attribute": "age",
			"value": "67",
			"dataType": "int",
			"matchType": "LET"
		  }
		]
	  },
	  {
		"name": "parties",
		"ID": 126,
		"constraints": [
		  {
			"attribute": "seats",
			"value": "14",
			"dataType": "int",
			"matchType": "LET"
		  }
		]
	  },
	  {
		"name": "parliament",
		"ID": 145,
		"constraints": []
	  }
	],
	"relations": [
	  {
		"ID": 8,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 9,
		"toType": "entity",
		"toID": 10,
		"constraints": []
	  },
	  {
		"ID": 67,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 68,
		"toType": "entity",
		"toID": 69,
		"constraints": []
	  },
	  {
		"ID": 76,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "groupBy",
		"fromID": 72,
		"toType": "groupBy",
		"toID": 13,
		"constraints": []
	  },
	  {
		"ID": 92,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 93,
		"toType": "groupBy",
		"toID": 13,
		"constraints": []
	  },
	  {
		"ID": 98,
		"name": "part_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 93,
		"toType": "entity",
		"toID": 104,
		"constraints": []
	  },
	  {
		"ID": 118,
		"name": "part_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 119,
		"toType": "groupBy",
		"toID": 106,
		"constraints": []
	  },
	  {
		"ID": 124,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 119,
		"toType": "entity",
		"toID": 126,
		"constraints": []
	  },
	  {
		"ID": 144,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 145,
		"toType": "groupBy",
		"toID": 138,
		"constraints": []
	  }
	],
	"groupBys": [
	  {
		"ID": 13,
		"groupType": "entity",
		"groupID": [
		  9
		],
		"groupAttribute": "age",
		"byType": "entity",
		"byID": [
		  10
		],
		"byAttribute": "_id",
		"appliedModifier": "AVG",
		"relationID": 8,
		"constraints": [
		  {
			"attribute": "age",
			"value": "42",
			"dataType": "int",
			"matchType": "GET"
		  }
		]
	  },
	  {
		"ID": 72,
		"groupType": "entity",
		"groupID": [
		  69
		],
		"groupAttribute": "seats",
		"byType": "entity",
		"byID": [
		  68
		],
		"byAttribute": "_id",
		"appliedModifier": "AVG",
		"relationID": 67,
		"constraints": [
		  {
			"attribute": "seats",
			"value": "13",
			"dataType": "int",
			"matchType": "LT"
		  }
		]
	  },
	  {
		"ID": 106,
		"groupType": "entity",
		"groupID": [
		  93
		],
		"groupAttribute": "age",
		"byType": "entity",
		"byID": [
		  104
		],
		"byAttribute": "_id",
		"appliedModifier": "AVG",
		"relationID": 98,
		"constraints": [
		  {
			"attribute": "age",
			"value": "69",
			"dataType": "int",
			"matchType": "LET"
		  }
		]
	  },
	  {
		"ID": 138,
		"groupType": "entity",
		"groupID": [
		  119
		],
		"groupAttribute": "age",
		"byType": "entity",
		"byID": [
		  126
		],
		"byAttribute": "_id",
		"appliedModifier": "PRODUCT",
		"relationID": 124,
		"constraints": [
		  {
			"attribute": "age",
			"value": "8008",
			"dataType": "int",
			"matchType": "GET"
		  }
		]
	  }
	],
	"machineLearning": [],
	"limit": 5000
  }
`)

var IncorrectRelationFrom = []byte(`{
	"databaseName": "TweedeKamer",
	"return": {
		"entities": [
			0,
			1
		],
		"relations": [
			0
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 1,
			"constraints": []
		}
	],
	"relations": [
		{
			"ID": 0,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": -4,
			"constraints":[]
		}
	],
	"groupBys": [],		
	"limit": 5000,
	"modifiers": []
}`)

var SeparatedChainSingleRelationPerChain = []byte(`{
	"databaseName": "TweedeKamer",
	"return": {
		"entities": [
			0,
			1,
			2,
			3
		],
		"relations": [
			0
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 1,
			"constraints": []
		},
		{
			"name": "parliament",
			"ID": 2,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 3,
			"constraints": []
		}
	],
	"relations": [
		{
			"ID": 0,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 1,
			"constraints":[]
		},
		{
			"ID": 1,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 2,
			"toType": "entity",
			"toID": 3,
			"constraints":[]
		}
	],
	"groupBys": [],		
	"limit": 5000,
	"modifiers": []
}`)

var SeparatedChainDoubleRelationPerChain = []byte(`{
	"databaseName": "TweedeKamer",
	"return": {
		"entities": [
			0,
			1,
			2,
			3,
			4,
			5
		],
		"relations": [
			0
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 1,
			"constraints": []
		},
		{
			"name": "parliament",
			"ID": 2,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 3,
			"constraints": []
		},
		{
			"name": "resolutions",
			"ID": 4,
			"constraints": []
		},
		{
			"name": "resolutions",
			"ID": 5,
			"constraints": []
		}
	],
	"relations": [
		{
			"ID": 0,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 1,
			"constraints":[]
		},
		{
			"ID": 1,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 2,
			"toType": "entity",
			"toID": 3,
			"constraints":[]
		},
		{
			"ID": 2,
			"name": "submits",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 4,
			"constraints":[]
		},
		{
			"ID": 3,
			"name": "submits",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 2,
			"toType": "entity",
			"toID": 5,
			"constraints":[]
		}
	],
	"groupBys": [],		
	"limit": 5000,
	"modifiers": []
}`)

var SeperatedChainWithGroupBys = []byte(`{
	"return": {
	  "entities": [
		2,
		3,
		14,
		28,
		37,
		38,
		47
	  ],
	  "relations": [
		1,
		13,
		26,
		36,
		46
	  ],
	  "groupBys": [
		12,
		35
	  ]
	},
	"entities": [
	  {
		"name": "parliament",
		"ID": 2,
		"constraints": []
	  },
	  {
		"name": "parties",
		"ID": 3,
		"constraints": []
	  },
	  {
		"name": "parliament",
		"ID": 14,
		"constraints": []
	  },
	  {
		"name": "commissions",
		"ID": 28,
		"constraints": [
		  {
			"attribute": "name",
			"value": "f",
			"dataType": "string",
			"matchType": "contains"
		  }
		]
	  },
	  {
		"name": "parliament",
		"ID": 37,
		"constraints": []
	  },
	  {
		"name": "resolutions",
		"ID": 38,
		"constraints": []
	  },
	  {
		"name": "parliament",
		"ID": 47,
		"constraints": []
	  }
	],
	"relations": [
	  {
		"ID": 1,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 2,
		"toType": "entity",
		"toID": 3,
		"constraints": []
	  },
	  {
		"ID": 13,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 14,
		"toType": "groupBy",
		"toID": 12,
		"constraints": []
	  },
	  {
		"ID": 26,
		"name": "part_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 14,
		"toType": "entity",
		"toID": 28,
		"constraints": []
	  },
	  {
		"ID": 36,
		"name": "submits",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 37,
		"toType": "entity",
		"toID": 38,
		"constraints": []
	  },
	  {
		"ID": 46,
		"name": "submits",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 47,
		"toType": "groupBy",
		"toID": 35,
		"constraints": []
	  }
	],
	"groupBys": [
	  {
		"ID": 12,
		"groupType": "entity",
		"groupID": [
		  2
		],
		"groupAttribute": "age",
		"byType": "entity",
		"byID": [
		  3
		],
		"byAttribute": "_id",
		"appliedModifier": "AVG",
		"relationID": 1,
		"constraints": [
		  {
			"attribute": "age",
			"value": "42",
			"dataType": "int",
			"matchType": "GET"
		  }
		]
	  },
	  {
		"ID": 35,
		"groupType": "entity",
		"groupID": [
		  37
		],
		"groupAttribute": "age",
		"byType": "entity",
		"byID": [
		  38
		],
		"byAttribute": "_id",
		"appliedModifier": "AVG",
		"relationID": 36,
		"constraints": [
		  {
			"attribute": "age",
			"value": "45",
			"dataType": "int",
			"matchType": "GET"
		  }
		]
	  }
	],
	"machineLearning": [],
	"limit": 5000
  }`)

var HierarchyBasic = []byte(`{
	"return": {
		"entities": [
			0,
			1,
			2,
			3,
			4
		],
		"relations": [
			0,
			1,
			2,
			3
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,
			"constraints": [
				{
					"attribute": "name",
					"value": "Geert",
					"dataType": "string",
					"matchType": "CONTAINS"
				}
			]
		},
		{
			"name": "commissions",
			"ID": 1,
			"constraints": []
		},
		{
			"name": "parliament",
			"ID": 2,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 3,
			"constraints": [
				{
					"attribute": "seats",
					"value": "10",
					"dataType": "int",
					"matchType": "LT"
				}
			]
		},
		{
			"name": "resolutions",
			"ID": 4,
			"constraints": [
				{
					"attribute": "date",
					"value": "mei",
					"dataType": "string",
					"matchType": "CONTAINS"
				}
			]
		}
	],
	"groupBys": [],
	"relations": [
		{
			"ID": 0,
			"name": "part_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromId": 0,
			"toType": "entity",
			"toID": 1,
			"constraints": []
		},
		{
			"ID": 1,
			"name": "part_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromId": 2,
			"toType": "entity",
			"toID": 1,
			"constraints": []
		},
		{
			"ID": 2,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromId": 2,
			"toType": "entity",
			"toID": 3,
			"constraints": []
		},
		{
			"ID": 3,
			"name": "submits",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromId": 2,
			"toType": "entity",
			"toID": 4,
			"constraints": []
		}
	],
	"limit": 5000
}
`)

var AllStreamers = []byte(`{
	"return": {
	  "entities": [
		2,
		3
	  ],
	  "relations": [
		1,
		14
	  ],
	  "groupBys": [
		6,
		7,
		21
	  ]
	},
	"entities": [
	  {
		"name": "Streamer",
		"ID": 2,
		"constraints": []
	  },
	  {
		"name": "Streamer",
		"ID": 3,
		"constraints": []
	  }
	],
	"relations": [
	  {
		"ID": 1,
		"name": "viewerOverlap",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 2,
		"toType": "entity",
		"toID": 3,
		"constraints": []
	  },
	  {
		"ID": 14,
		"name": "viewerOverlap",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "groupBy",
		"fromID": 6,
		"toType": "groupBy",
		"toID": 7,
		"constraints": []
	  }
	],
	"groupBys": [
	  {
		"ID": 6,
		"groupType": "entity",
		"groupID": [
		  2
		],
		"groupAttribute": "Count",
		"byType": "entity",
		"byID": [
		  3
		],
		"byAttribute": "_id",
		"appliedModifier": "AVG",
		"relationID": 1,
		"constraints": []
	  },
	  {
		"ID": 7,
		"groupType": "entity",
		"groupID": [
		  2
		],
		"groupAttribute": "Count",
		"byType": "entity",
		"byID": [
		  3
		],
		"byAttribute": "_id",
		"appliedModifier": "AVG",
		"relationID": 1,
		"constraints": []
	  },
	  {
		"ID": 21,
		"groupType": "groupBy",
		"groupID": [
		  6
		],
		"groupAttribute": "Count",
		"byType": "groupBy",
		"byID": [
		  7
		],
		"byAttribute": "_id",
		"appliedModifier": "AVG",
		"relationID": 14,
		"constraints": []
	  }
	],
	"machineLearning": [],
	"limit": 5000
  }
`)

var StringMatchTypes = []byte(`{
	"return": {
	  "entities": [
		2,
		3,
		8
	  ],
	  "relations": [
		1,
		6
	  ],
	  "groupBys": []
	},
	"entities": [
	  {
		"name": "parliament",
		"ID": 2,
		"constraints": [
		  {
			"attribute": "name",
			"value": "Geert Wilders",
			"dataType": "string",
			"matchType": "NEQ"
		  }
		]
	  },
	  {
		"name": "parties",
		"ID": 3,
		"constraints": [
		  {
			"attribute": "name",
			"value": "v",
			"dataType": "string",
			"matchType": "excludes"
		  }
		]
	  },
	  {
		"name": "commissions",
		"ID": 8,
		"constraints": [
		  {
			"attribute": "name",
			"value": "groep",
			"dataType": "string",
			"matchType": "excludes"
		  }
		]
	  }
	],
	"relations": [
	  {
		"ID": 1,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 2,
		"toType": "entity",
		"toID": 3,
		"constraints": []
	  },
	  {
		"ID": 6,
		"name": "part_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 2,
		"toType": "entity",
		"toID": 8,
		"constraints": []
	  }
	],
	"groupBys": [],
	"machineLearning": [],
	"limit": 5000
  }`)

var TopEntityToFromGroupBy = []byte(`{
	"return": {
	  "entities": [
		26,
		27,
		36
	  ],
	  "relations": [
		25,
		34
	  ],
	  "groupBys": [
		30
	  ]
	},
	"entities": [
	  {
		"name": "Streamer",
		"ID": 26,
		"constraints": []
	  },
	  {
		"name": "Streamer",
		"ID": 27,
		"constraints": []
	  },
	  {
		"name": "Streamer",
		"ID": 36,
		"constraints": []
	  }
	],
	"relations": [
	  {
		"ID": 25,
		"name": "viewerOverlap",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 26,
		"toType": "entity",
		"toID": 27,
		"constraints": []
	  },
	  {
		"ID": 34,
		"name": "viewerOverlap",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "groupBy",
		"fromID": 30,
		"toType": "entity",
		"toID": 36,
		"constraints": []
	  }
	],
	"groupBys": [
	  {
		"ID": 30,
		"groupType": "entity",
		"groupID": [
		  27
		],
		"groupAttribute": "Count",
		"byType": "entity",
		"byID": [
		  26
		],
		"byAttribute": "_id",
		"appliedModifier": "avg",
		"relationID": 25,
		"constraints": []
	  }
	],
	"machineLearning": [],
	"limit": 5000
  }`)

var IntNotEquals = []byte(`{
	"return": {
	  "entities": [
		2,
		3
	  ],
	  "relations": [
		1
	  ],
	  "groupBys": []
	},
	"entities": [
	  {
		"name": "parliament",
		"ID": 2,
		"constraints": [
			{
				"attribute": "seniority",
				"value": "57",
				"dataType": "int",
				"matchType": "NEQ"
			}
		]
	  },
	  {
		"name": "parties",
		"ID": 3,
		"constraints": []
	  }
	],
	"relations": [
	  {
		"ID": 1,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 2,
		"toType": "entity",
		"toID": 3,
		"constraints": []
	  }
	],
	"groupBys": [],
	"machineLearning": [],
	"limit": 5000
  }`)

var BoolEqual = []byte(`{
	"return": {
	  "entities": [
		8,
		9
	  ],
	  "relations": [
		7
	  ],
	  "groupBys": []
	},
	"entities": [
	  {
		"name": "parliament",
		"ID": 8,
		"constraints": []
	  },
	  {
		"name": "parties",
		"ID": 9,
		"constraints": []
	  }
	],
	"relations": [
	  {
		"ID": 7,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 8,
		"toType": "entity",
		"toID": 9,
		"constraints": [
		  {
			"attribute": "isChairman",
			"value": "true",
			"dataType": "bool",
			"matchType": "EQ"
		  }
		]
	  }
	],
	"groupBys": [],
	"machineLearning": [],
	"limit": 5000
  }`)

var BoolNotEqual = []byte(`{
	"return": {
	  "entities": [
		8,
		9
	  ],
	  "relations": [
		7
	  ],
	  "groupBys": []
	},
	"entities": [
	  {
		"name": "parliament",
		"ID": 8,
		"constraints": []
	  },
	  {
		"name": "parties",
		"ID": 9,
		"constraints": []
	  }
	],
	"relations": [
	  {
		"ID": 7,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 8,
		"toType": "entity",
		"toID": 9,
		"constraints": [
		  {
			"attribute": "isChairman",
			"value": "true",
			"dataType": "bool",
			"matchType": "NEQ"
		  }
		]
	  }
	],
	"groupBys": [],
	"machineLearning": [],
	"limit": 5000
  }`)

var NestedGroupBys2 = []byte(`{
	"return": {
	  "entities": [
		9,
		10,
		25,
		52
	  ],
	  "relations": [
		8,
		24,
		51
	  ],
	  "groupBys": [
		6,
		40
	  ]
	},
	"entities": [
	  {
		"name": "parliament",
		"ID": 9,
		"constraints": []
	  },
	  {
		"name": "commissions",
		"ID": 10,
		"constraints": []
	  },
	  {
		"name": "parliament",
		"ID": 25,
		"constraints": []
	  },
	  {
		"name": "parliament",
		"ID": 52,
		"constraints": []
	  }
	],
	"relations": [
	  {
		"ID": 8,
		"name": "part_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 9,
		"toType": "entity",
		"toID": 10,
		"constraints": []
	  },
	  {
		"ID": 24,
		"name": "part_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 25,
		"toType": "groupBy",
		"toID": 6,
		"constraints": []
	  },
	  {
		"ID": 51,
		"name": "part_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 52,
		"toType": "groupBy",
		"toID": 40,
		"constraints": []
	  }
	],
	"groupBys": [
	  {
		"ID": 6,
		"groupType": "entity",
		"groupID": [
		  9
		],
		"groupAttribute": "age",
		"byType": "entity",
		"byID": [
		  10
		],
		"byAttribute": "_id",
		"appliedModifier": "AVG",
		"relationID": 8,
		"constraints": [
		  {
			"attribute": "age",
			"value": "41",
			"dataType": "int",
			"matchType": "LET"
		  }
		]
	  },
	  {
		"ID": 40,
		"groupType": "entity",
		"groupID": [
		  25
		],
		"groupAttribute": "age",
		"byType": "groupBy",
		"byID": [
		  6
		],
		"byAttribute": "_id",
		"appliedModifier": "MAX",
		"relationID": 24,
		"constraints": []
	  }
	],
	"machineLearning": [],
	"limit": 5000
  }`)

var DifferentNodesOnGroupBy = []byte(`{
	"return": {
	  "entities": [
		2,
		3,
		11,
		12
	  ],
	  "relations": [
		1,
		10
	  ],
	  "groupBys": [
		6
	  ]
	},
	"entities": [
	  {
		"name": "parliament",
		"ID": 2,
		"constraints": []
	  },
	  {
		"name": "commissions",
		"ID": 3,
		"constraints": []
	  },
	  {
		"name": "parliament",
		"ID": 11,
		"constraints": []
	  },
	  {
		"name": "parties",
		"ID": 12,
		"constraints": []
	  }
	],
	"relations": [
	  {
		"ID": 1,
		"name": "part_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 2,
		"toType": "entity",
		"toID": 3,
		"constraints": []
	  },
	  {
		"ID": 10,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 11,
		"toType": "entity",
		"toID": 12,
		"constraints": []
	  }
	],
	"groupBys": [
	  {
		"ID": 6,
		"groupType": "entity",
		"groupID": [
		  3,
		  11
		],
		"groupAttribute": "age",
		"byType": "entity",
		"byID": [
		  3,
		  12
		],
		"byAttribute": "_id",
		"appliedModifier": "avg",
		"relationID": 1,
		"constraints": []
	  }
	],
	"machineLearning": [],
	"limit": 5000
  }`)

var IncorrectGroupByModifier = []byte(`{
	"return": {
	  "entities": [
		2,
		3
	  ],
	  "relations": [
		1
	  ],
	  "groupBys": [
		6
	  ]
	},
	"entities": [
	  {
		"name": "parliament",
		"ID": 2,
		"constraints": []
	  },
	  {
		"name": "commissions",
		"ID": 3,
		"constraints": []
	  }
	],
	"relations": [
	  {
		"ID": 1,
		"name": "part_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 2,
		"toType": "entity",
		"toID": 3,
		"constraints": []
	  }
	],
	"groupBys": [
	  {
		"ID": 6,
		"groupType": "entity",
		"groupID": [
		  2
		],
		"groupAttribute": "age",
		"byType": "entity",
		"byID": [
		  3
		],
		"byAttribute": "_id",
		"appliedModifier": "wibblywobbly",
		"relationID": 1,
		"constraints": []
	  }
	],
	"machineLearning": [],
	"limit": 5000
  }`)
