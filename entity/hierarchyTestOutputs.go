/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entity

var HierarchyBasicTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "name",
                                    "value": "Geert",
                                    "dataType": "string",
                                    "matchType": "CONTAINS",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 0,
                            "name": "part_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 1,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 1,
                            "name": "commissions",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": [
                        1
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 2,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 1,
                            "name": "part_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 2,
                            "toType": "entity",
                            "toID": 1,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 1,
                            "name": "commissions",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 0,
                    "Children": [
                        2,
                        3
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 2,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 2,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 2,
                            "toType": "entity",
                            "toID": 3,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 3,
                            "name": "parties",
                            "constraints": [
                                {
                                    "attribute": "seats",
                                    "value": "10",
                                    "dataType": "int",
                                    "matchType": "LT",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 1,
                    "Children": []
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 2,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 3,
                            "name": "submits",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 2,
                            "toType": "entity",
                            "toID": 4,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 4,
                            "name": "resolutions",
                            "constraints": [
                                {
                                    "attribute": "date",
                                    "value": "mei",
                                    "dataType": "string",
                                    "matchType": "CONTAINS",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 0,
                "name": "parliament",
                "constraints": [
                    {
                        "attribute": "name",
                        "value": "Geert",
                        "dataType": "string",
                        "matchType": "CONTAINS",
                        "inID": 0,
                        "inType": ""
                    }
                ]
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": null
        }
    ]
}`)

var TwoEntitiesNoFilterTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 0,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 1,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 1,
                            "name": "parties",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 0,
                "name": "parliament",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": null
        }
    ]
}
`)

var TwoEntitiesOneEntityFilterTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 0,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 1,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 1,
                            "name": "parties",
                            "constraints": [
                                {
                                    "attribute": "seats",
                                    "value": "10",
                                    "dataType": "int",
                                    "matchType": "LT",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 0,
                "name": "parliament",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": null
        }
    ]
}
`)

var TwoEntitiesTwoEntityFiltersTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "45",
                                    "dataType": "int",
                                    "matchType": "GT",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 0,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 1,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 1,
                            "name": "parties",
                            "constraints": [
                                {
                                    "attribute": "seats",
                                    "value": "10",
                                    "dataType": "int",
                                    "matchType": "LT",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 0,
                "name": "parliament",
                "constraints": [
                    {
                        "attribute": "age",
                        "value": "45",
                        "dataType": "int",
                        "matchType": "GT",
                        "inID": 0,
                        "inType": ""
                    }
                ]
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": null
        }
    ]
}`)

var ThreeEntitiesNoFilterTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 0,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 1,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 1,
                            "name": "parties",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": [
                        1
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 1,
                            "name": "submits",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 2,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 2,
                            "name": "resolutions",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 0,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 1,
                "name": "parties",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": null
        }
    ]
}
`)

var ThreeEntitiesOneEntityFilterTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "name",
                                    "value": "Geert",
                                    "dataType": "string",
                                    "matchType": "contains",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 0,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 1,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 1,
                            "name": "parties",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": [
                        1
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "name",
                                    "value": "Geert",
                                    "dataType": "string",
                                    "matchType": "contains",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 1,
                            "name": "submits",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 2,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 2,
                            "name": "resolutions",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 0,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 1,
                "name": "parties",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": null
        }
    ]
}
`)

var ThreeEntitiesTwoEntityFiltersTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "name",
                                    "value": "Geert",
                                    "dataType": "string",
                                    "matchType": "contains",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 0,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 1,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 1,
                            "name": "parties",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": [
                        1
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "name",
                                    "value": "Geert",
                                    "dataType": "string",
                                    "matchType": "contains",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 1,
                            "name": "submits",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 2,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 2,
                            "name": "resolutions",
                            "constraints": [
                                {
                                    "attribute": "date",
                                    "value": "mei",
                                    "dataType": "string",
                                    "matchType": "contains",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 0,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 1,
                "name": "parties",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": null
        }
    ]
}
`)

var FiveEntitiesFourEntityFiltersTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "name",
                                    "value": "A",
                                    "dataType": "string",
                                    "matchType": "contains",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 0,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 1,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 1,
                            "name": "parties",
                            "constraints": [
                                {
                                    "attribute": "seats",
                                    "value": "10",
                                    "dataType": "int",
                                    "matchType": "LT",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": [
                        1
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "name",
                                    "value": "A",
                                    "dataType": "string",
                                    "matchType": "contains",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 1,
                            "name": "submits",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 2,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 2,
                            "name": "resolutions",
                            "constraints": [
                                {
                                    "attribute": "date",
                                    "value": "mei",
                                    "dataType": "string",
                                    "matchType": "contains",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 0,
                    "Children": [
                        2
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 3,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 2,
                            "name": "submits",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 3,
                            "toType": "entity",
                            "toID": 2,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 2,
                            "name": "resolutions",
                            "constraints": [
                                {
                                    "attribute": "date",
                                    "value": "mei",
                                    "dataType": "string",
                                    "matchType": "contains",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 1,
                    "Children": [
                        3
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 3,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 3,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 3,
                            "toType": "entity",
                            "toID": 4,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 4,
                            "name": "parties",
                            "constraints": [
                                {
                                    "attribute": "name",
                                    "value": "Volkspartij voor Vrijheid en Democratie",
                                    "dataType": "string",
                                    "matchType": "==",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 2,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 1,
                "name": "parties",
                "constraints": [
                    {
                        "attribute": "seats",
                        "value": "10",
                        "dataType": "int",
                        "matchType": "LT",
                        "inID": 0,
                        "inType": ""
                    }
                ]
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": null
        }
    ]
}
`)

var SingleJunctionFiveEntitiesThreeEntityFiltersTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "name",
                                    "value": "Geert",
                                    "dataType": "string",
                                    "matchType": "contains",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 0,
                            "name": "part_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 1,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 1,
                            "name": "commissions",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": [
                        1
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 2,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 1,
                            "name": "part_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 2,
                            "toType": "entity",
                            "toID": 1,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 1,
                            "name": "commissions",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 0,
                    "Children": [
                        2,
                        3
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 2,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 2,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 2,
                            "toType": "entity",
                            "toID": 3,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 3,
                            "name": "parties",
                            "constraints": [
                                {
                                    "attribute": "seats",
                                    "value": "10",
                                    "dataType": "int",
                                    "matchType": "LT",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 1,
                    "Children": []
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 2,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 3,
                            "name": "submits",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 2,
                            "toType": "entity",
                            "toID": 4,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 4,
                            "name": "resolutions",
                            "constraints": [
                                {
                                    "attribute": "date",
                                    "value": "mei",
                                    "dataType": "string",
                                    "matchType": "contains",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 0,
                "name": "parliament",
                "constraints": [
                    {
                        "attribute": "name",
                        "value": "Geert",
                        "dataType": "string",
                        "matchType": "contains",
                        "inID": 0,
                        "inType": ""
                    }
                ]
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": null
        }
    ]
}
`)

var DoubleJunctionNineEntitiesThreeEntityFiltersTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 2,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 2,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 2,
                            "toType": "entity",
                            "toID": 3,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 3,
                            "name": "parties",
                            "constraints": [
                                {
                                    "attribute": "seats",
                                    "value": "10",
                                    "dataType": "int",
                                    "matchType": "LT",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": [
                        1,
                        6
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 2,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 1,
                            "name": "part_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 2,
                            "toType": "entity",
                            "toID": 1,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 1,
                            "name": "commissions",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 0,
                    "Children": [
                        2
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "name",
                                    "value": "Geert",
                                    "dataType": "string",
                                    "matchType": "contains",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 0,
                            "name": "part_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 1,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 1,
                            "name": "commissions",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 1,
                    "Children": [
                        3,
                        4
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "name",
                                    "value": "Geert",
                                    "dataType": "string",
                                    "matchType": "contains",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 4,
                            "name": "submits",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 5,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 5,
                            "name": "resolutions",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 2,
                    "Children": []
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "name",
                                    "value": "Geert",
                                    "dataType": "string",
                                    "matchType": "contains",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 5,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 6,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 6,
                            "name": "parties",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 2,
                    "Children": [
                        5
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 7,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 6,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 7,
                            "toType": "entity",
                            "toID": 6,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 6,
                            "name": "parties",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 4,
                    "Children": []
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 2,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 3,
                            "name": "submits",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 2,
                            "toType": "entity",
                            "toID": 4,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 4,
                            "name": "resolutions",
                            "constraints": [
                                {
                                    "attribute": "date",
                                    "value": "mei",
                                    "dataType": "string",
                                    "matchType": "contains",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 0,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 3,
                "name": "parties",
                "constraints": [
                    {
                        "attribute": "seats",
                        "value": "10",
                        "dataType": "int",
                        "matchType": "LT",
                        "inID": 0,
                        "inType": ""
                    }
                ]
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": null
        }
    ]
}
`)

var TwoEntitiesOneEntityFilterOneRelationFilterTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "airports",
                            "constraints": [
                                {
                                    "attribute": "state",
                                    "value": "HI",
                                    "dataType": "string",
                                    "matchType": "exact",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 0,
                            "name": "flights",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 0,
                            "toType": "entity",
                            "toID": 1,
                            "constraints": [
                                {
                                    "attribute": "Day",
                                    "value": "15",
                                    "dataType": "int",
                                    "matchType": "EQ",
                                    "inID": -1,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToNode": {
                            "id": 1,
                            "name": "airports",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 0,
                "name": "airports",
                "constraints": [
                    {
                        "attribute": "state",
                        "value": "HI",
                        "dataType": "string",
                        "matchType": "exact",
                        "inID": 0,
                        "inType": ""
                    }
                ]
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": null
        }
    ]
}
`)

var SingleEndPointGroupByTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 2,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 1,
                            "name": "part_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 2,
                            "toType": "entity",
                            "toID": 3,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 3,
                            "name": "commissions",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 2,
                "name": "parliament",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": [
                {
                    "id": 6,
                    "groupType": "entity",
                    "groupID": [
                        2
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        3
                    ],
                    "byAttribute": "name",
                    "appliedModifier": "AVG",
                    "relationID": 1,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "45",
                            "dataType": "int",
                            "matchType": "GT",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "AllGroupBys": [
                {
                    "id": 6,
                    "groupType": "entity",
                    "groupID": [
                        2
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        3
                    ],
                    "byAttribute": "name",
                    "appliedModifier": "AVG",
                    "relationID": 1,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "45",
                            "dataType": "int",
                            "matchType": "GT",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "Parents": null
        }
    ]
}
`)

var SingleGroupByTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 2,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 1,
                            "name": "part_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 2,
                            "toType": "entity",
                            "toID": 3,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 3,
                            "name": "commissions",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 2,
                "name": "parliament",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": [
                {
                    "id": 6,
                    "groupType": "entity",
                    "groupID": [
                        2
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        3
                    ],
                    "byAttribute": "name",
                    "appliedModifier": "avg",
                    "relationID": 1,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "45",
                            "dataType": "int",
                            "matchType": "GT",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "AllGroupBys": [
                {
                    "id": 6,
                    "groupType": "entity",
                    "groupID": [
                        2
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        3
                    ],
                    "byAttribute": "name",
                    "appliedModifier": "avg",
                    "relationID": 1,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "45",
                            "dataType": "int",
                            "matchType": "GT",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "Parents": null
        },
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 4,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 2,
                            "name": "part_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 4,
                            "toType": "groupBy",
                            "toID": 6,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 0,
                            "name": "",
                            "constraints": null
                        },
                        "ToGroupBy": {
                            "id": 6,
                            "groupType": "entity",
                            "groupID": [
                                2
                            ],
                            "groupAttribute": "age",
                            "byType": "entity",
                            "byID": [
                                3
                            ],
                            "byAttribute": "name",
                            "appliedModifier": "avg",
                            "relationID": 1,
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "45",
                                    "dataType": "int",
                                    "matchType": "GT",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 4,
                "name": "parliament",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": [
                0
            ]
        }
    ]
}
`)

var MultipleInputGroupByEndpointTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 43,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "42",
                                    "dataType": "int",
                                    "matchType": "LT",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 42,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 43,
                            "toType": "entity",
                            "toID": 44,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 44,
                            "name": "parties",
                            "constraints": [
                                {
                                    "attribute": "seats",
                                    "value": "6",
                                    "dataType": "int",
                                    "matchType": "GT",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 43,
                "name": "parliament",
                "constraints": [
                    {
                        "attribute": "age",
                        "value": "42",
                        "dataType": "int",
                        "matchType": "LT",
                        "inID": 0,
                        "inType": ""
                    }
                ]
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": [
                {
                    "id": 31,
                    "groupType": "entity",
                    "groupID": [
                        43,
                        48
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        49,
                        44
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 47,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "40",
                            "dataType": "int",
                            "matchType": "GET",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "Parents": null
        },
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 48,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "42",
                                    "dataType": "int",
                                    "matchType": "GET",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 47,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 48,
                            "toType": "entity",
                            "toID": 49,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 49,
                            "name": "parties",
                            "constraints": [
                                {
                                    "attribute": "seats",
                                    "value": "6",
                                    "dataType": "int",
                                    "matchType": "LET",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 48,
                "name": "parliament",
                "constraints": [
                    {
                        "attribute": "age",
                        "value": "42",
                        "dataType": "int",
                        "matchType": "GET",
                        "inID": 0,
                        "inType": ""
                    }
                ]
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": [
                {
                    "id": 31,
                    "groupType": "entity",
                    "groupID": [
                        43,
                        48
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        49,
                        44
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 47,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "40",
                            "dataType": "int",
                            "matchType": "GET",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "AllGroupBys": [
                {
                    "id": 31,
                    "groupType": "entity",
                    "groupID": [
                        43,
                        48
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        49,
                        44
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 47,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "40",
                            "dataType": "int",
                            "matchType": "GET",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "Parents": null
        }
    ]
}
`)

var MultipleInputGroupByTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 43,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "42",
                                    "dataType": "int",
                                    "matchType": "LT",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 42,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 43,
                            "toType": "entity",
                            "toID": 44,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 44,
                            "name": "parties",
                            "constraints": [
                                {
                                    "attribute": "seats",
                                    "value": "6",
                                    "dataType": "int",
                                    "matchType": "GT",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 43,
                "name": "parliament",
                "constraints": [
                    {
                        "attribute": "age",
                        "value": "42",
                        "dataType": "int",
                        "matchType": "LT",
                        "inID": 0,
                        "inType": ""
                    }
                ]
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": [
                {
                    "id": 31,
                    "groupType": "entity",
                    "groupID": [
                        43,
                        48
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        49,
                        44
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 47,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "40",
                            "dataType": "int",
                            "matchType": "GET",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "Parents": null
        },
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 48,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "42",
                                    "dataType": "int",
                                    "matchType": "GET",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 47,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 48,
                            "toType": "entity",
                            "toID": 49,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 49,
                            "name": "parties",
                            "constraints": [
                                {
                                    "attribute": "seats",
                                    "value": "6",
                                    "dataType": "int",
                                    "matchType": "LET",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 48,
                "name": "parliament",
                "constraints": [
                    {
                        "attribute": "age",
                        "value": "42",
                        "dataType": "int",
                        "matchType": "GET",
                        "inID": 0,
                        "inType": ""
                    }
                ]
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": [
                {
                    "id": 31,
                    "groupType": "entity",
                    "groupID": [
                        43,
                        48
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        49,
                        44
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 47,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "40",
                            "dataType": "int",
                            "matchType": "GET",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "AllGroupBys": [
                {
                    "id": 31,
                    "groupType": "entity",
                    "groupID": [
                        43,
                        48
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        49,
                        44
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 47,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "40",
                            "dataType": "int",
                            "matchType": "GET",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "Parents": null
        },
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 73,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 71,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 73,
                            "toType": "groupBy",
                            "toID": 31,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 0,
                            "name": "",
                            "constraints": null
                        },
                        "ToGroupBy": {
                            "id": 31,
                            "groupType": "entity",
                            "groupID": [
                                43,
                                48
                            ],
                            "groupAttribute": "age",
                            "byType": "entity",
                            "byID": [
                                49,
                                44
                            ],
                            "byAttribute": "_id",
                            "appliedModifier": "AVG",
                            "relationID": 47,
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "40",
                                    "dataType": "int",
                                    "matchType": "GET",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 73,
                "name": "parliament",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": [
                1
            ]
        }
    ]
}
`)

var TopGroupByTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 9,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 8,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 9,
                            "toType": "entity",
                            "toID": 10,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 10,
                            "name": "parties",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 9,
                "name": "parliament",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": [
                {
                    "id": 13,
                    "groupType": "entity",
                    "groupID": [
                        9
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        10
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 8,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "42",
                            "dataType": "int",
                            "matchType": "GET",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "AllGroupBys": [
                {
                    "id": 13,
                    "groupType": "entity",
                    "groupID": [
                        9
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        10
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 8,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "42",
                            "dataType": "int",
                            "matchType": "GET",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "Parents": null
        },
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 68,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 67,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 68,
                            "toType": "entity",
                            "toID": 69,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 69,
                            "name": "parties",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 68,
                "name": "parliament",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": [
                {
                    "id": 72,
                    "groupType": "entity",
                    "groupID": [
                        69
                    ],
                    "groupAttribute": "seats",
                    "byType": "entity",
                    "byID": [
                        68
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 67,
                    "constraints": [
                        {
                            "attribute": "seats",
                            "value": "13",
                            "dataType": "int",
                            "matchType": "LT",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "AllGroupBys": [
                {
                    "id": 72,
                    "groupType": "entity",
                    "groupID": [
                        69
                    ],
                    "groupAttribute": "seats",
                    "byType": "entity",
                    "byID": [
                        68
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 67,
                    "constraints": [
                        {
                            "attribute": "seats",
                            "value": "13",
                            "dataType": "int",
                            "matchType": "LT",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "Parents": null
        },
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "",
                            "constraints": null
                        },
                        "FromGroupBy": {
                            "id": 72,
                            "groupType": "entity",
                            "groupID": [
                                69
                            ],
                            "groupAttribute": "seats",
                            "byType": "entity",
                            "byID": [
                                68
                            ],
                            "byAttribute": "_id",
                            "appliedModifier": "AVG",
                            "relationID": 67,
                            "constraints": [
                                {
                                    "attribute": "seats",
                                    "value": "13",
                                    "dataType": "int",
                                    "matchType": "LT",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "Rel": {
                            "id": 76,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "groupBy",
                            "fromID": 72,
                            "toType": "groupBy",
                            "toID": 13,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 0,
                            "name": "",
                            "constraints": null
                        },
                        "ToGroupBy": {
                            "id": 13,
                            "groupType": "entity",
                            "groupID": [
                                9
                            ],
                            "groupAttribute": "age",
                            "byType": "entity",
                            "byID": [
                                10
                            ],
                            "byAttribute": "_id",
                            "appliedModifier": "AVG",
                            "relationID": 8,
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "42",
                                    "dataType": "int",
                                    "matchType": "GET",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 0,
                "name": "",
                "constraints": null
            },
            "TopGroupBy": {
                "id": 13,
                "groupType": "entity",
                "groupID": [
                    9
                ],
                "groupAttribute": "age",
                "byType": "entity",
                "byID": [
                    10
                ],
                "byAttribute": "_id",
                "appliedModifier": "AVG",
                "relationID": 8,
                "constraints": [
                    {
                        "attribute": "age",
                        "value": "42",
                        "dataType": "int",
                        "matchType": "GET",
                        "inID": 0,
                        "inType": ""
                    }
                ]
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": [
                0
            ]
        }
    ]
}
`)

var WayTooLargeQueryTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 9,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 8,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 9,
                            "toType": "entity",
                            "toID": 10,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 10,
                            "name": "parties",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 9,
                "name": "parliament",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": [
                {
                    "id": 13,
                    "groupType": "entity",
                    "groupID": [
                        9
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        10
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 8,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "42",
                            "dataType": "int",
                            "matchType": "GET",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "AllGroupBys": [
                {
                    "id": 13,
                    "groupType": "entity",
                    "groupID": [
                        9
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        10
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 8,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "42",
                            "dataType": "int",
                            "matchType": "GET",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "Parents": null
        },
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 68,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 67,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 68,
                            "toType": "entity",
                            "toID": 69,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 69,
                            "name": "parties",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 68,
                "name": "parliament",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": [
                {
                    "id": 72,
                    "groupType": "entity",
                    "groupID": [
                        69
                    ],
                    "groupAttribute": "seats",
                    "byType": "entity",
                    "byID": [
                        68
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 67,
                    "constraints": [
                        {
                            "attribute": "seats",
                            "value": "13",
                            "dataType": "int",
                            "matchType": "LT",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "AllGroupBys": [
                {
                    "id": 72,
                    "groupType": "entity",
                    "groupID": [
                        69
                    ],
                    "groupAttribute": "seats",
                    "byType": "entity",
                    "byID": [
                        68
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 67,
                    "constraints": [
                        {
                            "attribute": "seats",
                            "value": "13",
                            "dataType": "int",
                            "matchType": "LT",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "Parents": null
        },
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 93,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "32",
                                    "dataType": "int",
                                    "matchType": "GET",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 98,
                            "name": "part_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 93,
                            "toType": "entity",
                            "toID": 104,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 104,
                            "name": "commissions",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": [
                        1
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 93,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "32",
                                    "dataType": "int",
                                    "matchType": "GET",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 92,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 93,
                            "toType": "groupBy",
                            "toID": 13,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 0,
                            "name": "",
                            "constraints": null
                        },
                        "ToGroupBy": {
                            "id": 13,
                            "groupType": "entity",
                            "groupID": [
                                9
                            ],
                            "groupAttribute": "age",
                            "byType": "entity",
                            "byID": [
                                10
                            ],
                            "byAttribute": "_id",
                            "appliedModifier": "AVG",
                            "relationID": 8,
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "42",
                                    "dataType": "int",
                                    "matchType": "GET",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        }
                    },
                    "Parent": 0,
                    "Children": [
                        2
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "",
                            "constraints": null
                        },
                        "FromGroupBy": {
                            "id": 72,
                            "groupType": "entity",
                            "groupID": [
                                69
                            ],
                            "groupAttribute": "seats",
                            "byType": "entity",
                            "byID": [
                                68
                            ],
                            "byAttribute": "_id",
                            "appliedModifier": "AVG",
                            "relationID": 67,
                            "constraints": [
                                {
                                    "attribute": "seats",
                                    "value": "13",
                                    "dataType": "int",
                                    "matchType": "LT",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "Rel": {
                            "id": 76,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "groupBy",
                            "fromID": 72,
                            "toType": "groupBy",
                            "toID": 13,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 0,
                            "name": "",
                            "constraints": null
                        },
                        "ToGroupBy": {
                            "id": 13,
                            "groupType": "entity",
                            "groupID": [
                                9
                            ],
                            "groupAttribute": "age",
                            "byType": "entity",
                            "byID": [
                                10
                            ],
                            "byAttribute": "_id",
                            "appliedModifier": "AVG",
                            "relationID": 8,
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "42",
                                    "dataType": "int",
                                    "matchType": "GET",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        }
                    },
                    "Parent": 1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 104,
                "name": "commissions",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": [
                {
                    "id": 106,
                    "groupType": "entity",
                    "groupID": [
                        93
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        104
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 98,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "69",
                            "dataType": "int",
                            "matchType": "LET",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "AllGroupBys": [
                {
                    "id": 106,
                    "groupType": "entity",
                    "groupID": [
                        93
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        104
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 98,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "69",
                            "dataType": "int",
                            "matchType": "LET",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "Parents": [
                0
            ]
        },
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 119,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "67",
                                    "dataType": "int",
                                    "matchType": "LET",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 124,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 119,
                            "toType": "entity",
                            "toID": 126,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 126,
                            "name": "parties",
                            "constraints": [
                                {
                                    "attribute": "seats",
                                    "value": "14",
                                    "dataType": "int",
                                    "matchType": "LET",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": [
                        1
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 119,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "67",
                                    "dataType": "int",
                                    "matchType": "LET",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 118,
                            "name": "part_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 119,
                            "toType": "groupBy",
                            "toID": 106,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 0,
                            "name": "",
                            "constraints": null
                        },
                        "ToGroupBy": {
                            "id": 106,
                            "groupType": "entity",
                            "groupID": [
                                93
                            ],
                            "groupAttribute": "age",
                            "byType": "entity",
                            "byID": [
                                104
                            ],
                            "byAttribute": "_id",
                            "appliedModifier": "AVG",
                            "relationID": 98,
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "69",
                                    "dataType": "int",
                                    "matchType": "LET",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        }
                    },
                    "Parent": 0,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 126,
                "name": "parties",
                "constraints": [
                    {
                        "attribute": "seats",
                        "value": "14",
                        "dataType": "int",
                        "matchType": "LET",
                        "inID": 0,
                        "inType": ""
                    }
                ]
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": [
                {
                    "id": 138,
                    "groupType": "entity",
                    "groupID": [
                        119
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        126
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "PRODUCT",
                    "relationID": 124,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "8008",
                            "dataType": "int",
                            "matchType": "GET",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "AllGroupBys": [
                {
                    "id": 138,
                    "groupType": "entity",
                    "groupID": [
                        119
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        126
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "PRODUCT",
                    "relationID": 124,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "8008",
                            "dataType": "int",
                            "matchType": "GET",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "Parents": [
                2
            ]
        },
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 145,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 144,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 145,
                            "toType": "groupBy",
                            "toID": 138,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 0,
                            "name": "",
                            "constraints": null
                        },
                        "ToGroupBy": {
                            "id": 138,
                            "groupType": "entity",
                            "groupID": [
                                119
                            ],
                            "groupAttribute": "age",
                            "byType": "entity",
                            "byID": [
                                126
                            ],
                            "byAttribute": "_id",
                            "appliedModifier": "PRODUCT",
                            "relationID": 124,
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "8008",
                                    "dataType": "int",
                                    "matchType": "GET",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 145,
                "name": "parliament",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": [
                3
            ]
        }
    ]
}
`)

var AllStreamersTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 2,
                            "name": "Streamer",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 1,
                            "name": "viewerOverlap",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 2,
                            "toType": "entity",
                            "toID": 3,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 3,
                            "name": "Streamer",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 2,
                "name": "Streamer",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": [
                {
                    "id": 6,
                    "groupType": "entity",
                    "groupID": [
                        2
                    ],
                    "groupAttribute": "Count",
                    "byType": "entity",
                    "byID": [
                        3
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 1,
                    "constraints": []
                },
                {
                    "id": 7,
                    "groupType": "entity",
                    "groupID": [
                        2
                    ],
                    "groupAttribute": "Count",
                    "byType": "entity",
                    "byID": [
                        3
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 1,
                    "constraints": []
                }
            ],
            "AllGroupBys": [
                {
                    "id": 6,
                    "groupType": "entity",
                    "groupID": [
                        2
                    ],
                    "groupAttribute": "Count",
                    "byType": "entity",
                    "byID": [
                        3
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 1,
                    "constraints": []
                },
                {
                    "id": 7,
                    "groupType": "entity",
                    "groupID": [
                        2
                    ],
                    "groupAttribute": "Count",
                    "byType": "entity",
                    "byID": [
                        3
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 1,
                    "constraints": []
                }
            ],
            "Parents": null
        },
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "",
                            "constraints": null
                        },
                        "FromGroupBy": {
                            "id": 6,
                            "groupType": "entity",
                            "groupID": [
                                2
                            ],
                            "groupAttribute": "Count",
                            "byType": "entity",
                            "byID": [
                                3
                            ],
                            "byAttribute": "_id",
                            "appliedModifier": "AVG",
                            "relationID": 1,
                            "constraints": []
                        },
                        "Rel": {
                            "id": 14,
                            "name": "viewerOverlap",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "groupBy",
                            "fromID": 6,
                            "toType": "groupBy",
                            "toID": 7,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 0,
                            "name": "",
                            "constraints": null
                        },
                        "ToGroupBy": {
                            "id": 7,
                            "groupType": "entity",
                            "groupID": [
                                2
                            ],
                            "groupAttribute": "Count",
                            "byType": "entity",
                            "byID": [
                                3
                            ],
                            "byAttribute": "_id",
                            "appliedModifier": "AVG",
                            "relationID": 1,
                            "constraints": []
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 0,
                "name": "",
                "constraints": null
            },
            "TopGroupBy": {
                "id": 6,
                "groupType": "entity",
                "groupID": [
                    2
                ],
                "groupAttribute": "Count",
                "byType": "entity",
                "byID": [
                    3
                ],
                "byAttribute": "_id",
                "appliedModifier": "AVG",
                "relationID": 1,
                "constraints": []
            },
            "GroupBys": [
                {
                    "id": 21,
                    "groupType": "groupBy",
                    "groupID": [
                        6
                    ],
                    "groupAttribute": "Count",
                    "byType": "groupBy",
                    "byID": [
                        7
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 14,
                    "constraints": []
                }
            ],
            "AllGroupBys": [
                {
                    "id": 21,
                    "groupType": "groupBy",
                    "groupID": [
                        6
                    ],
                    "groupAttribute": "Count",
                    "byType": "groupBy",
                    "byID": [
                        7
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 14,
                    "constraints": []
                }
            ],
            "Parents": [
                0
            ]
        }
    ]
}`)

var StringMatchTypesTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 2,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "name",
                                    "value": "Geert Wilders",
                                    "dataType": "string",
                                    "matchType": "NEQ",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 1,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 2,
                            "toType": "entity",
                            "toID": 3,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 3,
                            "name": "parties",
                            "constraints": [
                                {
                                    "attribute": "name",
                                    "value": "v",
                                    "dataType": "string",
                                    "matchType": "excludes",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": [
                        1
                    ]
                },
                {
                    "Self": {
                        "FromNode": {
                            "id": 2,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "name",
                                    "value": "Geert Wilders",
                                    "dataType": "string",
                                    "matchType": "NEQ",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 6,
                            "name": "part_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 2,
                            "toType": "entity",
                            "toID": 8,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 8,
                            "name": "commissions",
                            "constraints": [
                                {
                                    "attribute": "name",
                                    "value": "groep",
                                    "dataType": "string",
                                    "matchType": "excludes",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": 0,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 3,
                "name": "parties",
                "constraints": [
                    {
                        "attribute": "name",
                        "value": "v",
                        "dataType": "string",
                        "matchType": "excludes",
                        "inID": 0,
                        "inType": ""
                    }
                ]
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": null
        }
    ]
}`)

var TopEntityToFromGroupByTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 26,
                            "name": "Streamer",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 25,
                            "name": "viewerOverlap",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 26,
                            "toType": "entity",
                            "toID": 27,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 27,
                            "name": "Streamer",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 26,
                "name": "Streamer",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": [
                {
                    "id": 30,
                    "groupType": "entity",
                    "groupID": [
                        27
                    ],
                    "groupAttribute": "Count",
                    "byType": "entity",
                    "byID": [
                        26
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "avg",
                    "relationID": 25,
                    "constraints": []
                }
            ],
            "AllGroupBys": [
                {
                    "id": 30,
                    "groupType": "entity",
                    "groupID": [
                        27
                    ],
                    "groupAttribute": "Count",
                    "byType": "entity",
                    "byID": [
                        26
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "avg",
                    "relationID": 25,
                    "constraints": []
                }
            ],
            "Parents": null
        },
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 0,
                            "name": "",
                            "constraints": null
                        },
                        "FromGroupBy": {
                            "id": 30,
                            "groupType": "entity",
                            "groupID": [
                                27
                            ],
                            "groupAttribute": "Count",
                            "byType": "entity",
                            "byID": [
                                26
                            ],
                            "byAttribute": "_id",
                            "appliedModifier": "avg",
                            "relationID": 25,
                            "constraints": []
                        },
                        "Rel": {
                            "id": 34,
                            "name": "viewerOverlap",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "groupBy",
                            "fromID": 30,
                            "toType": "entity",
                            "toID": 36,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 36,
                            "name": "Streamer",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 36,
                "name": "Streamer",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": [
                0
            ]
        }
    ]
}`)

var IntNotEqualsTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 2,
                            "name": "parliament",
                            "constraints": [
                                {
                                    "attribute": "seniority",
                                    "value": "57",
                                    "dataType": "int",
                                    "matchType": "NEQ",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 1,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 2,
                            "toType": "entity",
                            "toID": 3,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 3,
                            "name": "parties",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 2,
                "name": "parliament",
                "constraints": [
                    {
                        "attribute": "seniority",
                        "value": "57",
                        "dataType": "int",
                        "matchType": "NEQ",
                        "inID": 0,
                        "inType": ""
                    }
                ]
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": null
        }
    ]
}`)

var BoolEqualTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 8,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 7,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 8,
                            "toType": "entity",
                            "toID": 9,
                            "constraints": [
                                {
                                    "attribute": "isChairman",
                                    "value": "true",
                                    "dataType": "bool",
                                    "matchType": "EQ",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToNode": {
                            "id": 9,
                            "name": "parties",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 8,
                "name": "parliament",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": null
        }
    ]
}`)

var BoolNotEqualTreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 8,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 7,
                            "name": "member_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 8,
                            "toType": "entity",
                            "toID": 9,
                            "constraints": [
                                {
                                    "attribute": "isChairman",
                                    "value": "true",
                                    "dataType": "bool",
                                    "matchType": "NEQ",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        },
                        "ToNode": {
                            "id": 9,
                            "name": "parties",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 8,
                "name": "parliament",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": null
        }
    ]
}`)

var NestedGroupBys2TreeList = string(`{
    "Trees": [
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 9,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 8,
                            "name": "part_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 9,
                            "toType": "entity",
                            "toID": 10,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 10,
                            "name": "commissions",
                            "constraints": []
                        },
                        "ToGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 9,
                "name": "parliament",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": [
                {
                    "id": 6,
                    "groupType": "entity",
                    "groupID": [
                        9
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        10
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 8,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "41",
                            "dataType": "int",
                            "matchType": "LET",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "AllGroupBys": [
                {
                    "id": 6,
                    "groupType": "entity",
                    "groupID": [
                        9
                    ],
                    "groupAttribute": "age",
                    "byType": "entity",
                    "byID": [
                        10
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "AVG",
                    "relationID": 8,
                    "constraints": [
                        {
                            "attribute": "age",
                            "value": "41",
                            "dataType": "int",
                            "matchType": "LET",
                            "inID": 0,
                            "inType": ""
                        }
                    ]
                }
            ],
            "Parents": null
        },
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 25,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 24,
                            "name": "part_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 25,
                            "toType": "groupBy",
                            "toID": 6,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 0,
                            "name": "",
                            "constraints": null
                        },
                        "ToGroupBy": {
                            "id": 6,
                            "groupType": "entity",
                            "groupID": [
                                9
                            ],
                            "groupAttribute": "age",
                            "byType": "entity",
                            "byID": [
                                10
                            ],
                            "byAttribute": "_id",
                            "appliedModifier": "AVG",
                            "relationID": 8,
                            "constraints": [
                                {
                                    "attribute": "age",
                                    "value": "41",
                                    "dataType": "int",
                                    "matchType": "LET",
                                    "inID": 0,
                                    "inType": ""
                                }
                            ]
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 25,
                "name": "parliament",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": [
                {
                    "id": 40,
                    "groupType": "entity",
                    "groupID": [
                        25
                    ],
                    "groupAttribute": "age",
                    "byType": "groupBy",
                    "byID": [
                        6
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "MAX",
                    "relationID": 24,
                    "constraints": []
                }
            ],
            "AllGroupBys": [
                {
                    "id": 40,
                    "groupType": "entity",
                    "groupID": [
                        25
                    ],
                    "groupAttribute": "age",
                    "byType": "groupBy",
                    "byID": [
                        6
                    ],
                    "byAttribute": "_id",
                    "appliedModifier": "MAX",
                    "relationID": 24,
                    "constraints": []
                }
            ],
            "Parents": [
                0
            ]
        },
        {
            "TreeElements": [
                {
                    "Self": {
                        "FromNode": {
                            "id": 52,
                            "name": "parliament",
                            "constraints": []
                        },
                        "FromGroupBy": {
                            "id": 0,
                            "groupType": "",
                            "groupID": null,
                            "groupAttribute": "",
                            "byType": "",
                            "byID": null,
                            "byAttribute": "",
                            "appliedModifier": "",
                            "relationID": 0,
                            "constraints": null
                        },
                        "Rel": {
                            "id": 51,
                            "name": "part_of",
                            "depth": {
                                "min": 1,
                                "max": 1
                            },
                            "fromType": "entity",
                            "fromID": 52,
                            "toType": "groupBy",
                            "toID": 40,
                            "constraints": []
                        },
                        "ToNode": {
                            "id": 0,
                            "name": "",
                            "constraints": null
                        },
                        "ToGroupBy": {
                            "id": 40,
                            "groupType": "entity",
                            "groupID": [
                                25
                            ],
                            "groupAttribute": "age",
                            "byType": "groupBy",
                            "byID": [
                                6
                            ],
                            "byAttribute": "_id",
                            "appliedModifier": "MAX",
                            "relationID": 24,
                            "constraints": []
                        }
                    },
                    "Parent": -1,
                    "Children": []
                }
            ],
            "TopNode": {
                "id": 52,
                "name": "parliament",
                "constraints": []
            },
            "TopGroupBy": {
                "id": 0,
                "groupType": "",
                "groupID": null,
                "groupAttribute": "",
                "byType": "",
                "byID": null,
                "byAttribute": "",
                "appliedModifier": "",
                "relationID": 0,
                "constraints": null
            },
            "GroupBys": null,
            "AllGroupBys": null,
            "Parents": [
                1
            ]
        }
    ]
}`)
