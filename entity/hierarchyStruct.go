/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entity

type GroupByDone struct {
	RelationDone bool
	GroupIDDone  map[int]bool
	ByIDDone     map[int]bool
}

type TreeList struct {
	Trees []Tree
}

type Tree struct {
	TreeElements []TreeElement
	TopNode      QueryEntityStruct
	TopGroupBy   QueryGroupByStruct
	GroupBys     []QueryGroupByStruct
	AllGroupBys  []QueryGroupByStruct
	Parents      []int
}

type Triple struct {
	FromNode    QueryEntityStruct
	FromGroupBy QueryGroupByStruct
	Rel         QueryRelationStruct
	ToNode      QueryEntityStruct
	ToGroupBy   QueryGroupByStruct
}

type TreeElement struct {
	Self     Triple
	Parent   int
	Children []int
}
