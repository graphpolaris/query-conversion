/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entity

/*
IncomingQueryJSON describes the query coming into the service in JSON format
*/
type IncomingQueryJSON struct {
	DatabaseName    string                `json:"databaseName"`
	Return          QueryReturnStruct     `json:"return"`
	Entities        []QueryEntityStruct   `json:"entities"`
	Relations       []QueryRelationStruct `json:"relations"`
	GroupBys        []QueryGroupByStruct  `json:"groupBys"`
	MachineLearning []QueryMLStruct       `json:"machineLearning"`
	// Limit is for limiting the amount of paths AQL will return in a relation let statement
	Limit     int `json:"limit"`
	Modifiers []QueryModifierStruct
	Prefix    string
}

/*
QueryReturnStruct holds the indices of the entities and relations that need to be returned
*/
type QueryReturnStruct struct {
	Entities  []int `json:"entities"`
	Relations []int `json:"relations"`
	GroupBys  []int `json:"groupBys"`
	//Modifiers []int
}

/*
QueryEntityStruct encapsulates a single entity with its corresponding constraints
*/
type QueryEntityStruct struct {
	ID          int                     `json:"id"`
	Name        string                  `json:"name"`
	Constraints []QueryConstraintStruct `json:"constraints"`
}

/*
QueryRelationStruct encapsulates a single relation with its corresponding constraints
*/
type QueryRelationStruct struct {
	ID          int                     `json:"id"`
	Name        string                  `json:"name"`
	Depth       QuerySearchDepthStruct  `json:"depth"`
	FromType    string                  `json:"fromType"`
	FromID      int                     `json:"fromID"`
	ToType      string                  `json:"toType"`
	ToID        int                     `json:"toID"`
	Constraints []QueryConstraintStruct `json:"constraints"`
}

/*
QueryGroupByStruct holds all the info needed to form a group by
*/
type QueryGroupByStruct struct {
	ID              int                     `json:"id"`
	GroupType       string                  `json:"groupType"`
	GroupID         []int                   `json:"groupID"`
	GroupAttribute  string                  `json:"groupAttribute"`
	ByType          string                  `json:"byType"`
	ByID            []int                   `json:"byID"`
	ByAttribute     string                  `json:"byAttribute"`
	AppliedModifier string                  `json:"appliedModifier"`
	RelationID      int                     `json:"relationID"`
	Constraints     []QueryConstraintStruct `json:"constraints"`
}

/*
QueryConstraintStruct holds the information of the constraint
Constraint datatypes
    string     MatchTypes: exact/contains/startswith/endswith
    int   MatchTypes: GT/LT/EQ
    bool     MatchTypes: EQ/NEQ
*/
type QueryConstraintStruct struct {
	Attribute string `json:"attribute"`
	Value     string `json:"value"`
	DataType  string `json:"dataType"`
	MatchType string `json:"matchType"`
	InID      int    `json:"inID"`
	InType    string `json:"inType"`
}

/*
QueryMLStruct holds info for machinelearning
*/
type QueryMLStruct struct {
	Queuename  string
	Parameters []string
}

/*
QueryModifierStruct encapsulates a single modifier with its corresponding constraints
*/
type QueryModifierStruct struct {
	Type           string // SUM COUNT AVG
	SelectedType   string // node relation
	SelectedTypeID int    // ID of the enitity or relation
	AttributeIndex int    // = -1 if its the node or relation, = > -1 if an attribute is selected
}

/*
QuerySearchDepthStruct holds the range of traversals for the relation
*/
type QuerySearchDepthStruct struct {
	Min int `json:"min"`
	Max int `json:"max"`
}

/*
Maps a relation (e.g. r1) to the two entities it's connceted to (e.g. e0,e1)
This is our neccesary metadata for executing a query and converting the output into our standard format
*/
type MetaData struct {
	Triples map[string]Tuple
}

/*
Go has no tuples, so we make one ourselves specifically for SPARQL subject-predicate-object relation types
*/
type Tuple struct {
	From string
	To   string
}

/*
FindE finds the entity with a specified ID in an IncomingQueryJSON struct
*/
func (JSONQuery IncomingQueryJSON) FindE(qID int) *QueryEntityStruct {
	for _, part := range JSONQuery.Entities {
		if part.ID == qID {
			return &part
		}
	}
	return nil
}

/*
FindR finds the relation with a specified ID in an IncomingQueryJSON struct
*/
func (JSONQuery IncomingQueryJSON) FindR(qID int) *QueryRelationStruct {
	for _, part := range JSONQuery.Relations {
		if part.ID == qID {
			return &part
		}
	}
	return nil
}

/*
FindG finds the groupBy with a specified ID in an IncomingQueryJSON struct
*/
func (JSONQuery IncomingQueryJSON) FindG(qID int) *QueryGroupByStruct {
	for _, part := range JSONQuery.GroupBys {
		if part.ID == qID {
			return &part
		}
	}
	return nil
}

/*
QueryPart is a struct containing a part of the query and a list of dependencies on which this part of the query depends
*/
type QueryPart struct {
	QType        string // Eg if it is a relation or groupby
	QID          int    // ID of said relation/gb
	PartID       int    // Custom ID used for dependency
	Dependencies []int  // List of partID's that need to come before
}

/*
Query is a list of (possibly unordered) queryparts
*/
type Query []QueryPart

/*
Find retrieves a QueryPart based on the query's specifications
*/
func (q Query) Find(qID int, qType string) *QueryPart {
	for i := range q {
		if q[i].QID == qID && q[i].QType == qType {
			return &q[i]
		}
	}
	return nil
}

/*
SelectByID retrieves a QueryPart based on its PartID
*/
func (q Query) SelectByID(ID int) *QueryPart {
	for i := range q {
		if q[i].PartID == ID {
			return &q[i]
		}
	}
	return nil
}

type QueryGenericStruct struct {
	Type string
	ID   int
}
