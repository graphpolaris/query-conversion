/*
 This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
 © Copyright Utrecht University(Department of Information and Computing Sciences)
*/

package query

import "git.science.uu.nl/graphpolaris/query-conversion/entityv2"

// A Converter converts an incoming message in our JSON format to a format like AQL or Cypher
type Converter interface {
	ConvertQuery(JSONQuery *entityv2.IncomingQueryJSON) (*string, *[]byte, error)
}
