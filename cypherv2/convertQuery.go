/*
 This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
 © Copyright Utrecht University(Department of Information and Computing Sciences)
*/

package cypherv2

import (
	"errors"
	"fmt"
	"strings"

	"git.science.uu.nl/graphpolaris/query-conversion/entityv2"
)

type relationCacheData struct {
	id      string
	queryId string
}
type entityCacheData struct {
	id      string
	queryId string
}

type queryCacheData struct {
	entities  []entityCacheData
	relations []relationCacheData
}

// ConvertQuery takes the json from the visual query builder and converts it into Cypher
func (s *Service) ConvertQuery(totalJSONQuery *entityv2.IncomingQueryJSON) (*string, *[]byte, error) {
	var finalCypher *string

	queryJSON := totalJSONQuery

	finalCypher, err := createCypher(queryJSON)
	if err != nil {
		return nil, nil, err
	}

	return finalCypher, nil, nil
}

// createCypher translates a cluster of nodes (query) to Cypher
func createCypher(JSONQuery *entityv2.IncomingQueryJSON) (*string, error) {
	// translate it to cypher in the right order, using the hierarchy
	cypher, err := formQuery(JSONQuery)
	if err != nil {
		return nil, err
	}

	return cypher, nil
}

func getNodeCypher(JSONQuery *entityv2.NodeStruct) (cypher string, cacheData queryCacheData, err error) {
	label := ""
	if JSONQuery.Label != "" {
		label = fmt.Sprintf(":%s", JSONQuery.Label)
	}
	id := ""
	if JSONQuery.ID != "" {
		id = JSONQuery.ID
	}

	if id != "" {
		cacheData.entities = append(cacheData.entities, entityCacheData{id, ""})
	}

	cypher = fmt.Sprintf("(%s%s)", id, label)

	if JSONQuery.Relation != (entityv2.RelationStruct{}) {
		relationCypher, subCache, err := getRelationCypher(&JSONQuery.Relation)
		cacheData.entities = append(cacheData.entities, subCache.entities...)
		cacheData.relations = append(cacheData.relations, subCache.relations...)
		if err != nil {
			return "", cacheData, err
		}
		if JSONQuery.Relation.Direction == "FROM" {
			cypher += fmt.Sprintf("<-%s", relationCypher)
		} else if JSONQuery.Relation.Direction == "TO" {
			cypher += fmt.Sprintf("-%s", relationCypher)
		} else {
			cypher += fmt.Sprintf("-%s", relationCypher)
		}
	}
	return cypher, cacheData, nil
}

func getRelationCypher(JSONQuery *entityv2.RelationStruct) (cypher string, cacheData queryCacheData, err error) {
	label := ""
	if JSONQuery.Label != "" {
		label = fmt.Sprintf(":%s", JSONQuery.Label)
	}
	id := ""
	if JSONQuery.ID != "" {
		id = JSONQuery.ID
	}
	depth := ""
	if JSONQuery.Depth != (entityv2.QuerySearchDepthStruct{}) {
		depth = fmt.Sprintf("*%d..%d", JSONQuery.Depth.Min, JSONQuery.Depth.Max)
	}

	if id != "" {
		cacheData.relations = append(cacheData.relations, relationCacheData{id, ""})
	}

	cypher = fmt.Sprintf("[%s%s%s]", id, label, depth)

	if JSONQuery.Node == (&entityv2.NodeStruct{}) {
		return "", cacheData, errors.New("relation must have a node")
	}

	nodeCypher, subCache, err := getNodeCypher(JSONQuery.Node)
	cacheData.entities = append(cacheData.entities, subCache.entities...)
	cacheData.relations = append(cacheData.relations, subCache.relations...)
	if err != nil {
		return "", cacheData, err
	}
	if JSONQuery.Direction == "TO" {
		cypher += fmt.Sprintf("->%s", nodeCypher)
	} else if JSONQuery.Direction == "FROM" {
		cypher += fmt.Sprintf("-%s", nodeCypher)
	} else {
		cypher += fmt.Sprintf("-%s", nodeCypher)
	}
	return cypher, cacheData, nil
}

func createFilterCypher(JSONQuery *entityv2.FilterStruct, ID string) (string, error) {
	cypher := fmt.Sprintf("%s.%s", ID, JSONQuery.Attribute)
	switch JSONQuery.Operation {
	case "EQ":
		cypher += fmt.Sprintf(" = %s", JSONQuery.Value)
	case "NEQ":
		cypher += fmt.Sprintf(" <> %s", JSONQuery.Value)
	case "GT":
		cypher += fmt.Sprintf(" > %s", JSONQuery.Value)
	case "GTE":
		cypher += fmt.Sprintf(" >= %s", JSONQuery.Value)
	case "LT":
		cypher += fmt.Sprintf(" < %s", JSONQuery.Value)
	case "LTE":
		cypher += fmt.Sprintf(" <= %s", JSONQuery.Value)
	case "CONTAINS":
		cypher += fmt.Sprintf(" CONTAINS %s", JSONQuery.Value)
	case "STARTS_WITH":
		cypher += fmt.Sprintf(" STARTS WITH %s", JSONQuery.Value)
	case "ENDS_WITH":
		cypher += fmt.Sprintf(" ENDS WITH %s", JSONQuery.Value)
	case "IN":
		cypher += fmt.Sprintf(" IN %s", JSONQuery.Value)
	case "NOT_IN":
		cypher += fmt.Sprintf(" NOT IN %s", JSONQuery.Value)
	case "IS_NULL":
		cypher += " IS NULL"
	case "IS_NOT_NULL":
		cypher += " IS NOT NULL"
	default:
		return "", errors.New("operator not supported")
	}
	return cypher, nil
}

func extractFilterCypher(JSONQuery *entityv2.NodeStruct) ([]string, error) {
	filters := []string{}
	for _, filter := range JSONQuery.Filter {
		if filter != (entityv2.FilterStruct{}) {
			filterCypher, err := createFilterCypher(&filter, JSONQuery.ID)
			if err != nil {
				return filters, err
			}
			filters = append(filters, filterCypher)
		}
	}

	if JSONQuery.Relation != (entityv2.RelationStruct{}) {
		relationCypher, err := extractFilterCypher(JSONQuery.Relation.Node)
		if err != nil {
			return filters, err
		}
		filters = append(filters, relationCypher...)
	}
	return filters, nil
}

func createWhereLogic(op string, left string, whereLogic string, cacheData queryCacheData) (interface{}, string, error) {
	newWhereLogic := fmt.Sprintf("%s_%s", strings.ReplaceAll(left, ".", "_"), op)
	if whereLogic != "" {
		whereLogic += ", "
	}
	remainingNodes := []string{}

	for _, entity := range cacheData.entities {
		if entity.id != left {
			remainingNodes = append(remainingNodes, entity.id)
		}
	}

	// TODO: Relation temporarily ignored due to unnecessary added complexity in the query
	// for _, relation := range cacheData.relations {
	// 	if relation.id != left {
	// 		remainingNodes = append(remainingNodes, relation.id)
	// 	}
	// }

	remainingNodesStr := ""
	if len(remainingNodes) > 0 {
		remainingNodesStr = ", " + strings.Join(remainingNodes, ", ")
	}
	newWithLogic := fmt.Sprintf("%s%s(%s) AS %s%s", whereLogic, op, left, newWhereLogic, remainingNodesStr)

	return newWhereLogic, newWithLogic, nil
}

func extractLogicCypher(LogicQuery interface{}, cacheData queryCacheData) (interface{}, string, error) {
	switch v := LogicQuery.(type) {
	case []interface{}:
		op := strings.ReplaceAll(v[0].(string), "_", "")
		op = strings.ToLower(op)
		left, whereLogic, err := extractLogicCypher(v[1], cacheData)
		if err != nil {
			return "", "", err
		}
		switch op {
		// Operations
		case "!=":
			op = "<>"
		case "==":
			op = "="
		case "like":
			op = "=~"
		case "isempty":
			return fmt.Sprintf(`(%s IS NULL OR %s = "")`, left, left), whereLogic, nil
		case "lower":
			return fmt.Sprintf("toLower(%s)", left), whereLogic, nil
		case "upper":
			return fmt.Sprintf("toUpper(%s)", left), whereLogic, nil
		case "avg":
			return createWhereLogic(op, left.(string), whereLogic, cacheData)
		case "count":
			return createWhereLogic(op, left.(string), whereLogic, cacheData)
		case "max":
			return createWhereLogic(op, left.(string), whereLogic, cacheData)
		case "min":
			return createWhereLogic(op, left.(string), whereLogic, cacheData)
		case "sum":
			return createWhereLogic(op, left.(string), whereLogic, cacheData)
		}
		if len(v) > 2 {
			right, whereLogicRight, err := extractLogicCypher(v[2], cacheData)
			if whereLogicRight != "" {
				if whereLogic != "" {
					whereLogic += ", "
				}
				whereLogic += whereLogicRight
			}
			if err != nil {
				return "", "", err
			}
			switch op {
			case "=~":
				right = fmt.Sprintf("(\".*\" + %s + \".*\")", right)
			}
			return fmt.Sprintf("(%s %s %s)", left, op, right), whereLogic, nil
		}
		return fmt.Sprintf("(%s %s)", op, left), whereLogic, nil
	case string:
		return strings.ReplaceAll(v, "@", ""), "", nil
	case float64:
		return fmt.Sprintf("%f", v), "", nil
	case float32:
		return fmt.Sprintf("%f", v), "", nil
	case int64:
		return fmt.Sprintf("%d", v), "", nil
	case int32:
		return fmt.Sprintf("%d", v), "", nil
	case int16:
		return fmt.Sprintf("%d", v), "", nil
	case int:
		return fmt.Sprintf("%d", v), "", nil
	default:
		return v, "", nil
	}
}

func createExportCypher(JSONQuery *entityv2.ExportNodeStruct, ID string) (string, error) {
	return fmt.Sprintf("%s.%s as %s", ID, JSONQuery.Attribute, JSONQuery.ID), nil
}

func extractExportCypher(JSONQuery *entityv2.NodeStruct) ([]string, error) {
	exports := []string{}
	for _, export := range JSONQuery.Export {
		if export != (entityv2.ExportNodeStruct{}) {
			exportCypher, err := createExportCypher(&export, JSONQuery.ID)
			if err != nil {
				return exports, err
			}
			exports = append(exports, exportCypher)
		}
	}

	if JSONQuery.Relation != (entityv2.RelationStruct{}) {
		relationCypher, err := extractExportCypher(JSONQuery.Relation.Node)
		if err != nil {
			return exports, err
		}
		exports = append(exports, relationCypher...)
	}
	return exports, nil
}

// formQuery uses the hierarchy to create cypher for each part of the query in the right order
func formQuery(JSONQuery *entityv2.IncomingQueryJSON) (*string, error) {
	totalQuery := ""
	matchQuery := ""
	var cacheData queryCacheData

	// MATCH block
	for _, query := range JSONQuery.Query {
		match := fmt.Sprintf("MATCH ")
		nodeCypher, _cacheData, err := getNodeCypher(&query.Node)
		cacheData = _cacheData
		for i := 0; i < len(cacheData.entities); i++ {
			cacheData.entities[i].queryId = query.ID
		}
		for i := 0; i < len(cacheData.relations); i++ {
			cacheData.relations[i].queryId = query.ID
		}

		// Generate cypher query for path
		if err != nil {
			return nil, err
		}
		if query.ID != "" {
			match += fmt.Sprintf("%s = (%s)", query.ID, nodeCypher)
		} else {
			match += nodeCypher
		}

		// Filters OLD VERSION (keep?)
		// filters, err := extractFilterCypher(&query.Node)
		// if err != nil {
		// 	return nil, err
		// }
		// if len(filters) > 0 && filters[0] != "" {
		// 	match += " WHERE " + fmt.Sprintf(strings.Join(filters, " AND "))
		// }

		// Exports (not used right now)
		exports, err := extractExportCypher(&query.Node)
		if err != nil {
			return nil, err
		}
		if len(exports) > 0 && exports[0] != "" {
			match += " WITH " + fmt.Sprintf(strings.Join(exports, ", "))
		}

		matchQuery = strings.ReplaceAll(strings.ReplaceAll(match, "@", ""), "\\\"", "\"") + "\n"
		totalQuery += matchQuery
	}

	// logic calculation for WHERE block
	if JSONQuery.Logic != nil && JSONQuery.Logic != "" {
		_logic, whereLogic, err := extractLogicCypher(JSONQuery.Logic, cacheData)
		if err != nil {
			return nil, err
		}
		logic := _logic.(string)

		for _, relation := range cacheData.relations {
			// if we need to do logic on relations, this with is required
			if relation.queryId != "" && strings.Contains(logic, relation.id) {
				logic = fmt.Sprintf("ALL(%s_rel_%s in %s WHERE %s)",
					relation.queryId, relation.id, relation.id,
					strings.ReplaceAll(logic, relation.id, fmt.Sprintf("%s_rel_%s", relation.queryId, relation.id)))
			}
		}
		totalQueryWithLogic := totalQuery
		if whereLogic != "" {
			totalQueryWithLogic += fmt.Sprintf("WITH %s\n", whereLogic)
			totalQuery = totalQueryWithLogic + totalQuery
		}
		totalQuery += fmt.Sprintf("WHERE %s\n", logic)
	}

	// RETURN block
	if len(JSONQuery.Return) == 0 || JSONQuery.Return[0] == "*" {
		totalQuery += "RETURN *"
	} else {
		totalQuery += "RETURN " + strings.ReplaceAll(strings.Join(JSONQuery.Return, ", "), "@", "")
	}

	// LIMIT block
	totalQuery += fmt.Sprintf("\nLIMIT %d", JSONQuery.Limit)

	return &totalQuery, nil
}
