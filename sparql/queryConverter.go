/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/
package sparql

// Service implements the QueryConverter interface (in the query service)
type Service struct {
}

// NewService creates a new AQL conversion service
// func NewService() query.Converter {
// 	return &Service{}
// }
