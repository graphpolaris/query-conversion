/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package sparql

var EmptyQueryTripleMap = string(`{
    "Triples": {
        "r0": {
            "From": "e0",
            "To": "e1"
        },
        "r1": {
            "From": "e2",
            "To": "e1"
        }
    }
}`)

var SingleGroupByTripleMap = string(`{
    "Triples": {
        "r0": {
            "From": "e0",
            "To": "e1"
        },
        "r1": {
            "From": "e1",
            "To": "e2"
        },
        "r2": {
            "From": "g0",
            "To": "e3"
        },
        "r3": {
            "From": "e4",
            "To": "g0"
        }
    }
}`)

var NestingAndFilterTypesTripleMap = string(`{
    "Triples": {
        "r0": {
            "From": "e0",
            "To": "e1"
        },
        "r1": {
            "From": "e1",
            "To": "e2"
        },
        "r10": {
            "From": "g4",
            "To": "e11"
        },
        "r11": {
            "From": "e11",
            "To": "e12"
        },
        "r12": {
            "From": "g5",
            "To": "e13"
        },
        "r13": {
            "From": "e14",
            "To": "g5"
        },
        "r2": {
            "From": "g0",
            "To": "e3"
        },
        "r3": {
            "From": "e3",
            "To": "e4"
        },
        "r4": {
            "From": "g1",
            "To": "e5"
        },
        "r5": {
            "From": "e5",
            "To": "e6"
        },
        "r6": {
            "From": "g2",
            "To": "e7"
        },
        "r7": {
            "From": "e7",
            "To": "e8"
        },
        "r8": {
            "From": "g3",
            "To": "e9"
        },
        "r9": {
            "From": "e9",
            "To": "e10"
        }
    }
}`)
