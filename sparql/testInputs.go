/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package sparql

var emptyQuery = []byte(`{
	"return": {
		"entities": [],
		"relations": []
	},
	"entities": [],
	"relations": [],
	"groupBys": [],
	"filters": [],
	"limit": 5000
}`)

var NoRelationsField = []byte(`{
	"databaseName": "TweedeKamer",
	"return": {
		"entities": [
			0,
			1
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 1,
			"constraints": []
		}
	],		
	"groupBys": [],		
	"limit": 5000,
	"modifiers": []
}`)

var IncorrectRelationFrom = []byte(`{
	"databaseName": "TweedeKamer",
	"return": {
		"entities": [
			0,
			1
		],
		"relations": [
			0
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 1,
			"constraints": []
		}
	],
	"relations": [
		{
			"ID": 0,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": -4,
			"constraints":[]
		}
	],
	"groupBys": [],		
	"limit": 5000,
	"modifiers": []
}`)

var SeparatedChainSingleRelationPerChain = []byte(`{
	"databaseName": "TweedeKamer",
	"return": {
		"entities": [
			0,
			1,
			2,
			3
		],
		"relations": [
			0
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 1,
			"constraints": []
		},
		{
			"name": "parliament",
			"ID": 2,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 3,
			"constraints": []
		}
	],
	"relations": [
		{
			"ID": 0,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 1,
			"constraints":[]
		},
		{
			"ID": 1,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 2,
			"toType": "entity",
			"toID": 3,
			"constraints":[]
		}
	],
	"groupBys": [],		
	"limit": 5000,
	"modifiers": []
}`)

var SeparatedChainDoubleRelationPerChain = []byte(`{
	"databaseName": "TweedeKamer",
	"return": {
		"entities": [
			0,
			1,
			2,
			3,
			4,
			5
		],
		"relations": [
			0
		]
	},
	"entities": [
		{
			"name": "parliament",
			"ID": 0,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 1,
			"constraints": []
		},
		{
			"name": "parliament",
			"ID": 2,
			"constraints": []
		},
		{
			"name": "parties",
			"ID": 3,
			"constraints": []
		},
		{
			"name": "resolutions",
			"ID": 4,
			"constraints": []
		},
		{
			"name": "resolutions",
			"ID": 5,
			"constraints": []
		}
	],
	"relations": [
		{
			"ID": 0,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 1,
			"constraints":[]
		},
		{
			"ID": 1,
			"name": "member_of",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 2,
			"toType": "entity",
			"toID": 3,
			"constraints":[]
		},
		{
			"ID": 2,
			"name": "submits",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 0,
			"toType": "entity",
			"toID": 4,
			"constraints":[]
		},
		{
			"ID": 3,
			"name": "submits",
			"depth": {
				"min": 1,
				"max": 1
			},
			"fromType": "entity",
			"fromID": 2,
			"toType": "entity",
			"toID": 5,
			"constraints":[]
		}
	],
	"groupBys": [],		
	"limit": 5000,
	"modifiers": []
}`)

var SeperatedChainWithGroupBys = []byte(`{
	"return": {
	  "entities": [
		2,
		3,
		14,
		28,
		37,
		38,
		47
	  ],
	  "relations": [
		1,
		13,
		26,
		36,
		46
	  ],
	  "groupBys": [
		12,
		35
	  ]
	},
	"entities": [
	  {
		"name": "parliament",
		"ID": 2,
		"constraints": []
	  },
	  {
		"name": "parties",
		"ID": 3,
		"constraints": []
	  },
	  {
		"name": "parliament",
		"ID": 14,
		"constraints": []
	  },
	  {
		"name": "commissions",
		"ID": 28,
		"constraints": [
		  {
			"attribute": "name",
			"value": "f",
			"dataType": "string",
			"matchType": "contains"
		  }
		]
	  },
	  {
		"name": "parliament",
		"ID": 37,
		"constraints": []
	  },
	  {
		"name": "resolutions",
		"ID": 38,
		"constraints": []
	  },
	  {
		"name": "parliament",
		"ID": 47,
		"constraints": []
	  }
	],
	"relations": [
	  {
		"ID": 1,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 2,
		"toType": "entity",
		"toID": 3,
		"constraints": []
	  },
	  {
		"ID": 13,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 14,
		"toType": "groupBy",
		"toID": 12,
		"constraints": []
	  },
	  {
		"ID": 26,
		"name": "part_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 14,
		"toType": "entity",
		"toID": 28,
		"constraints": []
	  },
	  {
		"ID": 36,
		"name": "submits",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 37,
		"toType": "entity",
		"toID": 38,
		"constraints": []
	  },
	  {
		"ID": 46,
		"name": "submits",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 47,
		"toType": "groupBy",
		"toID": 35,
		"constraints": []
	  }
	],
	"groupBys": [
	  {
		"ID": 12,
		"groupType": "entity",
		"groupID": [
		  2
		],
		"groupAttribute": "age",
		"byType": "entity",
		"byID": [
		  3
		],
		"byAttribute": "_id",
		"appliedModifier": "AVG",
		"relationID": 1,
		"constraints": [
		  {
			"attribute": "age",
			"value": "42",
			"dataType": "int",
			"matchType": "GET"
		  }
		]
	  },
	  {
		"ID": 35,
		"groupType": "entity",
		"groupID": [
		  37
		],
		"groupAttribute": "age",
		"byType": "entity",
		"byID": [
		  38
		],
		"byAttribute": "_id",
		"appliedModifier": "AVG",
		"relationID": 36,
		"constraints": [
		  {
			"attribute": "age",
			"value": "45",
			"dataType": "int",
			"matchType": "GET"
		  }
		]
	  }
	],
	"machineLearning": [],
	"limit": 5000
  }`)

var DifferentNodesOnGroupBy = []byte(`{
	"return": {
	  "entities": [
		2,
		3,
		11,
		12
	  ],
	  "relations": [
		1,
		10
	  ],
	  "groupBys": [
		6
	  ]
	},
	"entities": [
	  {
		"name": "parliament",
		"ID": 2,
		"constraints": []
	  },
	  {
		"name": "commissions",
		"ID": 3,
		"constraints": []
	  },
	  {
		"name": "parliament",
		"ID": 11,
		"constraints": []
	  },
	  {
		"name": "parties",
		"ID": 12,
		"constraints": []
	  }
	],
	"relations": [
	  {
		"ID": 1,
		"name": "part_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 2,
		"toType": "entity",
		"toID": 3,
		"constraints": []
	  },
	  {
		"ID": 10,
		"name": "member_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 11,
		"toType": "entity",
		"toID": 12,
		"constraints": []
	  }
	],
	"groupBys": [
	  {
		"ID": 6,
		"groupType": "entity",
		"groupID": [
		  3,
		  11
		],
		"groupAttribute": "age",
		"byType": "entity",
		"byID": [
		  3,
		  12
		],
		"byAttribute": "_id",
		"appliedModifier": "avg",
		"relationID": 1,
		"constraints": []
	  }
	],
	"machineLearning": [],
	"limit": 5000
  }`)

var IncorrectGroupByModifier = []byte(`{
	"return": {
	  "entities": [
		2,
		3
	  ],
	  "relations": [
		1
	  ],
	  "groupBys": [
		6
	  ]
	},
	"entities": [
	  {
		"name": "parliament",
		"ID": 2,
		"constraints": []
	  },
	  {
		"name": "commissions",
		"ID": 3,
		"constraints": []
	  }
	],
	"relations": [
	  {
		"ID": 1,
		"name": "part_of",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 2,
		"toType": "entity",
		"toID": 3,
		"constraints": []
	  }
	],
	"groupBys": [
	  {
		"ID": 6,
		"groupType": "entity",
		"groupID": [
		  2
		],
		"groupAttribute": "age",
		"byType": "entity",
		"byID": [
		  3
		],
		"byAttribute": "_id",
		"appliedModifier": "wibblywobbly",
		"relationID": 1,
		"constraints": []
	  }
	],
	"machineLearning": [],
	"limit": 5000
  }`)

var SingleGroupBy = []byte(`{
	"return": {
	  "entities": [
		0,
		1,
		2,
		3,
		4
	  ],
	  "relations": [
		0,
		1,
		2,
		3
	  ],
	  "groupBys": [
		0
	  ]
	},
	"entities": [
	  {
		"name": "Band",
		"ID": 0,
		"constraints": [
		  {
			"attribute": "id",
			"value": "e",
			"dataType": "string",
			"matchType": "contains"
		  }
		]
	  },
	  {
		"name": "Song",
		"ID": 1,
		"constraints": []
	  },
	  {
		"name": "Length",
		"ID": 2,
		"constraints": []
	  },
	  {
		"name": "Song",
		"ID": 3,
		"constraints": [
		  {
			"attribute": "id",
			"value": "er",
			"dataType": "string",
			"matchType": "contains"
		  }
		]
	  },
	  {
		"name": "Person",
		"ID": 4,
		"constraints": [
		  {
			"attribute": "id",
			"value": "es",
			"dataType": "string",
			"matchType": "contains"
		  }
		]
	  }
	],
	"relations": [
	  {
		"ID": 0,
		"name": "sings",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 0,
		"toType": "entity",
		"toID": 1,
		"constraints": []
	  },
	  {
		"ID": 1,
		"name": "hasLength",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 1,
		"toType": "entity",
		"toID": 2,
		"constraints": []
	  },
	  {
		"ID": 2,
		"name": "sings",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "groupBy",
		"fromID": 0,
		"toType": "entity",
		"toID": 3,
		"constraints": []
	  },
	  {
		"ID": 3,
		"name": "memberOf",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 4,
		"toType": "groupBy",
		"toID": 0,
		"constraints": []
	  }
	],
	"groupBys": [
	  {
		"ID": 0,
		"groupType": "entity",
		"groupID": [
			2
		],
		"groupAttribute": "_id",
		"byType": "entity",
		"byID": [
			0
		],
		"byAttribute": "_id",
		"appliedModifier": "avg",
		"relationID": 0,
		"constraints": [
			{
				"attribute": "_id",
				"value": "300",
				"dataType": "int",
				"matchType": "GT"
			}
		]
	  }
	],
	"machineLearning": [],
	"limit": 5000,
	"prefix": "http://api.stardog.com/"
  }
`)

var NestingAndFilterTypes = []byte(`{
	"return": {
	  "entities": [
		0,
		1,
		2,
		3,
		4,
		5,
		6,
		7,
		8,
		9,
		10,
		11,
		12,
		13,
		14
	  ],
	  "relations": [
		0,
		1,
		2,
		3,
		4,
		5,
		6,
		7,
		8,
		9,
		10,
		11,
		12,
		13
	  ],
	  "groupBys": [
		0,
		1,
		2,
		3,
		4,
		5
	  ]
	},
	"entities": [
	  {
		"name": "Band",
		"ID": 0,
		"constraints": [
		  {
			"attribute": "id",
			"value": "e",
			"dataType": "string",
			"matchType": "contains"
		  }
		]
	  },
	  {
		"name": "Song",
		"ID": 1,
		"constraints": []
	  },
	  {
		"name": "Length",
		"ID": 2,
		"constraints": []
	  },
	  {
		"name": "Song",
		"ID": 3,
		"constraints": []
	  },
	  {
		"name": "Length",
		"ID": 4,
		"constraints": []
	  },
	  {
		"name": "Song",
		"ID": 5,
		"constraints": []
	  },
	  {
		"name": "Length",
		"ID": 6,
		"constraints": []
	  },
	  {
		"name": "Song",
		"ID": 7,
		"constraints": []
	  },
	  {
		"name": "Length",
		"ID": 8,
		"constraints": []
	  },
	  {
		"name": "Song",
		"ID": 9,
		"constraints": []
	  },
	  {
		"name": "Length",
		"ID": 10,
		"constraints": []
	  },
	  {
		"name": "Song",
		"ID": 11,
		"constraints": []
	  },
	  {
		"name": "Length",
		"ID": 12,
		"constraints": []
	  },
	  {
		"name": "Song",
		"ID": 13,
		"constraints": [
		  {
			"attribute": "id",
			"value": "er",
			"dataType": "string",
			"matchType": "contains"
		  }
		]
	  },
	  {
		"name": "Person",
		"ID": 14,
		"constraints": [
		  {
			"attribute": "id",
			"value": "May",
			"dataType": "string",
			"matchType": "contains"
		  }
		]
	  }
	],
	"relations": [
	  {
		"ID": 0,
		"name": "sings",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 0,
		"toType": "entity",
		"toID": 1,
		"constraints": []
	  },
	  {
		"ID": 1,
		"name": "hasLength",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 1,
		"toType": "entity",
		"toID": 2,
		"constraints": []
	  },
	  {
		"ID": 2,
		"name": "sings",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "groupBy",
		"fromID": 0,
		"toType": "entity",
		"toID": 3,
		"constraints": []
	  },
	  {
		"ID": 3,
		"name": "hasLength",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 3,
		"toType": "entity",
		"toID": 4,
		"constraints": []
	  },
	  {
		"ID": 4,
		"name": "sings",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "groupBy",
		"fromID": 1,
		"toType": "entity",
		"toID": 5,
		"constraints": []
	  },
	  {
		"ID": 5,
		"name": "hasLength",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 5,
		"toType": "entity",
		"toID": 6,
		"constraints": []
	  },
	  {
		"ID": 6,
		"name": "sings",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "groupBy",
		"fromID": 2,
		"toType": "entity",
		"toID": 7,
		"constraints": []
	  },
	  {
		"ID": 7,
		"name": "hasLength",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 7,
		"toType": "entity",
		"toID": 8,
		"constraints": []
	  },
	  {
		"ID": 8,
		"name": "sings",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "groupBy",
		"fromID": 3,
		"toType": "entity",
		"toID": 9,
		"constraints": []
	  },
	  {
		"ID": 9,
		"name": "hasLength",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 9,
		"toType": "entity",
		"toID": 10,
		"constraints": []
	  },
	  {
		"ID": 10,
		"name": "sings",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "groupBy",
		"fromID": 4,
		"toType": "entity",
		"toID": 11,
		"constraints": []
	  },
	  {
		"ID": 11,
		"name": "hasLength",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 11,
		"toType": "entity",
		"toID": 12,
		"constraints": []
	  },
	  {
		"ID": 12,
		"name": "sings",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "groupBy",
		"fromID": 5,
		"toType": "entity",
		"toID": 13,
		"constraints": []
	  },
	  {
		"ID": 13,
		"name": "memberOf",
		"depth": {
		  "min": 1,
		  "max": 1
		},
		"fromType": "entity",
		"fromID": 14,
		"toType": "groupBy",
		"toID": 5,
		"constraints": []
	  }
	],
	"groupBys": [
	  {
		"ID": 0,
		"groupType": "entity",
		"groupID": [
			2
		],
		"groupAttribute": "_id",
		"byType": "entity",
		"byID": [
			0
		],
		"byAttribute": "_id",
		"appliedModifier": "avg",
		"relationID": 0,
		"constraints": [
			{
				"attribute": "_id",
				"value": "162",
				"dataType": "int",
				"matchType": "GET"
			}
		]
	  },
	  {
		"ID": 1,
		"groupType": "entity",
		"groupID": [
			4
		],
		"groupAttribute": "_id",
		"byType": "groupBy",
		"byID": [
			0
		],
		"byAttribute": "_id",
		"appliedModifier": "avg",
		"relationID": 0,
		"constraints": [
			{
				"attribute": "_id",
				"value": "200",
				"dataType": "int",
				"matchType": "GT"
			}
		]
	  },
	  {
		"ID": 2,
		"groupType": "entity",
		"groupID": [
			6
		],
		"groupAttribute": "_id",
		"byType": "groupBy",
		"byID": [
			1
		],
		"byAttribute": "_id",
		"appliedModifier": "avg",
		"relationID": 0,
		"constraints": [
			{
				"attribute": "_id",
				"value": "350",
				"dataType": "int",
				"matchType": "LET"
			}
		]
	  },
	  {
		"ID": 3,
		"groupType": "entity",
		"groupID": [
			8
		],
		"groupAttribute": "_id",
		"byType": "groupBy",
		"byID": [
			2
		],
		"byAttribute": "_id",
		"appliedModifier": "avg",
		"relationID": 0,
		"constraints": [
			{
				"attribute": "_id",
				"value": "300",
				"dataType": "int",
				"matchType": "LT"
			}
		]
	  },
	  {
		"ID": 4,
		"groupType": "entity",
		"groupID": [
			10
		],
		"groupAttribute": "_id",
		"byType": "groupBy",
		"byID": [
			3
		],
		"byAttribute": "_id",
		"appliedModifier": "avg",
		"relationID": 0,
		"constraints": [
			{
				"attribute": "_id",
				"value": "288",
				"dataType": "int",
				"matchType": "NEQ"
			}
		]
	  },
	  {
		"ID": 5,
		"groupType": "entity",
		"groupID": [
			12
		],
		"groupAttribute": "_id",
		"byType": "groupBy",
		"byID": [
			4
		],
		"byAttribute": "_id",
		"appliedModifier": "avg",
		"relationID": 0,
		"constraints": [
			{
				"attribute": "_id",
				"value": "251.8",
				"dataType": "int",
				"matchType": "EQ"
			}
		]
	  }
	],
	"machineLearning": [],
	"limit": 5000,
	"prefix": "http://api.stardog.com/"
  }`)
