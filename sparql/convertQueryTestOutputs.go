/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package sparql

var EmptyQueryResult = string(`SELECT DISTINCT ?e0 ?e1 ?r0 ?e2 ?r1
WHERE{
				FILTER(?r0 = <http://api.stardog.com/memberOf>)
				FILTER(?r1 = <http://api.stardog.com/memberOf>)
				FILTER regex(str(?e2), "e", "i")
				FILTER regex(str(?e0), "sierra", "i")
				?e0 ?r0 ?e1.
				?e2 ?r1 ?e1.
}`)

var SingleGroupByResult = string(`SELECT DISTINCT ?g0 ?e3 ?r2 ?e4 ?r3
Where
{
        {
                SELECT DISTINCT ?g0 (avg(?e2) AS ?avg_e2)
                Where
                {
                        FILTER(?r0 = <http://api.stardog.com/sings>)
                        FILTER(?r1 = <http://api.stardog.com/hasLength>)
                        FILTER regex(str(?e0), "e", "i")
                        ?e0 ?r0 ?e1.
                        ?e1 ?r1 ?e2.
                        BIND(?e0 AS ?g0)
                } GROUP BY ?g0 HAVING (?avg_e2 > 300)
        }
        FILTER(?r2 = <http://api.stardog.com/sings>)
        FILTER(?r3 = <http://api.stardog.com/memberOf>)
        FILTER regex(str(?e3), "er", "i")
        FILTER regex(str(?e4), "es", "i")
        ?g0 ?r2 ?e3.
        ?e4 ?r3 ?g0.
}`)

var NestingAndFilterTypesResult = string(`SELECT DISTINCT ?g5 ?e13 ?r12 ?e14 ?r13
Where
{
        {
                SELECT DISTINCT ?g5 (avg(?e12) AS ?avg_e12)
                Where
                {
                        {
                                SELECT DISTINCT ?g4 (avg(?e10) AS ?avg_e10)
                                Where
                                {
                                        {
                                                SELECT DISTINCT ?g3 (avg(?e8) AS ?avg_e8)
                                                Where
                                                {
                                                        {
                                                                SELECT DISTINCT ?g2 (avg(?e6) AS ?avg_e6)
                                                                Where
                                                                {
                                                                        {
                                                                                SELECT DISTINCT ?g1 (avg(?e4) AS ?avg_e4)
                                                                                Where
                                                                                {
                                                                                        {
                                                                                                SELECT DISTINCT ?g0 (avg(?e2) AS ?avg_e2)
                                                                                                Where
                                                                                                {
                                                                                                        FILTER(?r0 = <http://api.stardog.com/sings>)
                                                                                                        FILTER(?r1 = <http://api.stardog.com/hasLength>)
                                                                                                        FILTER regex(str(?e0), "e", "i")
                                                                                                        ?e0 ?r0 ?e1.
                                                                                                        ?e1 ?r1 ?e2.
                                                                                                        BIND(?e0 AS ?g0)
                                                                                                } GROUP BY ?g0 HAVING (?avg_e2 >= 162)
                                                                                        }
                                                                                        FILTER(?r3 = <http://api.stardog.com/hasLength>)
                                                                                        FILTER(?r2 = <http://api.stardog.com/sings>)
                                                                                        ?e3 ?r3 ?e4.
                                                                                        ?g0 ?r2 ?e3.
                                                                                        BIND(?g0 AS ?g1)
                                                                                } GROUP BY ?g1 HAVING (?avg_e4 > 200)
                                                                        }
                                                                        FILTER(?r5 = <http://api.stardog.com/hasLength>)
                                                                        FILTER(?r4 = <http://api.stardog.com/sings>)
                                                                        ?e5 ?r5 ?e6.
                                                                        ?g1 ?r4 ?e5.
                                                                        BIND(?g1 AS ?g2)
                                                                } GROUP BY ?g2 HAVING (?avg_e6 <= 350)
                                                        }
                                                        FILTER(?r7 = <http://api.stardog.com/hasLength>)
                                                        FILTER(?r6 = <http://api.stardog.com/sings>)
                                                        ?e7 ?r7 ?e8.
                                                        ?g2 ?r6 ?e7.
                                                        BIND(?g2 AS ?g3)
                                                } GROUP BY ?g3 HAVING (?avg_e8 < 300)
                                        }
                                        FILTER(?r9 = <http://api.stardog.com/hasLength>)
                                        FILTER(?r8 = <http://api.stardog.com/sings>)
                                        ?e9 ?r9 ?e10.
                                        ?g3 ?r8 ?e9.
                                        BIND(?g3 AS ?g4)
                                } GROUP BY ?g4 HAVING (?avg_e10 != 288)
                        }
                        FILTER(?r11 = <http://api.stardog.com/hasLength>)
                        FILTER(?r10 = <http://api.stardog.com/sings>)
                        ?e11 ?r11 ?e12.
                        ?g4 ?r10 ?e11.
                        BIND(?g4 AS ?g5)
                } GROUP BY ?g5 HAVING (?avg_e12 = 251.8)
        }
        FILTER(?r12 = <http://api.stardog.com/sings>)
        FILTER(?r13 = <http://api.stardog.com/memberOf>)
        FILTER regex(str(?e13), "er", "i")
        FILTER regex(str(?e14), "May", "i")
        ?g5 ?r12 ?e13.
        ?e14 ?r13 ?g5.
}`)
