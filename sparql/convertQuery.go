/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/
package sparql

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"

	"git.science.uu.nl/graphpolaris/query-conversion/entity"
)

/*
ConvertQuery converts an IncomingQueryJSON object into SPARQL
	JSONQuery: *entity.IncomingQueryJSON, the query to be converted to SPARQL
	Returns: *string, the SPARQL query and a possible error
*/
func (s *Service) ConvertQuery(JSONQuery *entity.IncomingQueryJSON) (*string, *[]byte, error) {
	entityCount := len(JSONQuery.Entities)
	// The count of relations
	relationCount := len(JSONQuery.Relations)
	// There are no entities or relations, our query is empty
	if entityCount <= 0 && relationCount <= 0 {
		fmt.Println("Empty query sent, returning default response")
		return defaultReturn()
	}

	potentialErrors := entity.Validator.Validate(JSONQuery)
	// If we find the JSONQuery to be invalid we return a error
	if len(potentialErrors) != 0 {
		for _, err := range potentialErrors {
			fmt.Printf("err: %v\n", err)
		}
		return nil, nil, errors.New("JSONQuery invalid")
	}
	entityMap, relationMap, groupByMap := entity.FixIndices(JSONQuery)

	var tree entity.TreeList
	if len(JSONQuery.Entities) != 0 && len(JSONQuery.Relations) != 0 {
		var err error
		err, tree = entity.CreateHierarchy(JSONQuery, entityMap, relationMap, groupByMap)
		if err != nil {
			return nil, nil, err
		}
	}

	seperateChain := entity.Validator.Validate(tree)
	if len(seperateChain) != 0 {
		for _, err := range seperateChain {
			fmt.Printf("err: %v\n", err)
		}
		return nil, nil, errors.New("JSONQuery invalid")
	}

	//treeJson, _ := json.Marshal(tree)
	//fmt.Println(string(treeJson))

	retval, err := createChain(JSONQuery, tree, len(tree.Trees)-1, entityMap, 0)
	if err != nil {
		return nil, nil, err
	}

	metaData := generateMetaData(JSONQuery)
	metaDataJson, err := json.Marshal(metaData)
	if err != nil {
		return nil, nil, err
	}
	return &retval, &metaDataJson, nil
}

/*
createChain converts a single chain to SPARQL, and recurses over other chains with group bys
	JSONQuery: *entity.IncomingQueryJSON, the query to be converted to SPARQL
	treeList: entity.TreeList, the hierarchy of the query
	currentTree: int, the current index of the chain in the hierarchy
	entityMap: map[int]int, a map to convert entity IDs to their indices in the JSONQuery.Entities
	indentindex: int, the amount of indentations the current chain has to have
	Returns: string, the SPARQL query and a possible error
*/
func createChain(JSONQuery *entity.IncomingQueryJSON, treeList entity.TreeList, currentTree int, entityMap map[int]int, indentindex int) (string, error) {
	tree := treeList.Trees[currentTree]
	var elements []string
	chainElements := createReturnEntities(tree)
	if len(tree.GroupBys) == 0 {
		elements = createReturnEntities(tree)
	} else {
		elements = createGroupByReturnEntities(tree)
	}
	retval := createSelect(elements, indentindex)
	for _, parentIndex := range tree.Parents {
		retval += fixIndent("\t{\n", indentindex)
		parent, err := createChain(JSONQuery, treeList, parentIndex, entityMap, indentindex+2)
		if err != nil {
			return "", err
		}
		retval += parent
		retval += fixIndent("\t}\n", indentindex)
	}
	retval += createRelationFilters(tree, JSONQuery, indentindex)
	entityFilter, err := createEntityFilters(JSONQuery, chainElements, entityMap, indentindex)
	if err != nil {
		return "", err
	}
	retval += entityFilter
	retval += createTriples(tree, indentindex)
	if len(tree.GroupBys) > 0 {
		groupBy := tree.GroupBys[0]
		if groupBy.ByType == "entity" {
			retval += fixIndent("\tBIND(?e", indentindex)
		} else {
			retval += fixIndent("\tBIND(?g", indentindex)
		}
		retval += strconv.Itoa(groupBy.ByID[0]) + " AS ?g" + strconv.Itoa(groupBy.ID) + ")\n"
	}
	retval += fixIndent("}", indentindex)
	if len(tree.GroupBys) > 0 {
		groupBy := tree.GroupBys[0]
		name := "?" + groupBy.AppliedModifier + "_"
		if groupBy.GroupType == "entity" {
			name += "e"
		} else {
			name += "g"
		}
		name += strconv.Itoa(groupBy.GroupID[0])
		retval += " GROUP BY ?g" + strconv.Itoa(groupBy.ID) + " HAVING (" + name + " " + wordsToLogicalSign(groupBy.Constraints[0]) + " " + groupBy.Constraints[0].Value + ")\n"
	}
	return retval, nil
}

/*
createSelect creates the select portion of a SPARQL query
	elements: []string, the elements to be included in the select
	indentindex: int, the amount of indentations the current chain has to have
	Returns: string, the SPARQL query
*/
func createSelect(elements []string, indentindex int) string {
	retval := fixIndent("SELECT DISTINCT", indentindex)
	for _, element := range elements {
		retval += " " + element
	}
	retval += "\n"
	retval += fixIndent("Where\n", indentindex)
	retval += fixIndent("{\n", indentindex)
	return retval
}

/*
createSelect creates the SPARQL filters to create relations
	tree: entity.Tree, the current tree containing all relations to be generated
	JSONQuery: *entity.IncomingQueryJSON, the converted JSON query to be converted to SPARQL
	indentindex: int, the amount of indentations the current chain has to have
	Returns: string, the SPARQL query
*/
func createRelationFilters(tree entity.Tree, JSONQuery *entity.IncomingQueryJSON, indentindex int) string {
	retval := ""
	for _, treeElement := range tree.TreeElements {
		retval += fixIndent("\tFILTER(?r"+strconv.Itoa(treeElement.Self.Rel.ID)+" = <"+JSONQuery.Prefix+treeElement.Self.Rel.Name+">)\n", indentindex)
	}
	return retval
}

/*
createSelect creates the SPARQL filters for entities
	JSONQuery: *entity.IncomingQueryJSON, the converted JSON query to be converted to SPARQL
	elements: []string, all the elements in the current chain
	entityMap: map[int]int, a map to convert entity IDs to their indices in the JSONQuery.Entities
	indentindex: int, the amount of indentations the current chain has to have
	Returns: (string, error), the SPARQL query and a possible error
*/
func createEntityFilters(JSONQuery *entity.IncomingQueryJSON, elements []string, entityMap map[int]int, indentindex int) (string, error) {
	retval := ""
	for _, element := range elements {
		if element[1:2] == "e" {
			index, err := strconv.Atoi(element[2:])
			if err != nil {
				return "", err
			}
			if len(JSONQuery.Entities[entityMap[index]].Constraints) > 0 {
				for _, constraint := range JSONQuery.Entities[entityMap[index]].Constraints {
					retval += fixIndent("\tFILTER regex(str("+element+"), \""+constraint.Value+"\", \"i\")\n", indentindex)
				}
			}
		}
	}
	return retval, nil
}

/*
createSelect creates the SPARQL element-relation-element triples
	tree: entity.Tree, the current tree containing all triples to be generated
	indentindex: int, the amount of indentations the current chain has to have
	Returns: string, the SPARQL query and a possible error
*/
func createTriples(tree entity.Tree, indentindex int) string {
	retval := ""
	for _, treeElement := range tree.TreeElements {
		retval += fixIndent("\t", indentindex)
		if treeElement.Self.FromNode.Name != "" {
			retval += "?e" + strconv.Itoa(treeElement.Self.FromNode.ID)
		} else {
			retval += "?g" + strconv.Itoa(treeElement.Self.FromGroupBy.ID)
		}
		retval += " ?r" + strconv.Itoa(treeElement.Self.Rel.ID)
		if treeElement.Self.ToNode.Name != "" {
			retval += " ?e" + strconv.Itoa(treeElement.Self.ToNode.ID) + ".\n"
		} else {
			retval += " ?g" + strconv.Itoa(treeElement.Self.ToGroupBy.ID) + ".\n"
		}
	}
	return retval
}

/*
createSelect creates a list of entities to be returned in the current chain
	tree: entity.Tree, the current tree containing all entities to be generated
	Returns: []string, the list of entities
*/
func createReturnEntities(tree entity.Tree) []string {
	output := make([]string, 0)
	elementMap := make(map[string]bool)
	for _, treeElement := range tree.TreeElements {
		if treeElement.Self.FromNode.Name != "" {
			name := "?e" + strconv.Itoa(treeElement.Self.FromNode.ID)
			if _, ok := elementMap[name]; !ok {
				elementMap[name] = true
				output = append(output, name)
			}
		} else {
			name := "?g" + strconv.Itoa(treeElement.Self.FromGroupBy.ID)
			if _, ok := elementMap[name]; !ok {
				elementMap[name] = true
				output = append(output, name)
			}
		}
		if treeElement.Self.ToNode.Name != "" {
			name := "?e" + strconv.Itoa(treeElement.Self.ToNode.ID)
			if _, ok := elementMap[name]; !ok {
				elementMap[name] = true
				output = append(output, name)
			}
		} else {
			name := "?g" + strconv.Itoa(treeElement.Self.ToGroupBy.ID)
			if _, ok := elementMap[name]; !ok {
				elementMap[name] = true
				output = append(output, name)
			}
		}
		name := "?r" + strconv.Itoa(treeElement.Self.Rel.ID)
		if _, ok := elementMap[name]; !ok {
			elementMap[name] = true
			output = append(output, name)
		}
	}
	return output
}

/*
createSelect creates a list of elements to be returned in the current chain
	tree: entity.Tree, the current tree containing all elements to be generated
	Returns: []string, the list of elements
*/
func createGroupByReturnEntities(tree entity.Tree) []string {
	output := make([]string, 0)
	elementMap := make(map[string]bool)
	groupBy := tree.GroupBys[0]
	elementMap["?g"+strconv.Itoa(groupBy.ID)] = true
	output = append(output, "?g"+strconv.Itoa(groupBy.ID))
	if groupBy.GroupType == "entity" {
		// Entity
		name := fmt.Sprintf("(%v(?e%v) AS ?%v_e%v)", groupBy.AppliedModifier, groupBy.GroupID[0], groupBy.AppliedModifier, groupBy.GroupID[0])
		elementMap[name] = true
		output = append(output, name)
	} else {
		// Group by
		name := fmt.Sprintf("(%v(?g%v) AS ?%v_g%v)", groupBy.AppliedModifier, groupBy.GroupID[0], groupBy.AppliedModifier, groupBy.GroupID[0])
		elementMap[name] = true
		output = append(output, name)
	}
	return output
}

/*
wordsToLogicalSign converts the MatchType in a constraint to the logical signs used in SPARQL
	element: entity.QueryConstraintStruct, the constraint for which the mathtype must be converted
	Return: string, the converted matchtype
*/
func wordsToLogicalSign(element entity.QueryConstraintStruct) string {
	var match string
	// The next lines are commented out since currently our sparql implementation can only filter on id contains, which is always string contains.
	// The only time this method gets called will be for group by, where the filter is always numerical.
	//switch element.DataType {
	//case "string":
	//	switch element.MatchType {
	//	case "NEQ":
	//		match = "!="
	//	case "contains":
	//		match = "LIKE"
	//	case "excludes":
	//		match = "NOT LIKE"
	//	default: //EQ
	//		match = "=="
	//	}
	//case "int", "float":
	switch element.MatchType {
	case "NEQ":
		match = "!="
	case "GT":
		match = ">"
	case "LT":
		match = "<"
	case "GET":
		match = ">="
	case "LET":
		match = "<="
	default: //EQ
		match = "="
	}
	//default: /*bool*/
	//	switch element.MatchType {
	//	case "NEQ":
	//		match = "!="
	//	default: //EQ
	//		match = "=="
	//	}
	//}
	return match
}

/*
fixIndent adds a certain amount of indents to a string
	input: string, the string for which the tabs must be prepended (the input must be one line only, otherwise the next lines won't have proper indentation)
	indentCount: int, the amount of tabs to be added
	Return: string, the input with prepended tabs
*/
func fixIndent(input string, indentCount int) string {
	output := ""
	for i := 0; i < indentCount; i++ {
		output += "\t"
	}
	output += input
	return output
}

/*
defaultReturn generates a default AQL query
	Return: string, the default AQL query
*/
func defaultReturn() (*string, *[]byte, error) {
	defaultReturn := `SELECT DISTINCT ?e0 ?e1 ?r0 ?e2 ?r1
	WHERE{
			FILTER(?r0 = <http://api.stardog.com/memberOf>)
			FILTER(?r1 = <http://api.stardog.com/memberOf>)
			FILTER regex(str(?e2), "e", "i")
			FILTER regex(str(?e0), "sierra", "i")
			?e0 ?r0 ?e1.
			?e2 ?r1 ?e1.
	}`
	var metaData entity.MetaData
	metaData.Triples = make(map[string]entity.Tuple)
	metaData.Triples["r0"] = entity.Tuple{From: "e0", To: "e1"}
	metaData.Triples["r1"] = entity.Tuple{From: "e2", To: "e1"}
	metaDataJson, _ := json.Marshal(metaData)
	return &defaultReturn, &metaDataJson, nil
}

/*
generateMetaData generates a map with the to and froms for every relation
	JSONQuery: *entity.IncomingQueryJSON, the converted json for which the metadata must be generated
	Return: entity.MetaData, the map containing the relation metadata
*/
func generateMetaData(JSONQuery *entity.IncomingQueryJSON) entity.MetaData {
	var metaData entity.MetaData
	metaData.Triples = make(map[string]entity.Tuple)
	for _, relation := range JSONQuery.Relations {
		relationName := "r" + strconv.Itoa(relation.ID)
		var fromName string
		if relation.FromType == "entity" {
			fromName = "e" + strconv.Itoa(relation.FromID)
		} else {
			fromName = "g" + strconv.Itoa(relation.FromID)
		}
		var toName string
		if relation.ToType == "entity" {
			toName = "e" + strconv.Itoa(relation.ToID)
		} else {
			toName = "g" + strconv.Itoa(relation.ToID)
		}
		metaData.Triples[relationName] = entity.Tuple{From: fromName, To: toName}
	}
	return metaData
}
