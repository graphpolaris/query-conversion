/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package sparql

// import (
// 	"encoding/json"
// 	"errors"
// 	"regexp"
// 	"strings"
// 	"testing"

// 	"git.science.uu.nl/graphpolaris/query-conversion/entity"
// 	"github.com/stretchr/testify/assert"
// )

// /*
// Tests an empty query
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestEmptyQueryConversion(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(emptyQuery, &JSONQuery)

// 	convertedResult, convertedtripleMap, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	regExCleaner := regexp.MustCompile(`\s+`)
// 	convertedCleanedResult := strings.TrimSpace(regExCleaner.ReplaceAllString(*convertedResult, " "))
// 	correctCleanedResult := strings.TrimSpace(regExCleaner.ReplaceAllString(EmptyQueryResult, " "))
// 	assert.Equal(t, convertedCleanedResult, correctCleanedResult)

// 	var convertedMetaData entity.MetaData
// 	var correctMetaData entity.MetaData
// 	json.Unmarshal(*convertedtripleMap, &convertedMetaData)
// 	json.Unmarshal([]byte(EmptyQueryTripleMap), &correctMetaData)

// 	metaDataEqual(t, convertedMetaData, correctMetaData)
// }

// /*
// Tests a single group by
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestSingleGroupByConversion(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(SingleGroupBy, &JSONQuery)

// 	convertedResult, convertedtripleMap, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	regExCleaner := regexp.MustCompile(`\s+`)
// 	convertedCleanedResult := strings.TrimSpace(regExCleaner.ReplaceAllString(*convertedResult, " "))
// 	correctCleanedResult := strings.TrimSpace(regExCleaner.ReplaceAllString(SingleGroupByResult, " "))
// 	assert.Equal(t, convertedCleanedResult, correctCleanedResult)

// 	var convertedMetaData entity.MetaData
// 	var correctMetaData entity.MetaData
// 	json.Unmarshal(*convertedtripleMap, &convertedMetaData)
// 	json.Unmarshal([]byte(SingleGroupByTripleMap), &correctMetaData)

// 	metaDataEqual(t, convertedMetaData, correctMetaData)
// }

// /*
// Tests multiple nested group bys with different filter types
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestNestingAndFilterTypesConversion(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(NestingAndFilterTypes, &JSONQuery)

// 	convertedResult, convertedtripleMap, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is no error
// 	assert.NoError(t, err)

// 	regExCleaner := regexp.MustCompile(`\s+`)
// 	convertedCleanedResult := strings.TrimSpace(regExCleaner.ReplaceAllString(*convertedResult, " "))
// 	correctCleanedResult := strings.TrimSpace(regExCleaner.ReplaceAllString(NestingAndFilterTypesResult, " "))
// 	assert.Equal(t, convertedCleanedResult, correctCleanedResult)

// 	var convertedMetaData entity.MetaData
// 	var correctMetaData entity.MetaData
// 	json.Unmarshal(*convertedtripleMap, &convertedMetaData)
// 	json.Unmarshal([]byte(NestingAndFilterTypesTripleMap), &correctMetaData)

// 	metaDataEqual(t, convertedMetaData, correctMetaData)
// }

// /*
// Tests a query with no relation field
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestNoRelationsField(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(NoRelationsField, &JSONQuery)

// 	_, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is an error
// 	assert.Equal(t, errors.New("JSONQuery invalid"), err)
// }

// /*
// Tests an entity with a lower than -1 in a relation
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestIncorrectRelationFrom(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(IncorrectRelationFrom, &JSONQuery)

// 	_, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is an error
// 	assert.Equal(t, errors.New("JSONQuery invalid"), err)
// }

// /*
// Tests two separated chains consisting of 1 relation each
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestSeparatedChainSingleRelationPerChain(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(SeparatedChainSingleRelationPerChain, &JSONQuery)

// 	_, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is an error
// 	assert.Equal(t, errors.New("JSONQuery invalid"), err)
// }

// /*
// Tests two separated chains consisting of 1 relation each
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestSeparatedChainDoubleRelationPerChain(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(SeparatedChainDoubleRelationPerChain, &JSONQuery)

// 	_, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is an error
// 	assert.Equal(t, errors.New("JSONQuery invalid"), err)
// }

// /*
// Tests two separated chains
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestSeperatedChainWithGroupBys(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(SeperatedChainWithGroupBys, &JSONQuery)

// 	_, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is an error
// 	assert.Equal(t, errors.New("JSONQuery invalid"), err)
// }

// /*
// Tests a group by which has different nodes on its group and by
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestDifferentNodesOnGroupBy(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(DifferentNodesOnGroupBy, &JSONQuery)

// 	_, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is an error
// 	assert.Equal(t, errors.New("JSONQuery invalid"), err)
// }

// /*
// Tests a group by which has a modifier which aql doesn't recognise
// 	t: *testing.T, makes go recognise this as a test
// */
// func TestIncorrectGroupByModifier(t *testing.T) {
// 	// Setup for test
// 	// Create query conversion service
// 	service := NewService()

// 	// Unmarshall the incoming message into an IncomingJSONQuery object
// 	var JSONQuery entity.IncomingQueryJSON
// 	json.Unmarshal(IncorrectGroupByModifier, &JSONQuery)

// 	_, _, err := service.ConvertQuery(&JSONQuery)

// 	// Assert that there is an error
// 	assert.Equal(t, errors.New("JSONQuery invalid"), err)
// }

// func metaDataEqual(t *testing.T, convertedMetaData entity.MetaData, correctMetaData entity.MetaData) {
// 	for key, val1 := range convertedMetaData.Triples {
// 		val2, ok := correctMetaData.Triples[key]
// 		assert.True(t, ok)
// 		assert.Equal(t, val1, val2)
// 	}
// 	for key, val1 := range correctMetaData.Triples {
// 		val2, ok := convertedMetaData.Triples[key]
// 		assert.True(t, ok)
// 		assert.Equal(t, val1, val2)
// 	}
// }
