.PHONY: dep test

dep: ## Get the dependencies 
	@go get -a ./...
	@go get -u github.com/boumenot/gocover-cobertura

test: dep ## Run unittests
	@go test -coverpkg=./aql -coverprofile=aql_coverage.txt -covermode count ./...
	@cat aql_coverage.txt > coverage.txt
	@go test -coverpkg=./sparql -coverprofile=sparql_coverage.txt -covermode count ./...
	@tail -n +2 ./sparql_coverage.txt >> coverage.txt
	@go test -coverpkg=./entity -coverprofile=entity_coverage.txt -covermode count ./...
	@tail -n +2 ./entity_coverage.txt >> coverage.txt

testneo: dep ## Run unittests
	@go test ./cypherv2 -v

testall:
	@go test ./cypherv2
	@go test ./cypher
	@go test ./aql
	@go test ./sparql

coverage: dep
	@go test -v -coverpkg=./aql -coverprofile=aql_cover.out ./...
	@cat aql_cover.out > cover.out
	@go test -v -coverpkg=./sparql -coverprofile=sparql_cover.out ./...
	@tail -n +2 ./sparql_cover.out >> cover.out
	@go test -v -coverpkg=./entity -coverprofile=entity_cover.out ./...
	@tail -n +2 ./entity_cover.out >> cover.out
	@go tool cover -func cover.out | grep total
	@go tool cover -html=cover.out -o cover.html

tidy:
	GOPROXY=direct go get -u -v all 
	@go mod tidy --compat=1.22

